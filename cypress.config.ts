export default {
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'http://127.0.0.1:4200',
    viewportWidth: 480,
    viewportHeight: 720

  },
};
