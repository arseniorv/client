import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'ionic.upmeup.app',
  appName: 'UpmeUp',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
