describe('Basic offer application', () => {
  it('Candidate should be able to apply to an existing offer', () => {
    
    let ent_login = 'andreitasanchezgonzalez@protonmail.com'
    let password = 'Holaquetal1234'
    const w_interval = 500
    
    const o = {
        title : 'Oferta generada automáticamente'
        
    }
            
    // Login
    cy.visit('/login')
    cy.get('input[id=userEmail]')
      .clear()
      .type(ent_login) 
    
    cy.get('input[id=userPassword]')
      .clear()
      .type(password) 
    // Click Enter button
    
    cy.get('ion-button[type=submit]').click()
    cy.wait(w_interval)
    
    // Close Ads
    cy.get('ion-row.md > .ion-color').click()
    cy.wait(w_interval)
    
    // Ofertas - bottom menu
    cy.get('ion-icon.ofertas-ico')
        .should('be.visible').click()  
    cy.wait(2 * w_interval)
    // cy.wait(w_interval) // TODO - Wait to be visible
    
    
    // Go to known offer
    cy.get('[cy-data="offer-title"]').contains(o.title).click({force:true}) 

    
    // Scroll down and Click on "inscribirme"
    cy.get('[cy-data="sign-me-up"]')
        .should('be.visible').click() // 1st option.click() // Finalitzar
    cy.wait(w_interval) // TODO - Wait to be visible
    
    
  })
})