describe('Basic offer creation', () => {
  it('Entities should be able to publish a new offer', () => {
    
    let ent_login = 'empresa@example.com'
    let password = 'Holaquetal1234'
    const w_interval = 500
    
    const o = {
        title : 'Oferta generada automáticamente',
        level : 'FPGA',
        skills : ['Liderazgo','Compromiso','Madurez','Buen humor'],
        salary : '43.000',
        description : 'El puestako de tu vida si esto no fuera un cypress :D',
        requirements : 'Hacer de todo un poco y un poco de tó',
        more_req:', y si acaso alguna cosilla más...',
        
    }
            
    // Login
    cy.visit('/login')
    cy.get('input[id=userEmail]')
      .clear()
      .type(ent_login) 
    
    cy.get('input[id=userPassword]')
      .clear()
      .type(password) 
    // Click Enter button
    
    cy.get('ion-button[type=submit]').click()
    cy.wait(w_interval)
    
    // Close Ads
    cy.get('ion-row.md > .ion-color').click()
    cy.wait(w_interval)
    
    // Ofertas - bottom menu
    cy.get('ion-icon.ofertas-ico')
        .should('be.visible').click() 
    cy.wait(2 * w_interval)
    // cy.wait(w_interval) // TODO - Wait to be visible
    
    // cy.get('.ion-icon .ofertas-ico'), 'Ofertas').click(); // Ofertas
    // cy.contains('ion-col > .ofertas-ico', 'Ofertas').click(); // Ofertas
    cy.get('[cy-data="add-offer"]')
        .should('be.visible').click() // 1st option.click() // Finalitzar
    cy.wait(w_interval) // TODO - Wait to be visible
    
    
    // cy.get('ion-input[id=iTitle]')
    cy.get('#iTitle > .native-input')
      .clear()
      .type(o.title) 
    
    cy.get('#iEduLevel > .native-input')
      .should('be.visible').click() // 1st option
      .clear()
      .type(o.level) 
    
    // cy.get('tag-input[id=iCompetence]').click()
    cy.get('#iCompetence').click()
      .type(o.skills[0]).type('\n')
      .type(o.skills[1]).type('\n')
      .type(o.skills[2]).type('\n')
      .type(o.skills[3]).type('\n')
      
    cy.get('#iCity').click()
    cy.wait(w_interval)
        
    cy.get('.action-sheet-button')
        .eq(1).should('be.visible').click() // 2nd option
    cy.wait(w_interval)
    
    cy.get('#iRemoto').should('be.visible').click()
    cy.wait(w_interval)
    cy.get('.action-sheet-button')
        .eq(1).should('be.visible')
        .click() // 2nd option
    cy.wait(w_interval)
    
    cy.get('#iTipoContrato')
        .should('be.visible').click()
    cy.wait(w_interval)
    cy.get('.action-sheet-button')
        .eq(2).should('be.visible').click() // 3th option
    cy.wait(w_interval)
    
    cy.get('#iRequiredExperience')
        .should('be.visible').click()
    cy.wait(w_interval)
    cy.get('.action-sheet-button')
        .should('be.visible').eq(0).click() // 1st option
    cy.wait(w_interval)
    
    cy.get('#iJornada')
        .should('be.visible').click()
    cy.wait(w_interval)
    cy.get('.action-sheet-button')
        .eq(2).should('be.visible').click() // 3th option
    cy.wait(w_interval)
    
    
    cy.get('#iRangoSalarial > .native-input')
      .clear()
      .type(o.salary) 
    cy.wait(w_interval)
    
    
    cy.get('#iDescripcio > .textarea-wrapper > .native-textarea')
      .clear()
      .type(o.description) 
    cy.wait(w_interval)
    
    
    cy.get('#iRequirements > .textarea-wrapper > .native-textarea')
      .clear()
      .type(o.requirements) 
    cy.wait(w_interval)
    
    // TODO Should not be clickable yet
    // cy.get('[cy-data="publish-offer"]') // Actualizar/publicar
        // .should('have.attr', 'disabled', true)
        // .should('not.have.class', 'disabled')
    // .should('be.disabled')
    
    cy.get('#iRequirements > .textarea-wrapper > .native-textarea')
      .type(o.more_req) 
    
    cy.get('[cy-data="publish-offer"]').click() // Actualizar/publicar
    
    
  })
})