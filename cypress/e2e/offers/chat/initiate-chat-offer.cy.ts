describe('Initiate chat with offer candidates', () => {
    it('Entities should be able to initiate chat with any candidate', () => {
      
      let ent_login = 'empresa@example.com'
      let password = 'Holaquetal1234'
      let message = 'Buenas tardes, soy tu cypress!'
      const w_interval = 500

      const o = {
        title : 'Oferta generada automáticamente'  
      }

      const u = {
        name: 'Fulanita'
      }
      
              
      // Login
      cy.visit('/login')
      cy.get('input[id=userEmail]')
        .clear()
        .type(ent_login) 
      
      cy.get('input[id=userPassword]')
        .clear()
        .type(password) 
      // Click Enter button
      
      cy.get('ion-button[type=submit]').click()
      cy.wait(w_interval)
      
      // Close Ads
      cy.get('ion-row.md > .ion-color').click()
      cy.wait(w_interval)
      
      // Ofertas - bottom menu
      cy.get('ion-icon.ofertas-ico')
          .should('be.visible').click() 
      cy.wait(2 * w_interval)
      // cy.wait(w_interval) // TODO - Wait to be visible
      
      
      // Go to known offer
    cy.get('[cy-data="offer-title"]').contains(o.title).click({force:true})

    // Search candidate and click con fit button
    // cy.get('[cy-data="candidate"]').contains(u.name)


    //PROBLEMA: si hay mas de un candidato, no sabe identificar en cual tiene que dar a encaja

    cy.get('[cy-data="candidate"]').each((a)=>{
      cy.wrap(a).contains(u.name)
      return a
    })
    cy.then(()=>{cy.find('[cy-data="fits"]')})
  

    // Click in chat bubble
    cy.get('[cy-data="chat-bubble"]').click()
    cy.wait(5 * w_interval)



    //ESTADO: no he conseguido que escriba solo, creo que es porque es una api y no encuentra los elementos. 
    //De momento se puede llegar hasta aquí y escribir manualmente el mensaje.

    // cy.get('textarea').type(message)
      
      
      
    })
  })