describe('Answer chat', () => {
    it('Candidates should be able to answer to chat', () => {
      
      let email = 'andrea@example.com'
      let password = 'Holaquetal1234'
      let message = 'Buenas tardes, soy tu cypress!'
      const w_interval = 500

      const o = {
        title : 'Oferta generada automáticamente'  
      }

      const u = {
        name: 'Andrea'
      }
      
              
      // Login
      cy.visit('/login')
      cy.get('input[id=userEmail]')
        .clear()
        .type(email) 
      
      cy.get('input[id=userPassword]')
        .clear()
        .type(password) 
      // Click Enter button
      
      cy.get('ion-button[type=submit]').click()
      cy.wait(w_interval)
      
      // Close Ads
      cy.get('ion-row.md > .ion-color').click()
      cy.wait(w_interval)
      
      // Ofertas - bottom menu
      cy.get('ion-icon.ofertas-ico')
          .should('be.visible').click() 
      cy.wait(2 * w_interval)

      
      //Go to my candidatures
      cy.get('[cy-data="my-candidatures"]').click({force:true})
      
      // Go to known offer
    cy.get('[cy-data="offer-title"]').contains(o.title).click({force:true})

    // Click in chat bubble
    cy.get('[cy-data="chat-bubble"]').click()
    cy.wait(5 * w_interval)



    //ESTADO: no he conseguido que escriba solo, creo que es porque es una api y no encuentra los elementos. 
    //De momento se puede llegar hasta aquí y escribir manualmente el mensaje.

    // cy.get('textarea').type(message)
      
      
      
    })
  })