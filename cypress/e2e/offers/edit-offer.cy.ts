describe('Offer edition', () => {
    it('Entities should be able to edit a created offer', () => {

        let ent_login = 'empresa@example.com'
        let password = 'Holaquetal1234'
        const w_interval = 500
        
        const o = {
            title : 'Oferta editada automáticamente',
            level : 'Bachillerato',
            skills : ['PHP','Typescript'],
            salary : '43.000',
            description : 'El puestako de tu vida si esto no fuera un cypress :D',
            requirements : 'Hacer de todo un poco y un poco de tó',
            more_req:', y si acaso alguna cosilla más...',
            
        }

        // Login
        cy.visit('/login')
        cy.get('input[id=userEmail]')
        .clear()
        .type(ent_login) 
        
        cy.get('input[id=userPassword]')
        .clear()
        .type(password) 

        // Click Enter button
        cy.get('ion-button[type=submit]').click()
        cy.wait(w_interval)
        
        // Close Ads
        cy.get('ion-row.md > .ion-color').click()
        cy.wait(w_interval)
        
        // Ofertas - bottom menu
        cy.get('ion-icon.ofertas-ico')
            .should('be.visible').click() 
        cy.wait(2 * w_interval)

        // Open modal to edit offer
        cy.get('[cy-data="edit-offer"]')
        .eq(0).should('be.visible').click() //1st offer
        cy.wait(w_interval)

        cy.get('#iTitle > .native-input')
        .clear()
        .type(o.title) 

        cy.get('#iEduLevel > .native-input')
        .should('be.visible').click()
        .clear()
        .type(o.level) 

        cy.get('tag-input-form').click()
        .type(o.skills[0]).type('\n')
        cy.wait(w_interval)

        cy.get('[cy-data="remove-competence"')
        .eq(0).click()

        cy.get('tag-input-form').click()
        .type(o.skills[1]).type('\n')
        cy.wait(w_interval)

        cy.get('#iCity').click()
        cy.wait(w_interval)    
        cy.get('.action-sheet-button')
            .eq(15).should('not.be.visible').click() // 16th option
        cy.wait(w_interval)

        cy.get('#iRemoto').should('be.visible').click()
        cy.wait(w_interval)
        cy.get('.action-sheet-button')
            .eq(2).should('be.visible')
            .click() // 3th option
        cy.wait(w_interval)

        cy.get('#iTipoContrato')
        .should('be.visible').click()
        cy.wait(w_interval)
        cy.get('.action-sheet-button')
            .eq(1).should('be.visible').click() // 2th option
        cy.wait(w_interval)

        cy.get('[cy-data="update-offer"]').click() // Actualizar




    })
})