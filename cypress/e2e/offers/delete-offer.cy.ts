describe('Basic offer deletion', () => {
    it('Entities should be able to delete de last of their offers', () => {
      
      let ent_login = 'empresa@example.com'
      let password = 'Holaquetal1234'
      const w_interval = 500
      
              
      // Login
      cy.visit('/login')
      cy.get('input[id=userEmail]')
        .clear()
        .type(ent_login) 
      
      cy.get('input[id=userPassword]')
        .clear()
        .type(password) 
      // Click Enter button
      
      cy.get('ion-button[type=submit]').click()
      cy.wait(w_interval)
      
      // Close Ads
      cy.get('ion-row.md > .ion-color').click()
      cy.wait(w_interval)
      
      // Ofertas - bottom menu
      cy.get('ion-icon.ofertas-ico')
          .should('be.visible').click() 
      cy.wait(2 * w_interval)
      // cy.wait(w_interval) // TODO - Wait to be visible
      
      // Open modal to edit offer
      cy.get('[cy-data="edit-offer"]')
      .eq(0).should('be.visible').click() //1st offer
      cy.wait(w_interval)

      // Click on delete button and confirm
      cy.get('ion-button[id=open-modal]').click()
      cy.get('ion-button[id=confirm]').click()
      
      
      
      
    })
  })