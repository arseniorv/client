describe('Basic project creation', () => {
    it('Users should be able to publish a new project', () => {
      
      let ent_login = 'empresa@example.com'
      let password = 'Holaquetal1234'
      const w_interval = 500
      
      const p = {
          title : 'Proyecto generado automáticamente',
          sector:['Aero', 'Aliment'],
          skills : ['Liderazgo','Compromiso','Madurez','Buen humor'],
          values: ['Colab', 'Resilie', 'Respons'],
          description : 'El proyectako de tu vida si esto no fuera un cypress :D',
          admins: ['andrea@example.com','emailfake@fake.com']
          
      }
              
      // Login
      cy.visit('/login')
      cy.get('input[id=userEmail]')
        .clear()
        .type(ent_login) 
      
      cy.get('input[id=userPassword]')
        .clear()
        .type(password) 
      // Click Enter button
      
      cy.get('ion-button[type=submit]').click()
      cy.wait(w_interval)
      
      // Close Ads
      cy.get('ion-row.md > .ion-color').click()
      cy.wait(w_interval)
      
      // Projects - bottom menu
      cy.get('[cy-data="projects"]')
          .should('be.visible').click() 
      cy.wait(w_interval)


      // Go to My projects
      cy.get('[cy-data="go-to-projects"]')
          .should('be.visible').click() 
      cy.wait(w_interval) 

      // Open modal to create project
      cy.get('[cy-data="add-project"]')
          .click({force: true})
      cy.wait(w_interval) 
      
      // Add title
      cy.get('#iTitle > .native-input')
        .clear()
        .type(p.title) 

      // Add description
      cy.get('#iDescription > .textarea-wrapper > .native-textarea')
        .clear()
        .type(p.description) 
      cy.wait(w_interval)

      // Select sectors
      cy.get('ion-select#iSector').click()
      cy.wait(w_interval)
      cy.contains('.alert-checkbox-button', p.sector[0]).click();
      cy.contains('.alert-checkbox-button', p.sector[1]).click();
      cy.contains('button.alert-button', 'OK').click(); // OK
      cy.wait(w_interval)

      // Select status
      cy.get('#iStatus').click()
      cy.wait(w_interval)
      cy.get('.action-sheet-button')
          .eq(1).should('be.visible').click() // 2nd option (just and idea)
      cy.wait(w_interval)

      
      // Add competences
      cy.get('#iCompetence').click()
        .type(p.skills[0]).type('\n')
        .type(p.skills[1]).type('\n')
        .type(p.skills[2]).type('\n')
        .type(p.skills[3]).type('\n')
        
      // Select city
      cy.get('#iCity').click()
      cy.wait(w_interval)
      cy.get('.action-sheet-button')
          .eq(1).should('be.visible').click() // 2nd option
      cy.wait(w_interval)

      // Add values
      cy.get('ion-select#iValues').click()
      cy.wait(w_interval)
      cy.contains('.alert-checkbox-button', p.values[0]).click();
      cy.contains('.alert-checkbox-button', p.values[1]).click();
      cy.contains('.alert-checkbox-button', p.values[2]).click();
      cy.contains('button.alert-button', 'OK').click(); // OK
      cy.wait(w_interval)

      //Add admins
      cy.get('#iAdmins').click()
        .type(p.admins[0]).type('\n')
        .type(p.admins[1]).type('\n')
      
      //Try to publish with error
      cy.get('[cy-data="publish-project"]').click() 
      cy.wait(8 * w_interval) // controlled error because  of wrong email

      //Delete wrong email
      cy.get('[cy-data="removeEmail"]').eq(1).click() 

      // Publish
      cy.get('[cy-data="publish-project"]').click() 


      
      
    })
  })