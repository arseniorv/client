describe('Basic project edition', () => {
    it('Users should be able to edit a project', () => {
      
      let ent_login = 'empresa@example.com'
      let password = 'Holaquetal1234'
      const w_interval = 500
      
      const p = {
          title1: 'Proyecto generado',
          title : 'Proyecto editado automáticamente',
          sector:['Aero', 'Aliment', 'Constr'],
          skills : ['Liderazgo','Compromiso','Madurez','Buen humor', 'Romanticismo'],
          values: ['Colab', 'Resilie', 'Respons', 'Diver'],
          description : 'El proyectako de tu vida si esto no fuera un cypress EDITADO :D',
          admins: ['andrea@example.com','emailfake@fake.com']
          
      }
              
      // Login
      cy.visit('/login')
      cy.get('input[id=userEmail]')
        .clear()
        .type(ent_login) 
      
      cy.get('input[id=userPassword]')
        .clear()
        .type(password) 
      // Click Enter button
      
      cy.get('ion-button[type=submit]').click()
      cy.wait(w_interval)
      
      // Close Ads
      cy.get('ion-row.md > .ion-color').click()
      cy.wait(w_interval)
      
      // Projects - bottom menu
      cy.get('[cy-data="projects"]')
          .should('be.visible').click() 
      cy.wait(w_interval)


      // Go to My projects
      cy.get('[cy-data="go-to-projects"]')
          .should('be.visible').click() 
      cy.wait(w_interval) 

      cy.get('[cy-data="project-title"]').contains(p.title1).click({force:true})

      // Open modal to edit project
      cy.get('[cy-data="edit-project"]')
          .should('be.visible').click()
      cy.wait(w_interval) 
      
      // Add title
      cy.get('#iTitle > .native-input')
        .clear()
        .type(p.title) 

      // Add description
      cy.get('#iDescription > .textarea-wrapper > .native-textarea')
        .clear()
        .type(p.description) 
      cy.wait(w_interval)

      // Select sectors
      cy.get('ion-select#iSector').click()
      cy.wait(w_interval)
      cy.contains('.alert-checkbox-button', p.sector[0]).click();
      cy.contains('.alert-checkbox-button', p.sector[1]).click();
      cy.contains('.alert-checkbox-button', p.sector[2]).click();
      cy.contains('button.alert-button', 'OK').click(); // OK
      cy.wait(w_interval)

      // Select status
      cy.get('#iStatus').click()
      cy.wait(w_interval)
      cy.get('.action-sheet-button')
          .eq(1).should('be.visible').click() // 2nd option (just and idea)
      cy.wait(w_interval)

      
      // Add competences
      cy.get('tag-input-form').eq(0).click()
        .type(p.skills[4]).type('\n')
        cy.wait(w_interval)
    
      cy.get('[cy-data="removeCompetence"]').eq(0).click()
      cy.get('[cy-data="removeCompetence"]').eq(1).click()

        
      // Select city
      cy.get('#iCity').click()
      cy.wait(w_interval)
      cy.get('.action-sheet-button')
          .eq(4).should('be.visible').click() // 5th option
      cy.wait(w_interval)

      // Add values
      cy.get('ion-select#iValues').click()
      cy.wait(w_interval)
      cy.contains('.alert-checkbox-button', p.values[0]).click();
      cy.contains('.alert-checkbox-button', p.values[1]).click();
      cy.contains('.alert-checkbox-button', p.values[3]).click();
      cy.contains('button.alert-button', 'OK').click(); // OK
      cy.wait(w_interval)

      // Add admins
      cy.get('tag-input-form').eq(1).click()
        .type(p.admins[1]).type('\n')
      
      // Try to publish with error
      cy.get('[cy-data="publish-project"]').click() 
      cy.wait(8 * w_interval) // controlled error because  of wrong email

      //Delete wrong email
      cy.get('#iAdmins').contains(p.admins[1]).within(()=>{
        cy.get('[cy-data="removeEmail"]').click()
      })
       

      // Publish
      cy.get('[cy-data="publish-project"]').click() 


      
      
    })
  })