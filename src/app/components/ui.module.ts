import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';

import { MultilanguageComponent } from '../partials/multilanguage/multilanguage.component';
import { MultilanguageModule } from '../partials/multilanguage/multilanguage.module';
import { BannerComponent } from './banner/banner.component';
import { BackButtonComponent } from './buttons/back-button/back-button.component';
import { ChatButtonComponent } from './buttons/chat-button/chat-button.component';
import { FabButtonComponent } from './buttons/fab-button/fab-button.component';
import { StartChatButtonComponent } from './buttons/start-chat-button/start-chat-button.component';
import { TextButtonComponent } from './buttons/text-button/text-button.component';
import { ConversationCardComponent } from './cards/conversation-card/conversation-card.component';
import { OfferCardComponent } from './cards/offer-card/offer-card.component';
import { SelectDropdownComponent } from './forms/select-dropdown/select-dropdown.component';
import { TagInputComponent } from './forms/tag-input/tag-input.component';
import { TextFieldComponent } from './forms/text-field/text-field.component';
import { TextAreaComponent } from './forms/textarea/textarea.component';
import { RegisterStep1Component } from './register/step1/register-step-1.component';
import { CandidateRegisterStep2Component } from './register/step2/candidate/candidate-register-step-2.component';
import { CompanyRegisterStep2Component } from './register/step2/company/company-register-step-2.component';
import { RegisterStep3Component } from './register/step3/register-step-3.component';
import { CandidateRegisterStep4Component } from './register/step4/candidate/candidate-register-step-4.component';
import { CompanyRegisterStep4Component } from './register/step4/company/company-register-step-4.component';
import { RegisterStep5Component } from './register/step5/register-step-5.component';
import { RegisterStep6Component } from './register/step6/register-step-6.component';
import { RegisterStep7Component } from './register/step7/register-step-7.component';
import { SlideDirective } from './slider/slide.directive';
import { SliderComponent } from './slider/slider.component';
import { StepperComponent } from './stepper/stepper.component';
import { ToolbarComponent } from './toolbar/toolbar.component';


@NgModule({
  imports: [
    ToolbarComponent,
    BannerComponent,
    TextButtonComponent,
    FabButtonComponent,
    StartChatButtonComponent,
    BackButtonComponent,
    ChatButtonComponent,
    TagInputComponent,
    ConversationCardComponent,
    OfferCardComponent,
    CommonModule,
    IonicModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    StepperComponent,
    SliderComponent,
    TagInputComponent,
    RegisterStep1Component,
    CompanyRegisterStep2Component,
    CandidateRegisterStep2Component,
    RegisterStep3Component,
    CompanyRegisterStep4Component,
    CandidateRegisterStep4Component,
    RegisterStep5Component,
    RegisterStep6Component,
    RegisterStep7Component,
    TextFieldComponent,
    TextAreaComponent,
    SelectDropdownComponent
  ],
  providers: [MultilanguageComponent],
  declarations: [SlideDirective],
  exports: [
    ToolbarComponent,
    BannerComponent,
    TextButtonComponent,
    FabButtonComponent,
    StartChatButtonComponent,
    BackButtonComponent,
    ChatButtonComponent,
    TagInputComponent,
    ConversationCardComponent,
    OfferCardComponent,
    StepperComponent,
    SliderComponent,
    SlideDirective,
    RegisterStep1Component,
    CompanyRegisterStep2Component,
    CandidateRegisterStep2Component,
    RegisterStep3Component,
    CompanyRegisterStep4Component,
    CandidateRegisterStep4Component,
    RegisterStep5Component,
    RegisterStep6Component,
    RegisterStep7Component,
    TextFieldComponent,
    TextAreaComponent,
    SelectDropdownComponent
  ],
})
export class UIModule {}