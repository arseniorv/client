import { SharedModule } from "../../shared.module";
import { Component, input, output } from "@angular/core";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";

@Component({
    standalone: true,
    selector: 'umu-alert',
    templateUrl: './alert.component.html',
    styleUrl:'./alert.component.css',
    imports: [SharedModule, TextButtonComponent]
})
export class AlertComponent {

    isOpen = input.required<boolean>();
    title = input<string>('');
    customIconName = input<string>('');
    iconName = input<string>('');
    message = input<string>('');
    button1 = input<string>('');
    button2 = input<string>('');
    onClose = output<void>();
    onClickButton1 = output<void>();
    onClickButton2 = output<void>();
    
    onCloseHandler() {
        this.onClose.emit();
    }

    onClickButton1Handler() {
        this.onClickButton1.emit();
    }

    onClickButton2Handler() {
        this.onClickButton2.emit();
    }
}