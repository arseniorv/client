import { Component, inject, input, output } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { ModalComponent } from "../modal/modal.component";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { TextFieldComponent } from "../../forms/text-field/text-field.component";
import { SelectDropdownComponent } from "../../forms/select-dropdown/select-dropdown.component";
import { TagInputComponent } from "../../forms/tag-input/tag-input.component";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { UploadFileAlertComponent } from "../upload-file-alert/upload-file-alert.component";
import { User } from "src/app/models/user";
import { CompetenciesService } from "src/app/services/competencies.service";
import { TranslateService } from "@ngx-translate/core";
import { UmuTranslateDirective } from "src/app/shared/directives/translate.directive";
import { SectorsService } from "src/app/services/sectors.service";
import { UserService } from "src/app/services/user/user.service";
import { ProjectsService } from "src/app/services/projects/projects.service";
import { SoftSkillsService } from "src/app/services/softskills.service";
import { MultilanguageComponent } from "src/app/partials/multilanguage/multilanguage.component";
import { map } from "rxjs";
import { cityList, statusOptions } from "src/app/utils/constants";
import { InputProject, Member, Project } from "src/app/models/project";
import { Competency } from "src/app/models/competency.model";
import { OfferStatus } from "src/app/models/offer-status.model";
import { LoadingController, ToastController } from "@ionic/angular";

@Component({
    standalone: true,
    selector: 'umu-edit-project-modal',
    templateUrl: './edit-project-modal.component.html',
    styleUrl: './edit-project-modal.component.scss',
    imports: [
        SharedModule,
        ReactiveFormsModule,
        ModalComponent,
        TextFieldComponent,
        SelectDropdownComponent,
        TagInputComponent,
        TextButtonComponent,
        UploadFileAlertComponent
    ]
})
export class EditProjectModalComponent {

    project = input.required<Project | any>()
    isOpen = input.required<boolean>();
    onClose = output<void>();

    private formsBuilder = inject(FormBuilder);
    private competenciesService = inject(CompetenciesService);
    private translateService = inject(TranslateService);
    private umuTranslate = inject(UmuTranslateDirective);
    private sectorsService = inject(SectorsService);
    private userService = inject(UserService);
    private projectsService = inject(ProjectsService);
    private softskillsService = inject(SoftSkillsService);
    private multilanguageComponent = inject(MultilanguageComponent);
    private loadingCtrl = inject(LoadingController);
    private toastCtrl = inject(ToastController);

    readonly cityList = cityList;
    readonly statusOptions = statusOptions;

    titleMinLength = 2
    titleMaxLength = 100
    descriptionMinLength = 40
    descriptionMaxLength = 1000
    competenciesMinLength = 2
    competenciesMaxLength = 20

    competenciesNames: string[] = []
    selectedCompetencies: Competency[] = [];
    selectedCompetencyNames: string [] = [];
    competenciesList: Competency[] = [];
    translatedAndOrderedCities = []
    sectorList: any[] = [];
    sectorNameList: any[] = [];
    translatedAndOrderedSectors:any[] = [];
    selectedSectors: any[] = [];
    selectedSkills: any[] = [];
    offerCompetencies: string[] = []
    errorMinTags: boolean = false
    minTags: number = 2
    me: User
    finalSkillsList: {value: any, label: any} [] = [];
    translatedSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
    skillsList: any[] = [];
    usersList: any[] = [];
    usersInfoList: any[] = [];
    admins: any[] = [];
    adminsList: Member[] = []
    isOpenInfoAlert: boolean = false
    projectInput: InputProject = {} as InputProject
    filenames: any[] = [];
    files: any[] = [];

    form = this.formsBuilder.group({
        title: ['', [
            Validators.required, 
            Validators.minLength(this.titleMinLength), 
            Validators.maxLength(this.titleMaxLength)
        ]],
        description: ['', [
            Validators.required,  
            Validators.minLength(this.descriptionMinLength), 
            Validators.maxLength(this.descriptionMaxLength)
        ]],
        sector: [[''], [Validators.required]],
        competencies: [[{} as Competency], [
            Validators.minLength(this.competenciesMinLength), 
            Validators.maxLength(this.competenciesMaxLength)
          ]],
        city: [''],
        softskills: [[], [Validators.required]],
        status: ['', [Validators.required]],
        admins: [['']] 
    });

    constructor(){
        this.form.valueChanges.subscribe(()=>{
            this.setInputValues();
            this.addSector(this.form.value.sector);
            this.addSoftSkill(this.form.value.softskills);
        })
    }

    ngOnInit(){
        this.userService.me.subscribe(me => this.me = me)
        this.competenciesService.competencies.subscribe(competencies => {
            if (competencies) {
                this.competenciesList = [...competencies]
                this.competenciesNames = [...competencies]
                .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
            }
        })
        this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list);
        this.getSectors();
        this.getSoftskills();
        this.getAllUsers();
        if(this.project()){   
            this.setCompetencies();
            this.setAdmins();
            this.setValues(this.project());
          }
        
        
    }

    get controls() {
        return this.form.controls;
    }

    getSectors(){
        this.sectorsService.sectors.subscribe((sectors) => {
            if(sectors !== undefined){
                this.sectorList = sectors
                this.sectorNameList = sectors.map(sector => sector.name)
                this.umuTranslate.translateAndOrder(this.sectorNameList).subscribe(list => {
                    this.translatedAndOrderedSectors = list
                })
            }
        })
    }

    getSoftskills() {
        this.softskillsService.qGetAllSkills().valueChanges.pipe(
            map(result => result.data)
        ).subscribe((item) => {
            this.skillsList = item.getSoftskills;
            this.translatedSkillsList = this.multilanguageComponent.translateSkillsLists(this.skillsList)
            for (let skill of this.translatedSkillsList){
            this.finalSkillsList.push({"value": skill.name, "label": skill.label})
            }
        });
    
    }

    getAllUsers(){
        this.userService.qGetAllUsers().valueChanges.pipe(
            map(result => result.data)
        ).subscribe((item) => {
            this.usersInfoList = item.getUsers
            this.usersList = item.getUsers.map(u => u.email);
        })
    }

    setCompetencies(){
        this.selectedCompetencies = []
        this.project().competencies.map(c => {
            const {_id, name} = {_id: c._id, name: c.name}
            this.selectedCompetencies.push({_id, name})
        })  
    }

    setAdmins(){
        this.project().admins.map(a => {
            const {_id, name} = {_id: a._id, name: a.email}
            this.admins.push({_id, name})
            const admin = {_id: a._id, email: a.email}
            this.adminsList.push(admin)
        })
    }

    setValues(project){
        this.form.get('title').setValue(project.title);
        this.form.get('description').setValue(project.description);
        this.form.get('sector').setValue(project.sector.map(s => s.name));
        this.form.get('city').setValue(project.city);
        this.form.get('competencies').setValue(this.selectedCompetencies);
        this.form.get('softskills').setValue(project.softSkills.map(s => s.name));
        this.form.get('status').setValue(project.status);
        this.form.get('admins').setValue(this.admins);

    }

    setInputValues(){
        this.projectInput.id = this.project()._id
        this.projectInput.userId = this.me._id
        this.projectInput.title = this.form.value.title;
        this.projectInput.description = this.form.value.description;
        this.projectInput.sector = this.selectedSectors
        this.projectInput.city = this.form.value.city;
        this.projectInput.competencies = this.selectedCompetencies;
        this.projectInput.softSkills = this.selectedSkills;
        this.projectInput.status = this.form.value.status;
        this.projectInput.admins = this.adminsList;
        this.projectInput.files = this.filenames;
    }

    addSector(sectors){
        this.selectedSectors = [];
        for (let sector of sectors){
            this.sectorList.map((s) => {
                if(s.name === sector){
                    this.selectedSectors.push(s)
                }
            })
        }
    }

    addSoftSkill(softskills){
        this.selectedSkills = [];
        for (let skill of softskills){
            this.skillsList.map((s) => {
                if(s.name === skill){
                    const {_id, name} = {_id: s._id, name: s.name}
                    this.selectedSkills.push({_id, name})
                }
            })
        }
    }

    addCompetency(competency: string) {
        if (!this.selectedCompetencies.map(c => c.name).includes(competency)){
            this.selectedCompetencyNames.push(competency);
        }
        
        // this.competenciesList.map(c => {
        //     if(c.name === competency){
        //         this.selectedCompetencies.push(c)
        //     } 
        // })
        this.errorMinTags = this.selectedCompetencies.length + this.selectedCompetencyNames.length < this.minTags
    }

    removeCompetency(competency: string) {
        this.selectedCompetencies = this.selectedCompetencies.filter(c => c.name !== competency);
        this.selectedCompetencyNames = this.selectedCompetencyNames.filter(c => c !== competency);
        this.errorMinTags = this.selectedCompetencies.length + this.selectedCompetencyNames.length < this.minTags

    }

    addAdmin(admin: string){
        this.usersInfoList.map(u => {
            if (u.email === admin){
                const {_id, email} = {"_id": u._id, "email": u.email}
                this.adminsList.push({_id, email})
            } else {
                // eliminar tag
                this.multilanguageComponent.translate.stream('email-exists').subscribe(async (value) => {
                    const toast = await this.toastCtrl.create({
                        color: 'danger',
                        message: value,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                })
            }
        })
    }

    removeAdmin(admin: string){
        this.adminsList = this.adminsList.filter(a => a.email !== admin);
    }

    openInfoAlert() {
        this.isOpenInfoAlert = true
    }

    submitForm(){
        this.createCompetencies(this.selectedCompetencyNames);
        this.multilanguageComponent.translate.stream('saving').subscribe(async(value)=>{
          this.loadingCtrl.create({
            message: value,
            duration: 1000
          }).then((result)=>{
            result.present();  
            this.setInputValues();
            this.editProject(this.projectInput);
            this.onCloseHandler();
          })
        }) 
      }

    editProject(projectInput: InputProject){
        console.log('vamos a editar el proyecto: ', projectInput)
        this.projectsService.editProject(projectInput).subscribe((response) => {
            console.log('proyecto editado!', response)
        })
        // this.createCompetencies(competencies => {

        // })
        // let inputProject: InputProject = {
        //     userId: this.me._id,
        //     title: this.form.value.title || '',
        //     description: this.form.value.description || '',
        //     sector: this.selectedSectors || [],
        //     competencies: this.selectedCompetencies || [],
        //     city: this.form.value.city || '',
        //     softSkills: this.selectedSkills,
        //     state: OfferStatus.ACTIVE,
        //     status: this.form.value.status,           
        //     // files: this.filenames || [],
        //     admins: this.adminsList || []
        // }
    }

    createCompetencies(competencies) {
        if(this.form.valid && !this.errorMinTags){
          this.competenciesService.findOrCreateCompentencies(competencies).subscribe(
            {
              next: (competencies) => {
                competencies.map(c => {
                    this.selectedCompetencies.push(c)
                })
                
            },
              error: (error) => {
                console.log("Error creating competencies...")
                console.error(error)
              }
            }
          )
        }
    }

    onSelectFile(event): any {
        this.filenames.push(event.target.files[0].name)
        this.files.push(event.target.files[0]);
      }
    
      onDeleteFile(file): any {
        const index = this.filenames.indexOf(file);
        this.filenames.splice(index, 1);
      }

    

    onCloseInfoAlert(){
        this.isOpenInfoAlert = false
    }

    onCloseHandler() {
        this.onClose.emit();
    }

}