import { Component, effect, input, output } from "@angular/core";
import { SharedModule } from "../../shared.module";

@Component({
    standalone: true,
    selector: 'umu-modal',
    templateUrl: './modal.component.html',
    styleUrl:'./modal.component.css',
    imports: [SharedModule]
})
export class ModalComponent {
    isOpen = input.required<boolean>();
    footer = input<boolean>(true)
    title = input<string>('');
    iconName = input<string>('');
    customIconName = input<string>('');
    backdropDismiss = input<boolean>(true);
    onClose = output<void>();
    
    iconPath: string = '' 

    constructor(){
        effect(() => {
            this.iconPath = '/assets/icons/' + this.customIconName() + '.svg'
        })
    }

    onCloseHandler() {
        this.onClose.emit();
    }
}