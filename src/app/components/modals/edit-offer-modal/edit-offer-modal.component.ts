import { Component, inject, input, OnInit, output } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';
import { SelectDropdownComponent } from '../../forms/select-dropdown/select-dropdown.component';
import { TagInputComponent } from '../../forms/tag-input/tag-input.component';
import { TextButtonComponent } from '../../buttons/text-button/text-button.component';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { cityList, contractTypeOptions, createdDateOptions, offerFieldsValidators, remoteOptions, requiredExperienceLabels, requiredExperienceOptions, workHourOptions } from 'src/app/utils/constants';
import { CompetenciesService } from 'src/app/services/competencies.service';
import { OffersService } from 'src/app/services/offers/offers.service';
import { InputOffer, Offer } from 'src/app/models/offer';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user';
import { OfferStatus } from 'src/app/models/offer-status.model';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { AlertComponent } from '../alert/alert.component';
import { Competency } from 'src/app/models/competency.model';
import { UploadFileAlertComponent } from '../upload-file-alert/upload-file-alert.component';

@Component({
  standalone: true,
  selector: 'umu-edit-offer-modal',
  templateUrl: './edit-offer-modal.component.html',
  styleUrls: ['./edit-offer-modal.component.css'],
  imports: [
    IonicModule,
    TranslateModule,
    ModalComponent,
    ReactiveFormsModule,
    TextButtonComponent,
    TextFieldComponent,
    SelectDropdownComponent,
    TagInputComponent,
    AlertComponent,
    UploadFileAlertComponent
  ],
})
export class EditOfferModalComponent implements OnInit {

  offer = input.required<Offer | any>()

  competenciesNames: string[] = []
  offerCompetencies: string[] = []
  errorMinTags: boolean = false
  minTags: number = 2
  filenames: any[] = [];
  files: any[] = [];

  readonly cityList = cityList;
  readonly workHourOptions = workHourOptions;
  readonly contractTypeOptions = contractTypeOptions;
  readonly remoteOptions = remoteOptions;
  readonly createdDateOptions = createdDateOptions;
  readonly requiredExperienceOptions = requiredExperienceOptions;
  readonly requiredExperienceLabels = requiredExperienceLabels;
  readonly validators = offerFieldsValidators;
  translatedAndOrderedCities = []
  translatedAndOrderedReqExp = []
  selectedCompetencies: Competency[] = [];
  competenciesList: Competency[] = [];

  competenciesMinLength = 2
  competenciesMaxLength = 20

  isOpen = input.required<boolean>();
  onClose = output<void>();

  isOpenDeleteOffer = false
  isOpenDeactivateOffer = false
  isOpenUploadFileAlert = false

  umuTranslate = inject(UmuTranslateDirective)
  translateService = inject(TranslateService)
  competenciesService = inject(CompetenciesService)
  private formsBuilder = inject(FormBuilder)
  private offersService = inject(OffersService)
  private userService = inject(UserService)

  me: User

  form = this.formsBuilder.group({
    title: ['', [
      Validators.required,
      Validators.minLength(this.validators.titleMinLength),
      Validators.maxLength(this.validators.titleMaxLength),
    ]],
    eduLevel: ['', [
      Validators.required,
      Validators.minLength(this.validators.eduLevelMinLength),
      Validators.maxLength(this.validators.eduLevelMaxLength),
    ]],
    competencies: [[{} as Competency], [
      Validators.minLength(this.competenciesMinLength), 
      Validators.maxLength(this.competenciesMaxLength)
    ]],
    city: ['', [Validators.required]],
    contractType: ['', [Validators.required]],
    workHours: ['', [Validators.required]],
    remote: ['', [Validators.required]],
    requiredExperience: ['', [Validators.required]],
    salaryRange: ['', [
      Validators.required,
      Validators.minLength(this.validators.salaryRangeMinLength),
      Validators.maxLength(this.validators.salaryRangeMaxLength),
    ]],
    description: ['', [
      Validators.required,
      Validators.minLength(this.validators.descriptionMinLength),
      Validators.maxLength(this.validators.descriptionMaxLength),
    ]],
    requirements: ['', [
      Validators.required,
      Validators.minLength(this.validators.requirementsMinLength),
      Validators.maxLength(this.validators.requirementsMaxLength),
    ]]
  });

  ngOnInit(): void {
    this.userService.me.subscribe(me => this.me = me)
    this.competenciesService.competencies.subscribe(competencies => {
      if (competencies) {
        this.competenciesList = [...competencies]
        this.competenciesNames = [...competencies]
          .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
      }
    })
    this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
    this.umuTranslate.translateTextWithParams(this.requiredExperienceLabels).subscribe(list => this.translatedAndOrderedReqExp = list)
    if(this.offer()){
      this.offer().competencies.map(c => {
        const {_id, name} = {_id: c._id, name: c.name}
        this.selectedCompetencies.push({_id, name})
      })  
    }
    this.setValues(this.offer())
    // TODO: hacer esto mas eficiente (las opciones del dropdown tienen que ser JUNIOR, MID, SENIOR, no la lista de traducciones)
    // revisar tambien el setValues()
    this.form.valueChanges.subscribe(()=>{
      const requiredExperience = this.form.value.requiredExperience
      if (requiredExperience === 'less-value'){
        this.form.value.requiredExperience = 'JUNIOR'
      }
      if (requiredExperience === 'between-min-max'){
        this.form.value.requiredExperience = 'MID'
      }
      if (requiredExperience === 'more-value'){
        this.form.value.requiredExperience = 'SENIOR'
      }
    })
  }

  setValues(offer) {
    this.form.get('title').setValue(offer.title);
    this.form.get('eduLevel').setValue(offer.eduLevel);
    this.form.get('city').setValue(offer.city);
    this.form.get('competencies').setValue(this.selectedCompetencies);
    this.form.get('salaryRange').setValue(offer.salaryRange);
    this.form.get('remote').setValue(offer.remote);
    this.form.get('contractType').setValue(offer.contractType);
    this.form.get('workHours').setValue(offer.workHours);
    this.form.get('description').setValue(offer.description);
    this.form.get('requirements').setValue(offer.requirements);
    if(offer.requiredExperience === 'JUNIOR'){
      this.form.get('requiredExperience').setValue('less-value');
    }
    if(offer.requiredExperience === 'MID'){
      this.form.get('requiredExperience').setValue('between-min-max');
    }
    if(offer.requiredExperience === 'SENIOR'){
      this.form.get('requiredExperience').setValue('more-value');
    }
  }

  get controls() {
    return this.form.controls;
  }

  onCloseHandler() {
    this.onClose.emit();
  }

  onClickDeleteOfferHanlder() {
    this.isOpenDeleteOffer = true
  }
  onCloseDeleteOfferHandler() {
    this.isOpenDeleteOffer = false
  }
  onClickDeactivateOfferHanlder() {
    this.isOpenDeactivateOffer = true
  }
  onCloseDeactivateOfferHandler() {
    this.isOpenDeactivateOffer = false
  }

  deleteOffer() {
    console.log("Delete offer...")
  }

  updateOffer() {
    this.createCompetencies(competencies => {
      console.log("comp",competencies)
      // TODO: Legacy file format control with toast, revisit
      
      // for (let file of this.filenames) {
      //   if (file.split('.').pop() === 'pdf') {
      //   }
      //   else {
      //     this.loadingCtrl.dismiss();
      //     this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
      //       const toast = await this.toastCtrl.create({
      //         color: 'dark',
      //         message: value,
      //         duration: 3000,
      //         position: 'middle'
      //       });
      //       await toast.present();
      //     });
      //     return;
      //   }
      // }
      
      let inputOffer: InputOffer = {
        id: this.offer()._id,
        title: this.form.value.title || '',
        eduLevel: this.form.value.eduLevel || '',
        salaryRange: this.form.value.salaryRange || '',
        contractType: this.form.value.contractType || '',
        city: this.form.value.city || '',
        workHours: this.form.value.workHours || '',
        remote: this.form.value.remote || '',
        requirements: this.form.value.requirements || '',
        requiredExperience: RequiredExperience[this.form.value.requiredExperience] || RequiredExperience.JUNIOR,
        description: this.form.value.description || '',
        userId: this.me._id,
        createdDate: new Date().toISOString(),
        enrolled: 0,
        status: OfferStatus.ACTIVE,
        competencies: competencies || [],
        file: this.filenames || []
      }

      // TODO: refactor file upload
      this.offersService.editOffer(inputOffer).subscribe((offer: Offer) => {
        for (let file of this.files){
          this.offersService.uploadOfferFile(offer._id, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadOfferUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name ,'subido correctamente!' )
              }
            });
          });
        } 
        this.onCloseHandler()
      })
    })
  }

  addCompetency(competency: string) {
    this.competenciesList.map(c => {
      if(c.name === competency){
        this.selectedCompetencies.push(c)
      }
    })
    this.errorMinTags = this.selectedCompetencies.length < this.minTags
    this.form.value.competencies = this.selectedCompetencies
  }

  removeCompetency(competency: string) {
    this.selectedCompetencies = this.selectedCompetencies.filter(c => c.name !== competency);
      this.errorMinTags = this.selectedCompetencies.length < this.minTags
      this.form.value.competencies = this.selectedCompetencies
  }

  // TODO: pending refactor, revisit
  createCompetencies(callback: Function) {
    if (this.form.valid && !this.errorMinTags) {
      this.competenciesService.findOrCreateCompentencies(this.offerCompetencies).subscribe(
        {
          next: (competencies) => { callback() },
          error: (error) => {
            console.log("Error creating competencies...")
            console.error(error)
          }
        }
      )
    }
  }

  onSelectFile(event): any {
    this.filenames.push(event.target.files[0].name)
    this.files.push(event.target.files[0]);
  }

  onDeleteFile(file): any {
    const index = this.filenames.indexOf(file);
    this.filenames.splice(index, 1);
  }

  openInfoAlert(){
    this.isOpenUploadFileAlert = true
  }

  onCloseInfoAlert() {
    this.isOpenUploadFileAlert = false
  }

}