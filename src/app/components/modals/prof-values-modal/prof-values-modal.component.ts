import { Component, inject, input, OnInit, output } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SoftSkill } from 'src/app/models/softskill.model';
import { SoftSkillsService } from 'src/app/services/softskills.service';
import { UserService } from 'src/app/services/user/user.service';
import { ModalComponent } from '../modal/modal.component';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { InfoOptionsListComponent } from '../../info/info-options-list/info-options-list.component';
import { TitleComponent } from '../../title/title.component';

@Component({
  standalone: true,
  selector: 'umu-profvalues-modal',
  templateUrl: './prof-values-modal.component.html',
  styleUrl: './prof-values-modal.component.css',
  imports: [
    IonicModule,
    TranslateModule, 
    ModalComponent, 
    TextButtonComponent, 
    TitleComponent, 
    InfoOptionsListComponent
  ], 
})
export class ProfValuesModalComponent implements OnInit {
    maxSoftSkills = 6;
    
    softSkillsList: SoftSkill[] = []
    userSkills: SoftSkill[] = []
    userSkillsIds: Set<string> = new Set()

    isModalOpen= input.required<boolean>();
    onClose = output<void>();
    
    umuTranslate = inject(UmuTranslateDirective)
    usersService = inject(UserService)
    softSkillsService = inject(SoftSkillsService)

    ngOnInit(): void {
      this.usersService.me.subscribe(me => {
        if(me.softSkills){
          this.userSkills = me.softSkills
          this.userSkillsIds = new Set(this.userSkills.map(s => s._id))
        }
      })
      this.softSkillsService.softSkills.subscribe((softSkills) => {
        this.softSkillsList = softSkills
      })
    }

    toggleSkill(id: string) {
      if(this.userSkillsIds.has(id)) {
        this.userSkillsIds.delete(id);
        this.userSkills = this.userSkills.filter(s => s._id !== id);
      } else {
        const skill = this.softSkillsList.find(s => s._id === id);
        if(skill) {
          this.userSkillsIds.add(id);
          this.userSkills = [...this.userSkills,{ _id: skill._id, name: skill.name }]
        }
      }
    }
   
    updateSkills() {
      this.usersService.updateUserSkills(this.userSkills.map(s => ({_id:s._id,name:s.name}))).subscribe(() => {
        this.onClose.emit();
      })
    }
    
    onCloseHandler() {
      this.onClose.emit();
    }
  
  }