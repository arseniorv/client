import { SharedModule } from "../../shared.module";
import { Component, inject, input, output } from "@angular/core";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { LoadingController, ToastController } from "@ionic/angular";
import { UserService } from "src/app/services/user/user.service";
import { User } from "src/app/models/user";
import { TranslateService } from "@ngx-translate/core";

@Component({
    standalone: true,
    selector: 'umu-upload-file-alert',
    templateUrl: './upload-file-alert.component.html',
    styleUrl:'./upload-file-alert.component.css',
    imports: [SharedModule, TextButtonComponent]
})
export class UploadFileAlertComponent {

    // Component vars
    fileType = input.required<'CV' | 'Cover_Letter' | 'other'>() 
    isOpen = input.required<boolean>();
    onClose = output<void>();
    me: User

    userService = inject(UserService)
    toastController = inject(ToastController)
    loadingController = inject(LoadingController)
    translateService = inject(TranslateService)

    constructor() {
        this.userService.me.subscribe(me => this.me = me)
    }
    
    onCloseHandler() {
      this.onClose.emit();
    }

    onSelectFile(event): any {
      let file = event.target.files[0]; // Take first if many
      console.debug(`onSelectFile : expected: [${this.fileType()}]; got file :`);console.debug(file)
      
      if (file.name.split('.').pop() === 'pdf') { // Check extension is PDF
        const filename = `${this.fileType()}_${this.me.name.split(' ').join('_')}_${this.me.surname.split(' ').join('_')}.pdf`
        console.log(`onSelectFile : PDF format OK: generated filename : ${filename} ; calling userService.uploadFile()`)
        this.userService.uploadFile(filename,file,this.fileType(),() => {
          
          console.log(`Back from userService:uploadFile()`)
          
          this.onCloseHandler() // Close modal
        })
      } else { // Not PDF
        console.error(`Not a PDF: ${file?.name}`)
        this.loadingController.dismiss();
        
        // Show toast - invalid format
        this.translateService.get('toast-files-should-be-pdf').subscribe(async (value) => {
          const toast = await this.toastController.create({
            color: 'dark',
            message: value,
            duration: 3000,
            position: 'middle'
          });
          toast.present();
        });  
        return;
      }
    }

    async success() {
      this.translateService.get('toast-file-updated-successfully').subscribe(async (value) => {
        const toast = await this.toastController.create({
          color: 'dark',
          message: value,
          duration: 3000,
          position: 'middle',
          
        });
    
        await toast.present();
      })
    }
}