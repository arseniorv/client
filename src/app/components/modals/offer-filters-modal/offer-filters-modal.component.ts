import { Component, inject, input, OnInit, output } from "@angular/core";
import { FormBuilder, ReactiveFormsModule } from "@angular/forms";
import { Filters } from "src/app/models/filters.model";
import { OffersService } from "src/app/services/offers/offers.service";
import { cityList, contractTypeOptions, createdDateOptions, remoteOptions, requiredExperienceLabels, requiredExperienceOptions, workHourOptions } from "src/app/utils/constants";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { SharedModule } from "../../shared.module";
import { SelectCheckboxComponent } from "../../forms/select-checkbox/select-checkbox.component";
import { SelectDropdownComponent } from "../../forms/select-dropdown/select-dropdown.component";
import { ModalComponent } from "../modal/modal.component";
import { UmuTranslateDirective, TranslatedItem } from "src/app/shared/directives/translate.directive";
import { TranslateService } from "@ngx-translate/core";

@Component({
  standalone: true,
  selector: 'umu-offer-filters-modal',
  templateUrl: './offer-filters-modal.component.html',
  styleUrl: './offer-filters-modal.component.css',
  imports: [
    ReactiveFormsModule,
    SharedModule,
    SelectDropdownComponent,
    SelectCheckboxComponent,
    TextButtonComponent,
    ModalComponent
  ]
})
export class OfferFiltersModalComponent implements OnInit {

  readonly cityList = cityList;
  readonly workHourOptions = workHourOptions;
  readonly contractTypeOptions = contractTypeOptions;
  readonly remoteOptions = remoteOptions;
  readonly createdDateOptions = createdDateOptions;
  readonly requiredExperienceOptions = requiredExperienceOptions;
  readonly requiredExperienceLabels = requiredExperienceLabels;
  translatedAndOrderedCities = []
  translatedAndOrderedReqExp = []

  umuTranslate = inject(UmuTranslateDirective)
  private formBuilder = inject(FormBuilder);
  private offersService = inject(OffersService);

  isOpen = input<boolean>(false);
  onClear: boolean;
  onClose = output<void>();

  translateService = inject(TranslateService)

  filtersForm = this.formBuilder.group({
    createdDate: [undefined],
    city: [undefined],
    contractType: [undefined],
    workHours: [undefined],
    remote: [undefined],
    requiredExperience: [undefined]
  });

  ngOnInit() {
    this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => {
      console.log("other:", this.translateService.instant('update'))
      console.log("list:", list)
      this.translatedAndOrderedCities = list
    })
    this.umuTranslate.translateTextWithParams(this.requiredExperienceLabels).subscribe(list => {
      console.log("Result:", list)
      console.log("other:", this.translateService.instant('update'))
      this.translatedAndOrderedReqExp = list
    })
  }

  formatFilters() {
    let filters: any = {}

    if (this.filtersForm.value.city)
      filters.city = this.filtersForm.value.city
    if (this.filtersForm.value.createdDate) {
      filters.createdDate = new Date();
      filters.createdDate.setHours(0, 0, 0, 0)
      for (let date of this.filtersForm.value.createdDate) {
        if (date === 'last-24') {
          filters.createdDate.setDate(filters.createdDate.getDate() - 1);
        }
        if (date === 'last-7') {
          filters.createdDate.setDate(filters.createdDate.getDate() - 7);
        }
        if (date === 'last-15') {
          filters.createdDate.setDate(filters.createdDate.getDate() - 15);
        }
      }
    }
    if (this.filtersForm.value.workHours)
      filters.workHours = this.filtersForm.value.workHours
    if (this.filtersForm.value.contractType)
      filters.contractType = this.filtersForm.value.contractType
    if (this.filtersForm.value.remote)
      filters.remote = this.filtersForm.value.remote
    if (this.filtersForm.value.requiredExperience)
      filters.requiredExperience = this.filtersForm.value.requiredExperience


    if (Object.keys(filters).length == 0)
      return undefined;

    return filters
  }

  onFilter() {
    this.offersService.setFilters(this.formatFilters());
    this.offersService.fetchOffers()
    this.onClose.emit();
  }

  clearFilters() {
    this.filtersForm.reset();
    this.offersService.setFilters({} as Filters);
    this.offersService.fetchOffers()
    //Send signal to uncheck boxes
    this.onClear = true;
    //Close modal
    this.onClose.emit();
  }

  get controls() {
    return this.filtersForm.controls;
  }

  setValues(values: string[], formName: string) {
    this.onClear = false;
    if (values.length !== 0) {
      if (formName === 'createdDate') {
        this.filtersForm.value.createdDate = values
      }
      if (formName === 'contractType') {
        this.filtersForm.value.contractType = values
      }
      if (formName === 'remote') {
        this.filtersForm.value.remote = values
      }
      if (formName === 'requiredExperience') {
        this.filtersForm.value.requiredExperience = values
      }
    }
  }

  onCloseHandler() {
    this.onClose.emit();
  }
}