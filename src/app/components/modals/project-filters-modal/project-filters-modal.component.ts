import { Component, inject, input, OnInit, output } from "@angular/core";
import { ModalComponent } from "../modal/modal.component";
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared.module";
import { SelectDropdownComponent } from "../../forms/select-dropdown/select-dropdown.component";
import { SelectCheckboxComponent } from "../../forms/select-checkbox/select-checkbox.component";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { cityList, createdDateOptions, statusOptions } from "src/app/utils/constants";
import { MultilanguageComponent } from "src/app/partials/multilanguage/multilanguage.component";
import { SectorsService } from "src/app/services/sectors.service";
import { UmuTranslateDirective } from "src/app/shared/directives/translate.directive";
import { ProjectsService } from "src/app/services/projects/projects.service";
import { Filters } from "src/app/models/filters.model";

@Component({
  standalone: true,
  selector: 'umu-project-filters-modal',
  templateUrl: './project-filters-modal.component.html',
  styleUrl: './project-filters-modal.component.scss',
  imports: [
    ReactiveFormsModule,
    SharedModule,
    SelectDropdownComponent,
    SelectCheckboxComponent,
    TextButtonComponent,
    ModalComponent
  ]
})
export class ProjectFiltersModalComponent implements OnInit {

  isOpen = input<boolean>(false);
  onClear: boolean;
  onClose = output<void>();

  filters: FormGroup;
  readonly cityList = cityList;
  readonly createdDateOptions = createdDateOptions;
  readonly statusOptions = statusOptions;
  sectorList: any[] = [];
  sectorNameList: any[] = [];
  translatedAndOrderedSectors:any[] = [];
  
  finalCityList: any[] = [];

  private fBuilder = inject(FormBuilder);
  private translateList = inject(MultilanguageComponent);
  private sectorsService = inject(SectorsService);
  private umuTranslate = inject(UmuTranslateDirective);
  private projectsService = inject(ProjectsService);

  constructor(){}
  
  ngOnInit(){
    this.filters = this.fBuilder.group({
        createdDate: [undefined],
        city: [undefined],
        sector: [undefined],
        status:[undefined]
      });
    this.finalCityList = this.translateList.translateCityList();
    this.getSectors();

  }

  get controls() {
      return this.filters.controls;
  }

  getSectors(){
      this.sectorsService.sectors.subscribe((sectors) => {
          if(sectors !== undefined){
              this.sectorList = sectors
              this.sectorNameList = sectors.map(sector => sector.name)
              this.umuTranslate.translateAndOrder(this.sectorNameList).subscribe(list => {
                  this.translatedAndOrderedSectors = list
              })
          }
        })
  }

  onFilter(){
      this.projectsService.setFilters(this.formatFilters());
      this.onClose.emit();
  }

  formatFilters() {
      const filters: any = {}

      if (this.filters.value.city)
        filters.city = this.filters.value.city
      if (this.filters.value.createdDate) {
        filters.createdDate = new Date();
        filters.createdDate.setHours(0,0,0,0)
        for (let date of this.filters.value.createdDate){
          if (date === 'last-24'){
            filters.createdDate.setDate(filters.createdDate.getDate() - 1);
          }
          if (date === 'last-7'){
            filters.createdDate.setDate(filters.createdDate.getDate() - 7);
          }
          if (date === 'last-15'){
            filters.createdDate.setDate(filters.createdDate.getDate() - 15);
          }
        }
      }
      if (this.filters.value.sector){
        filters.sector = [];
          for(let sector of this.filters.value.sector){
              this.sectorList.map((s) => {
                  if(s.name === sector){
                      filters.sector.push(s)
                  }
              })
              
          }
      }
          
      
      if (this.filters.value.status)
          filters.status = this.filters.value.status
      
      if(Object.keys(filters).length==0)
        return undefined;

      return filters 
    }
  
  clearFilters() {
      this.filters.reset();
      this.projectsService.setFilters({} as Filters);
      //Send signal to uncheck boxes
      this.onClear = true;
      //Close modal
      this.onClose.emit();
  }

  setValues(values: string[], formName: string){
      this.onClear = false;
      if (values.length !== 0){
        if(formName === 'createdDate'){
          this.filters.value.createdDate = values
        }
        if(formName === 'status'){
          this.filters.value.status = values
        }
      }
    }
  

  onCloseHandler() {
    this.onClose.emit();
  }
}