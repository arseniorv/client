import { Component, EventEmitter, inject, input, Output, output, signal } from "@angular/core";
import { ImageCropperModule } from "ngx-image-cropper";
import { ImageUtilsClass } from "src/app/utils/image-utils";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { SharedModule } from "../../shared.module";
import { ModalComponent } from "../modal/modal.component";
import { AuthService } from "src/app/services/auth.service";

@Component({
    standalone: true,
    selector: 'umu-avatar-modal',
    templateUrl: './avatar-modal.component.html',
    styleUrl: './avatar-modal.component.css',
    imports: [
        ModalComponent,
        SharedModule,
        TextButtonComponent,
        ImageCropperModule
]
})
export class AvatarModalComponent {

    isOpen= input<boolean>(false);
    avatarB64= input<string>("./../../../../assets/avatar.svg");
    avatar: string;
    imageChangedEvent: any = '';
    uploaded: string;
    onClose = output<void>();
    onNewAvatar = output<string>();

    private imageUtils = inject(ImageUtilsClass);
    public auth = inject(AuthService);

    displayInput(){
        let element = document.getElementById("upload-avatar");
        element.click();
    }
    
    onSelectNewAvatar(event): any {
        this.uploaded = "";
        this.imageChangedEvent = event;  
    }

    async imageCroppedHandler(event){
        this.avatar = await this.imageUtils.imageCroppedHandler(event);
    }

    setImageHandler(avatarB64: string){ 
        this.onNewAvatar.emit(avatarB64);
        this.onCloseHandler();
    }

    async onCloseHandler(){
        this.onClose.emit();
    }
    
}