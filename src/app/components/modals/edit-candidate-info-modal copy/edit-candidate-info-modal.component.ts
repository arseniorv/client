import { Component, inject, input, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule, LoadingController, ToastController } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { Sector } from 'src/app/models/sector.model';
import { InputUser, User } from 'src/app/models/user';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { CompetenciesService } from 'src/app/services/competencies.service';
import { MatchService } from 'src/app/services/match.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { UserService } from 'src/app/services/user/user.service';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { cityList, contractTypeOptions, languageList, remoteOptions, workHourOptions } from 'src/app/utils/constants';
import { TextButtonComponent } from '../../buttons/text-button/text-button.component';
import { SelectDropdownComponent } from '../../forms/select-dropdown/select-dropdown.component';
import { TagInputComponent } from '../../forms/tag-input/tag-input.component';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';
import { ModalComponent } from '../modal/modal.component';
import { Competency } from 'src/app/models/competency.model';

@Component({
  standalone: true,
  selector: 'umu-edit-candidate-info-modal',
  templateUrl: './edit-candidate-info-modal.component.html',
  styleUrls: ['./edit-candidate-info-modal.component.css'],
  imports: [
    IonicModule, 
    TranslateModule, 
    ModalComponent, 
    ReactiveFormsModule,
    TextButtonComponent,
    TextFieldComponent, 
    SelectDropdownComponent,
    TagInputComponent,
  ],
})
export class EditCandidateInfoModalComponent implements OnInit {
   
    nameMinLength = 2
    nameMaxLength = 30
    surnameMinLength = 2
    surnameMaxLength = 30
    emailMinLength = 8
    emailMaxLength = 60
    eduLevelMinLength = 5
    eduLevelMaxLength = 50
    jobPosMinLength = 4
    jobPosMaxLength = 70
    lastJobMinLength = 20
    lastJobMaxLength = 1000
    competenciesMinLength = 2
    competenciesMaxLength = 20
    langMinLength = 2
    langMaxLength = 30
    converLetterMaxLength = 2000

    me: User;


    
    errorMinTags: boolean = false
    minTags: number = 2

    sectorList = []
    sectorNameList = []
    selectedSectors: Sector[] = [];
    translatedAndOrderedSectors = []
    readonly cityList = cityList;
    readonly langList = languageList; 
    translatedAndOrderedCities = []
    translatedAndOrderedLangs = []
    competenciesNames: string[] = []
    userCompetencies: string[] = []
    competenciesList: Competency[] = [];
    selectedCompetencies: Competency[] = [];

    userInput: InputUser = {} as InputUser

    isOpen= input.required<boolean>();
    onClose = output<void>();
    
    umuTranslate = inject(UmuTranslateDirective)
    translateService = inject(TranslateService)
    competenciesService = inject(CompetenciesService)
    private formsBuilder = inject(FormBuilder);
    private sectorsService = inject(SectorsService);
    private competService = inject(CompetenciesService);
    private userService = inject(UserService);
    private multilanguageComponent = inject(MultilanguageComponent);
    private matchService = inject(MatchService);
    private loadingCtrl = inject(LoadingController);
    private toastCtrl = inject(ToastController); 

    form = this.formsBuilder.group({
      name: ['',[
        Validators.required,
        Validators.minLength(this.nameMinLength),
        Validators.maxLength(this.nameMaxLength),
      ]],
      surname: ['',[
        Validators.required,
        Validators.minLength(this.surnameMinLength),
        Validators.maxLength(this.surnameMaxLength),
      ]],
      email: ['', [
        Validators.required, 
        Validators.minLength(this.emailMinLength), 
        Validators.maxLength(this.emailMaxLength)
      ]],
      city: ['', [Validators.required]],
      eduLevel: ['',[
        Validators.required,
        Validators.minLength(this.eduLevelMinLength),
        Validators.maxLength(this.eduLevelMaxLength),
      ]],
      jobPosition:['', [
        Validators.required,  
        Validators.minLength(this.jobPosMinLength), 
        Validators.maxLength(this.jobPosMaxLength)
      ]],
      experience: [0, []],
      lastJobTasks:['', [
        Validators.required,  
        Validators.minLength(this.lastJobMinLength), 
        Validators.maxLength(this.lastJobMaxLength)
      ]],
      competencies: [[{} as Competency], [
        Validators.minLength(this.competenciesMinLength), 
        Validators.maxLength(this.competenciesMaxLength)
      ]],
      lang: [[''], [
        Validators.required,  
        Validators.minLength(this.langMinLength), 
        Validators.maxLength(this.langMaxLength)
      ]],
      sector: [[''], [Validators.required]],
      coverLetter:['', [Validators.maxLength(this.converLetterMaxLength)]],
      video: ['']
    });

    constructor(){
      this.userService.me.subscribe((me)=> {
        this.selectedCompetencies = []
        this.me = me;
        if(me._id){
          me.competencies.map(c => {
            const {_id, name} = {_id: c._id, name: c.name}
            this.selectedCompetencies.push({_id, name})
          })  
          this.setValues(me);
        }
            
      })
      this.form.valueChanges.subscribe(()=>{
        this.setInputValues();
      })
    }

    ngOnInit(): void {
      this.competenciesService.competencies.subscribe(competencies => {
        if(competencies){
          this.competenciesList = [...competencies]
          this.competenciesNames = [...competencies]
            .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
        }
      })
      this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
      this.umuTranslate.translateAndOrder(this.langList).subscribe(list => this.translatedAndOrderedLangs = list)
      this.getSectors();
    }

    get controls() {
      return this.form.controls;
    }

    getSectors(){
      this.sectorsService.sectors.subscribe((sectors) => {
        if(sectors !== undefined){
          this.sectorList = sectors
          this.sectorNameList = sectors.map(sector => sector.name)
          this.umuTranslate.translateAndOrder(this.sectorNameList).subscribe(list => this.translatedAndOrderedSectors = list)
        }
      })
    }

    setValues(me){
      this.form.get('name').setValue(me.name),
      this.form.get('surname').setValue(me.surname),
      this.form.get('email').setValue(me.email),
      this.form.get('city').setValue(me.city),
      this.form.get('eduLevel').setValue(me.eduLevel),
      this.form.get('jobPosition').setValue(me.jobPosition)
      this.form.get('experience').setValue(Number(me.experience)),
      this.form.get('lastJobTasks').setValue(me.lastJobTasks),
      this.form.get('competencies').setValue(this.selectedCompetencies)
      this.form.get('lang').setValue(me.languages),
      this.form.get('sector').setValue(me.sector.map(sector => sector.name)),
      this.form.get('coverLetter').setValue(me.coverLetter),
      this.form.get('video').setValue(me.video)
    }

    onCloseHandler() {
      this.onClose.emit();
    }
  
    createOffer() {
      // this.createCompetencies()
    }

    addCompetency(competency: string) {
      this.competenciesList.map(c => {
        if(c.name === competency){
          this.selectedCompetencies.push(c)
        }
      })
      this.errorMinTags = this.selectedCompetencies.length < this.minTags
      this.form.value.competencies = this.selectedCompetencies
    }
  
    removeCompetency(competency: string) {
      this.selectedCompetencies = this.selectedCompetencies.filter(c => c.name !== competency);
      this.errorMinTags = this.selectedCompetencies.length < this.minTags
      this.form.value.competencies = this.selectedCompetencies
    }

    createCompetencies() {
      if(this.form.valid && !this.errorMinTags){
        this.competenciesService.findOrCreateCompentencies(this.userCompetencies).subscribe(
          {
            next: (competencies) => {},
            error: (error) => {
              console.log("Error creating competencies...")
              console.error(error)
            }
          }
        )
      }
    }

    submitForm(){
      this.createCompetencies();
      this.multilanguageComponent.translate.stream('saving').subscribe(async(value)=>{
        this.loadingCtrl.create({
          message: value,
          duration: 1000
        }).then((result)=>{
          result.present();
          let sectors = []
          this.form.value.sector.forEach(sector => {
            const newSector = this.sectorList.find(s => sector === s.name)  
            sectors.push(newSector)
            this.selectedSectors = sectors
          })

          this.setInputValues();
          if(this.me.email !== this.form.value.email){
            this.validateEmail(this.form.value.email)
          } else {
            this.editUser(this.userInput);
          }
          this.dismissModal();
        })
      }) 
    }

    setInputValues(){
      this.userInput.name = this.form.value.name
      this.userInput.surname = this.form.value.surname
      this.userInput.email = this.form.value.email
      this.userInput.city = this.form.value.city
      this.userInput.eduLevel = this.form.value.eduLevel
      this.userInput.jobPosition = this.form.value.jobPosition
      this.userInput.experience = this.form.value.experience.toString(),
      this.userInput.lastJobTasks = this.form.value.lastJobTasks
      this.userInput.competencies = this.form.value.competencies
      this.userInput.languages = this.form.value.lang
      this.userInput.sector = this.selectedSectors
      this.userInput.coverLetter = this.form.value.coverLetter
      this.userInput.video = this.form.value.video
      
      
    }

    async validateEmail(email: string){
      this.userService.checkEmail(email).subscribe({
        next: (emailExists) => {
          if(emailExists) {
            this.loadingCtrl.dismiss();
            this.multilanguageComponent.translate.stream('email-exists').subscribe(async (value) => {
              const toast = await this.toastCtrl.create({
                color: 'danger',
                message: value,
                duration: 3000,
                position: 'bottom'
              });
            toast.present();
            })
          } 
          else {
            this.editUser(this.userInput);
          }
        }
      })
    }

    editUser(userInput: InputUser){
      this.userService.mEditUser(this.me._id, userInput)
      .subscribe(()=>{
        console.log('profile edited!')
        this.calculateMatches();
      })
    }

    calculateMatches(){
      this.matchService.calculateMyMatches().subscribe(()=>{
        console.log('match calculated!')
      })
    }

    dismissModal(){
      this.onClose.emit();
    }

  }