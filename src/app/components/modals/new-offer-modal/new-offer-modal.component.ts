import { Component, inject, input, OnInit, output } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';
import { SelectDropdownComponent } from '../../forms/select-dropdown/select-dropdown.component';
import { TagInputComponent } from '../../forms/tag-input/tag-input.component';
import { TextButtonComponent } from '../../buttons/text-button/text-button.component';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { cityList, contractTypeOptions, createdDateOptions, offerFieldsValidators, remoteOptions, requiredExperienceLabels, requiredExperienceOptions, workHourOptions } from 'src/app/utils/constants';
import { CompetenciesService } from 'src/app/services/competencies.service';
import { OffersService } from 'src/app/services/offers/offers.service';
import { InputOffer, Offer } from 'src/app/models/offer';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user';
import { OfferStatus } from 'src/app/models/offer-status.model';
import { RequiredExperience } from 'src/app/models/required-experience.model';

@Component({
  standalone: true,
  selector: 'umu-new-offer-modal',
  templateUrl: './new-offer-modal.component.html',
  styleUrls: ['./new-offer-modal.component.css'],
  imports: [
    IonicModule,
    TranslateModule,
    ModalComponent,
    ReactiveFormsModule,
    TextButtonComponent,
    TextFieldComponent,
    SelectDropdownComponent,
    TagInputComponent
  ],
})
export class NewOfferModalComponent implements OnInit {

  competenciesNames: string[] = []
  offerCompetencies: string[] = []
  errorMinTags: boolean = true
  minTags: number = 2
  filenames: any[] = [];
  files: any[] = [];

  readonly cityList = cityList;
  readonly workHourOptions = workHourOptions;
  readonly contractTypeOptions = contractTypeOptions;
  readonly remoteOptions = remoteOptions;
  readonly createdDateOptions = createdDateOptions;
  readonly requiredExperienceOptions = requiredExperienceOptions;
  readonly requiredExperienceLabels = requiredExperienceLabels;
  readonly validators = offerFieldsValidators;
  translatedAndOrderedCities = []
  translatedAndOrderedReqExp = []

  isOpen = input.required<boolean>();
  onClose = output<void>();

  umuTranslate = inject(UmuTranslateDirective)
  translateService = inject(TranslateService)
  competenciesService = inject(CompetenciesService)
  private formsBuilder = inject(FormBuilder)
  private offersService = inject(OffersService)
  private userService = inject(UserService)

  me: User

  form = this.formsBuilder.group({
    title: ['', [
      Validators.required,
      Validators.minLength(this.validators.titleMinLength),
      Validators.maxLength(this.validators.titleMaxLength),
    ]],
    eduLevel: ['', [
      Validators.required,
      Validators.minLength(this.validators.eduLevelMinLength),
      Validators.maxLength(this.validators.eduLevelMaxLength),
    ]],
    city: ['', [Validators.required]],
    contractType: ['', [Validators.required]],
    workHours: ['', [Validators.required]],
    remote: ['', [Validators.required]],
    requiredExperience: ['', [Validators.required]],
    salaryRange: ['', [
      Validators.required,
      Validators.minLength(this.validators.salaryRangeMinLength),
      Validators.maxLength(this.validators.salaryRangeMaxLength),
    ]],
    description: ['', [
      Validators.required,
      Validators.minLength(this.validators.descriptionMinLength),
      Validators.maxLength(this.validators.descriptionMaxLength),
    ]],
    requirements: ['', [
      Validators.required,
      Validators.minLength(this.validators.requirementsMinLength),
      Validators.maxLength(this.validators.requirementsMaxLength),
    ]]
  });

  ngOnInit(): void {
    this.userService.me.subscribe(me => this.me = me)
    this.competenciesService.competencies.subscribe(competencies => {
      if (competencies) {
        this.competenciesNames = [...competencies]
          .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
      }
    })
    this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
    this.umuTranslate.translateTextWithParams(this.requiredExperienceLabels).subscribe(list => this.translatedAndOrderedReqExp = list)
  }

  get controls() {
    return this.form.controls;
  }

  onCloseHandler() {
    this.onClose.emit();
  }

  createOffer() {
    this.createCompetencies(competencies => {
      console.log("comp",competencies)
      // TODO: Legacy file format control with toast, revisit
      
      // for (let file of this.filenames) {
      //   if (file.split('.').pop() === 'pdf') {
      //   }
      //   else {
      //     this.loadingCtrl.dismiss();
      //     this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
      //       const toast = await this.toastCtrl.create({
      //         color: 'dark',
      //         message: value,
      //         duration: 3000,
      //         position: 'middle'
      //       });
      //       await toast.present();
      //     });
      //     return;
      //   }
      // }
      
      let inputOffer: InputOffer = {
        title: this.form.value.title || '',
        eduLevel: this.form.value.eduLevel || '',
        salaryRange: this.form.value.salaryRange || '',
        contractType: this.form.value.contractType || '',
        city: this.form.value.city || '',
        workHours: this.form.value.workHours || '',
        remote: this.form.value.remote || '',
        requirements: this.form.value.requirements || '',
        requiredExperience: RequiredExperience[this.form.value.requiredExperience] || RequiredExperience.JUNIOR,
        description: this.form.value.description || '',
        userId: this.me._id,
        createdDate: new Date().toISOString(),
        enrolled: 0,
        status: OfferStatus.ACTIVE,
        competencies: competencies || [],
        file: this.filenames || []
      }

      // TODO: refactor file upload
      this.offersService.createOffer(inputOffer).subscribe((offer: Offer) => {
        for (let file of this.files){
          this.offersService.uploadOfferFile(offer._id, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadOfferUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name ,'subido correctamente!' )
              }
            });
          });
        } 
        this.onCloseHandler()
      })
    })
  }

  addCompetency(competency: string) {
    this.offerCompetencies.push(competency)
    this.errorMinTags = this.offerCompetencies.length < this.minTags
  }

  removeCompetency(competency: string) {
    this.offerCompetencies = this.offerCompetencies.filter(c => c !== competency);
    this.errorMinTags = this.offerCompetencies.length < this.minTags
  }

  // TODO: pending refactor, revisit
  createCompetencies(callback: Function) {
    if (this.form.valid && !this.errorMinTags) {
      this.competenciesService.findOrCreateCompentencies(this.offerCompetencies).subscribe(
        {
          next: (competencies) => { 
            callback(competencies)
          },
          error: (error) => {
            console.log("Error creating competencies...")
            console.error(error)
          }
        }
      )
    }
  }

  onSelectFile(event): any {
    this.filenames.push(event.target.files[0].name)
    this.files.push(event.target.files[0]);
  }

  onDeleteFile(file): any {
    const index = this.filenames.indexOf(file);
    this.filenames.splice(index, 1);
  }

  openInfoAlert(){}



}