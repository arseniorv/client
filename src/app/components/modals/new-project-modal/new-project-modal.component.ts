import { Component, inject, input, output } from "@angular/core";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { InputProject, Member } from "src/app/models/project";
import { CompetenciesService } from "src/app/services/competencies.service";
import { SectorsService } from "src/app/services/sectors.service";
import { UmuTranslateDirective } from "src/app/shared/directives/translate.directive";
import { cityList, statusOptions } from "src/app/utils/constants";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { SelectDropdownComponent } from "../../forms/select-dropdown/select-dropdown.component";
import { TagInputComponent } from "../../forms/tag-input/tag-input.component";
import { TextFieldComponent } from "../../forms/text-field/text-field.component";
import { SharedModule } from "../../shared.module";
import { ModalComponent } from "../modal/modal.component";
import { UserService } from "src/app/services/user/user.service";
import { User } from "src/app/models/user";
import { OfferStatus } from "src/app/models/offer-status.model";
import { ProjectsService } from "src/app/services/projects/projects.service";
import { SoftSkillsService } from "src/app/services/softskills.service";
import { map } from "rxjs";
import { MultilanguageComponent } from "src/app/partials/multilanguage/multilanguage.component";
import { UploadFileAlertComponent } from "../upload-file-alert/upload-file-alert.component";
import { MatchService } from "src/app/services/match.service";


@Component({
    standalone: true,
    selector: 'umu-new-project-modal',
    templateUrl: './new-project-modal.component.html',
    styleUrl: './new-project-modal.component.scss',
    imports: [
    SharedModule,
    ReactiveFormsModule,
    ModalComponent,
    TextFieldComponent,
    SelectDropdownComponent,
    TagInputComponent,
    TextButtonComponent,
    UploadFileAlertComponent
]
})
export class NewProjectModalComponent {

    isOpen = input.required<boolean>();
    onClose = output<void>();

    private formsBuilder = inject(FormBuilder);
    private competenciesService = inject(CompetenciesService);
    private translateService = inject(TranslateService);
    private umuTranslate = inject(UmuTranslateDirective);
    private sectorsService = inject(SectorsService);
    private userService = inject(UserService);
    private projectsService = inject(ProjectsService);
    private softskillsService = inject(SoftSkillsService);
    private multilanguageComponent = inject(MultilanguageComponent);
    private matchService = inject(MatchService);

    readonly cityList = cityList;
    readonly statusOptions = statusOptions;

    titleMinLength = 2
    titleMaxLength = 100
    descriptionMinLength = 40
    descriptionMaxLength = 1000

    competenciesNames: string[] = []
    translatedAndOrderedCities = []
    sectorList: any[] = [];
    sectorNameList: any[] = [];
    translatedAndOrderedSectors:any[] = [];
    selectedSectors: any[] = [];
    selectedSkills: any[] = [];
    projectCompetencies: string[] = []
    errorMinTags: boolean = true
    minTags: number = 2
    me: User
    finalSkillsList: {value: any, label: any} [] = [];
    translatedSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
    skillsList: any[] = [];
    usersList: any[] = [];
    usersInfoList: any[] = [];
    isOpenInfoAlert: boolean = false
    filenames: any[] = [];
    files: any[] = [];
    adminsList: Member[] = []

    form = this.formsBuilder.group({
        title: ['', [
            Validators.required, 
            Validators.minLength(this.titleMinLength), 
            Validators.maxLength(this.titleMaxLength)
        ]],
        description: ['', [
            Validators.required,  
            Validators.minLength(this.descriptionMinLength), 
            Validators.maxLength(this.descriptionMaxLength)
        ]],
        sector: ['', [Validators.required]],
        city: [''],
        softskills: ['', [Validators.required]],
        status: ['', [Validators.required]],
        admins: [''] 
    });

    constructor(){
        this.form.valueChanges.subscribe(()=>{
            this.addSector(this.form.value.sector);
            this.addSoftSkill(this.form.value.softskills);
        })
    }

    ngOnInit(){
        this.userService.me.subscribe(me => {
            this.me = me;
            if (me._id && this.adminsList.length === 0){
                const {_id, email} = {"_id": me._id, "email": me.email}
                this.adminsList.push({_id, email});
            }
           
        })
        this.competenciesService.competencies.subscribe(competencies => {
            if (competencies) {
              this.competenciesNames = [...competencies]
                .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
            }
        })
        this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list);
        this.getSectors();
        this.getSoftskills();
        this.getAllUsers();
    }

    get controls() {
        return this.form.controls;
    }

    getSectors(){
        this.sectorsService.sectors.subscribe((sectors) => {
            if(sectors !== undefined){
                this.sectorList = sectors
                this.sectorNameList = sectors.map(sector => sector.name)
                this.umuTranslate.translateAndOrder(this.sectorNameList).subscribe(list => {
                    this.translatedAndOrderedSectors = list
                })
            }
          })
    }

    getSoftskills() {
        this.softskillsService.qGetAllSkills().valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
          this.skillsList = item.getSoftskills;
          this.translatedSkillsList = this.multilanguageComponent.translateSkillsLists(this.skillsList)
          for (let skill of this.translatedSkillsList){
            this.finalSkillsList.push({"value": skill.name, "label": skill.label})
          }
        });
    
    }

    getAllUsers(){
        this.userService.qGetAllUsers().valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
            this.usersInfoList = item.getUsers
            this.usersList = item.getUsers.map(u => u.email);
        })
      }

    addSector(sectors){
        for (let sector of sectors){
            this.sectorList.map((s) => {
                if(s.name === sector){
                    this.selectedSectors.push(s)
                }
            })
        }
    }

    addSoftSkill(softskills){
        for (let skill of softskills){
            this.skillsList.map((s) => {
                if(s.name === skill){
                    const {_id, name} = {_id: s._id, name: s.name}
                    this.selectedSkills.push({_id, name})
                }
            })
        }
    }

    addCompetency(competency: string) {
        this.projectCompetencies.push(competency)
        this.errorMinTags = this.projectCompetencies.length < this.minTags
    }

    removeCompetency(competency: string) {
        this.projectCompetencies = this.projectCompetencies.filter(c => c !== competency);
        this.errorMinTags = this.projectCompetencies.length < this.minTags
    }

    addAdmin(admin: string){
        this.usersInfoList.map(u => {
            if (u.email === admin){
                const {_id, email} = {"_id": u._id, "email": u.email}
                this.adminsList.push({_id, email})
            }
        })
    }

    removeAdmin(admin: string){
        this.adminsList = this.adminsList.filter(a => a.email !== admin);
    }

    calculateMatch(projectID){
        this.matchService.mCalculateMatchByProject(projectID).subscribe(() => {
          console.log('Match calculated!');
        });
    
      }

    openInfoAlert() {
        this.isOpenInfoAlert = true
    }

    onCloseInfoAlert(){
        this.isOpenInfoAlert = false
    }

    createProject() {
        this.createCompetencies(competencies => {
          // TODO: Legacy file format control with toast, revisit
          
          // for (let file of this.filenames) {
          //   if (file.split('.').pop() === 'pdf') {
          //   }
          //   else {
          //     this.loadingCtrl.dismiss();
          //     this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
          //       const toast = await this.toastCtrl.create({
          //         color: 'dark',
          //         message: value,
          //         duration: 3000,
          //         position: 'middle'
          //       });
          //       await toast.present();
          //     });
          //     return;
          //   }
          // }
          if(this.adminsList.length === 0){
            this.adminsList.push({_id: this.me._id, email: this.me.email});
          }
          
          
          let inputProject: InputProject = {
            userId: this.me._id,
            title: this.form.value.title || '',
            description: this.form.value.description || '',
            sector: this.selectedSectors || [],
            competencies: competencies || [],
            city: this.form.value.city || '',
            softSkills: this.selectedSkills,
            state: OfferStatus.ACTIVE,
            status: this.form.value.status,
            createdDate: new Date().toISOString(),            
            files: this.filenames || [],
            admins: this.adminsList || []
          }
    
          // TODO: refactor file upload
          this.projectsService.createProject(inputProject).subscribe((response) => {
            this.calculateMatch(response.createProject._id)
            // for (let file of this.files){
            //   this.offersService.uploadOfferFile(offer._id, file.name).subscribe(response => {
            //     const formData = new FormData();
            //     formData.append('file', file)
            //     fetch(response.data.uploadOfferUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
            //       if (response.ok) {
            //         console.log('Archivo con nombre', file.name ,'subido correctamente!' )
            //       }
            //     });
            //   });
            // } 
            this.onCloseHandler()
          })
        })
      }
    
    createCompetencies(callback: Function) {
        if (this.form.valid && !this.errorMinTags) {
            this.competenciesService.findOrCreateCompentencies(this.projectCompetencies).subscribe(
            {
                next: (competencies) => { 
                callback(competencies)
                },
                error: (error) => {
                console.log("Error creating competencies...")
                console.error(error)
                }
            }
            )
        }
    }

    onSelectFile(event): any {
        this.filenames.push(event.target.files[0].name)
        this.files.push(event.target.files[0]);
      }
    
      onDeleteFile(file): any {
        const index = this.filenames.indexOf(file);
        this.filenames.splice(index, 1);
      }

    onCloseHandler() {
        this.onClose.emit();
    }
}