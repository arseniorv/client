import { Component, inject, input, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule, LoadingController, ToastController } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CompetenciesService } from 'src/app/services/competencies.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { cityList, contractTypeOptions, remoteOptions, workHourOptions } from 'src/app/utils/constants';
import { TextButtonComponent } from '../../buttons/text-button/text-button.component';
import { SelectDropdownComponent } from '../../forms/select-dropdown/select-dropdown.component';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';
import { ModalComponent } from '../modal/modal.component';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { Sector } from 'src/app/models/sector.model';
import { UserService } from 'src/app/services/user/user.service';
import { InputUser, User } from 'src/app/models/user';
import { MatchService } from 'src/app/services/match.service';

@Component({
  standalone: true,
  selector: 'umu-edit-company-info-modal',
  templateUrl: './edit-company-info-modal.component.html',
  styleUrls: ['./edit-company-info-modal.component.css'],
  imports: [
    IonicModule, 
    TranslateModule, 
    ModalComponent, 
    ReactiveFormsModule,
    TextButtonComponent,
    TextFieldComponent, 
    SelectDropdownComponent,
  ],
})
export class EditCompanyInfoModalComponent implements OnInit {
   
    nameMinLength = 2
    nameMaxLength = 30
    websiteMinLength = 0
    websiteMaxLength = 100
    emailMinLength = 8
    emailMaxLength = 60
    eduLevelMinLength = 5
    eduLevelMaxLength = 50
    jobDetailsMinLength = 20
    jobDetailsMaxLength = 1000


    competenciesNames: string[] = []
    offerCompetencies: string[] = []
    errorMinTags: boolean = true
    minTags: number = 4
    sectorList = []
    sectorNameList = []
    translatedAndOrderedSectors = []

    readonly workHourOptions = workHourOptions;
    readonly contractTypeOptions = contractTypeOptions;
    readonly remoteOptions = remoteOptions;

    readonly cityList = cityList;
    translatedAndOrderedCities = []

    isOpen= input.required<boolean>();
    onClose = output<void>();
    selectedSectors: Sector[] = [];
    me: User;
    userInput: InputUser = {} as InputUser
    
    umuTranslate = inject(UmuTranslateDirective)
    translateService = inject(TranslateService)
    competenciesService = inject(CompetenciesService)
    private formsBuilder = inject(FormBuilder);
    private sectorsService = inject(SectorsService);
    private loadingCtrl = inject(LoadingController);
    private toastCtrl = inject(ToastController); 
    private multilanguageComponent = inject(MultilanguageComponent);
    private userService = inject(UserService);
    private matchService = inject(MatchService);

    form = this.formsBuilder.group({
      name: ['',[
        Validators.required,
        Validators.minLength(this.nameMinLength),
        Validators.maxLength(this.nameMaxLength),
      ]],
      website: ['', [
        Validators.minLength(this.websiteMinLength), 
        Validators.maxLength(this.websiteMaxLength)
      ]],
      email: ['', [
        Validators.required, 
        Validators.minLength(this.emailMinLength), 
        Validators.maxLength(this.emailMaxLength)
      ]],
      sector: [[''], [Validators.required]],
      experience: [0, []],
      city: ['', [Validators.required]],
      jobDetails: ['', [
        Validators.required,  
        Validators.minLength(this.jobDetailsMinLength), 
        Validators.maxLength(this.jobDetailsMaxLength)
      ]]
    });

    constructor(){
      this.userService.me.subscribe((me) => {
        this.me = me
        this.selectedSectors = me.sector
        if(me._id){
          this.setValues(me);
        } 
      })
      this.form.valueChanges.subscribe(()=>{
        this.setInputValues();
      })
    }

    ngOnInit(): void {
      this.competenciesService.competencies.subscribe(competencies => {
        if(competencies){
          this.competenciesNames = [...competencies]
            .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
        }
      })
      this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
      this.getSectors();
    }

    setValues(me){
      this.form.get('name').setValue(me.name),
      this.form.get('website').setValue(me.website),
      this.form.get('email').setValue(me.email),
      this.form.get('city').setValue(me.city),
      this.form.get('sector').setValue(me.sector.map(sector => sector.name)),
      this.form.get('experience').setValue(me.experience),
      this.form.get('jobDetails').setValue(me.lastJobTasks)
    }

    getSectors(){
      this.sectorsService.sectors.subscribe((sectors) => {
        if(sectors !== undefined){
          this.sectorList = sectors
          this.sectorNameList = sectors.map(sector => sector.name)
          this.umuTranslate.translateAndOrder(this.sectorNameList).subscribe(list => this.translatedAndOrderedSectors = list)
        }
      })
    }

    get controls() {
      return this.form.controls;
    }

    onCloseHandler() {
      this.onClose.emit();
    }
  
    submitForm(){
      this.multilanguageComponent.translate.stream('saving').subscribe(async(value)=>{
        this.loadingCtrl.create({
          message: value,
          duration: 1000
        }).then((result)=>{
          result.present();
          let sectors = []
          this.form.value.sector.forEach(sector => {
            const newSector = this.sectorList.find(s => sector === s.name)  
            sectors.push(newSector)
            this.selectedSectors = sectors
            this.setInputValues();
          })
          if(this.me.email !== this.form.value.email){
            this.validateEmail(this.form.value.email)
          } else {
            this.editUser(this.userInput);
          }
          this.dismissModal();
        })
      }) 
    }

    setInputValues(){
      this.userInput.name = this.form.value.name
      this.userInput.website = this.form.value.website
      this.userInput.email = this.form.value.email
      this.userInput.city = this.form.value.city
      this.userInput.sector = this.selectedSectors
      this.userInput.experience = this.form.value.experience.toString(),
      this.userInput.lastJobTasks = this.form.value.jobDetails
    }

    async validateEmail(email: string){
      this.userService.checkEmail(email).subscribe({
        next: (emailExists) => {
          if(emailExists) {
            this.loadingCtrl.dismiss();
            this.multilanguageComponent.translate.stream('email-exists').subscribe(async (value) => {
              const toast = await this.toastCtrl.create({
                color: 'danger',
                message: value,
                duration: 3000,
                position: 'bottom'
              });
            toast.present();
            })
          } 
          else {
            this.editUser(this.userInput);
          }
        }
      })
    }

    editUser(userInput: InputUser){
      this.userService.mEditUser(this.me._id, userInput)
      .subscribe(()=>{
        console.log('profile edited!')
        this.calculateMatches();
      })
    }

    calculateMatches(){
      this.matchService.calculateMyMatches().subscribe(()=>{
        console.log('match calculated!')
      })
    }

    dismissModal(){
      this.onClose.emit();
    }

  }