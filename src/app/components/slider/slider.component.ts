import { AfterViewInit, Component, ContentChildren, CUSTOM_ELEMENTS_SCHEMA, effect, ElementRef, input, output, QueryList, ViewChild } from '@angular/core';
import { SlideDirective } from './slide.directive';
import { CommonModule } from '@angular/common';

const swiperOpts = {
  slidesPerView: 1,
  allowTouchMove: false,
}

@Component({
  standalone: true,
  selector: 'umu-slider',
  templateUrl: './slider.component.html',
  imports: [CommonModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SliderComponent implements AfterViewInit {

  // Optional external control of the slider
  currentSlide = input<number>(0) 
  slideOpts = input<any>(swiperOpts)
  onSlideChange = output<number>()

  _currentSlide = 0

  @ViewChild('slider') slider: ElementRef | undefined;

  @ContentChildren(SlideDirective) slideItems: QueryList<SlideDirective>;

  constructor() {
    effect(() => {
      this.goToSlide(this.currentSlide())
    })
  }
  
  ngAfterViewInit(): void {
    Object.assign(this.slider.nativeElement,this.slideOpts())
    this.slider.nativeElement.initialize()

    this.slider.nativeElement.addEventListener('swiperslidechange', (event) => {
      this.onSlideChange.emit(event.detail[0].activeIndex)
    })
  }

  goToSlide(slide: number) {
    // If mother component has updated the input due to inner update ignore
    if(slide == this._currentSlide) {
      return
    }
    this._currentSlide = slide
    this.slider.nativeElement.swiper.slideTo(slide);
  }

}
