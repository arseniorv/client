import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[umuSlide]'
})
export class SlideDirective {

  slideTemplate: TemplateRef<any>;

  constructor(private templateRef: TemplateRef<any>) {
    this.slideTemplate = this.templateRef;
  }

}