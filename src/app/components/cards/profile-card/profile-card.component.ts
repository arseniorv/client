import { Component, input, output } from "@angular/core";
import { User } from "src/app/models/user";
import { SharedModule } from "../../shared.module";
import { TitleComponent } from "../../title/title.component";

@Component({
    standalone: true,
    selector: 'umu-profile-card',
    templateUrl: './profile-card.component.html',
    styleUrl: './profile-card.component.scss',
    imports: [
        SharedModule,
        TitleComponent
    ]
})
export class ProfileCardComponent {
    users = input.required<User[]>();
    navigateProfile = output<string>();
    title = input<string>('');

    goToProfile(event){
        this.navigateProfile.emit(event);
    }
}