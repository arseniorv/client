import { Component, inject, input } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { UserService } from "src/app/services/user/user.service";
import { SharedModule } from "../../shared.module";
import { OfferCardComponent } from "../offer-card/offer-card.component";
import { Offer } from "src/app/models/offer";

@Component({
  standalone: true,
  selector: 'umu-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
  imports: [
    SharedModule, 
    TranslateModule,
    OfferCardComponent 
  ]
})
export class CardListComponent {
  private userService = inject(UserService);

  items = input.required<any>();

  constructor(
  ){
    this.userService.me.subscribe((user) => {
     

    });
  
  }

  ngOnInit() {
  }

}


