import { Component, inject, input, OnInit, output } from "@angular/core";
import { InfoCardComponent } from "../info-card/info-card.component";
import { InfoTextListComponent } from "../../info/info-text-list/info-text-list.component";
import { EditCompanyInfoModalComponent } from "../../modals/edit-company-info-modal/edit-company-info-modal.component";
import { UserService } from "src/app/services/user/user.service";
import { Info } from "src/app/models/info.model";
import { User } from "src/app/models/user";

@Component({
    standalone: true,
    selector: 'umu-company-info-card',
    templateUrl: './company-info-card.component.html',
    styleUrl: './company-info-card.component.scss',
    imports: [
        InfoCardComponent,
        InfoTextListComponent,
        EditCompanyInfoModalComponent
    ]
})
export class CompanyInfoCardComponent {
    
    isOpen: boolean = false
    me: User;
    infoList: Info[] = [];

    private userService = inject(UserService)

    constructor(){
        this.userService.me.subscribe(me => {
            this.me = me;
            if(this.infoList.length === 0 && me._id){
              this.setUserInfo(me);
            }
        })
    }

    setUserInfo(me){
        this.infoList.push(
          {name: 'email', content: me.email},
          {name: 'website', content: me.website},
          {name: 'city', content: me.city},
          {name: 'sectors', content: me.sector},
          {name: 'age', content: me.experience},
          {name: 'description', content: me.lastJobTasks}
        );
    }
    
    onEditHandler(){
        this.isOpen = true
    }

    onClose(){
        this.isOpen = false
    }
}