import { Component, inject, input, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Message } from "src/app/models/message.model";
import { User } from "src/app/models/user";
import { ChatService } from "src/app/services/chat/chat.service";
import { SharedModule } from "../../shared.module";

@Component({
  standalone: true,
  selector: 'umu-conversation-card',
  templateUrl: './conversation-card.component.html',
  styleUrls: ['./conversation-card.component.css'],
  imports: [SharedModule]
})
export class ConversationCardComponent implements OnInit {
  private chatService = inject(ChatService);
  private translate = inject(TranslateService);

  lastMessage = input.required<Message>();
  recipient = input.required<User>();
  unreadsCount = input.required<number>();

  date: string
  dateOptions: object = {
    month: 'long',
    day: 'numeric',
  }

  constructor(){}

  ngOnInit(): void {
    this.date = (new Date(this.lastMessage().timeSent)).toLocaleDateString(this.translate.currentLang, this.dateOptions)
  }

  onChanges() {
    this.date = (new Date(this.lastMessage().timeSent)).toLocaleDateString(this.translate.currentLang, this.dateOptions)
  }

  onCardClick() {
    this.chatService.openChat(this.recipient()._id)
  }

}