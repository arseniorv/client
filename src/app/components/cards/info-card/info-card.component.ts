import { Component, input, output } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { FabButtonComponent } from "../../buttons/fab-button/fab-button.component";
import { TitleComponent } from "../../title/title.component";

@Component({
    standalone: true,
    selector: 'umu-info-card',
    templateUrl: './info-card.component.html',
    styleUrl: './info-card.component.scss',
    imports: [
        TitleComponent,
        TranslateModule,
        FabButtonComponent
    ]
})
export class InfoCardComponent {
    
    extTitle = input<string>('');
    intTitle = input<string>('');
    isEditable = input<boolean>(false);
    iconPath: string = "../../../assets/icons/edit_icon.svg";
    addInfo = input<string>('');
    onEdit = output<void>();

    onEditHandler(){
        this.onEdit.emit();
    }
}