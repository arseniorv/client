import { Component, CUSTOM_ELEMENTS_SCHEMA, effect, inject, input, output } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MiniTagComponent } from "../../tags/mini-tag/mini-tag.component";
import { FabButtonComponent } from "../../buttons/fab-button/fab-button.component";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { ModalComponent } from "../../modals/modal/modal.component";
import { User } from "src/app/models/user";
import { CandidatureStatus } from "src/app/models/candidature-status.model";
import { OfferStatus } from "src/app/models/offer-status.model";
import { UserService } from "src/app/services/user/user.service";
import { Router } from "@angular/router";
import { StartChatButtonComponent } from "../../buttons/start-chat-button/start-chat-button.component";
import { CheckboxComponent } from "../../forms/checkbox/checkbox.component";
import { BreakpointObserver } from '@angular/cdk/layout';
import { Platform } from "@ionic/angular";
import { Subscription } from "rxjs";

@Component({
  standalone: true,
  selector: 'umu-candidate-card',
  templateUrl: './candidate-card.component.html',
  styleUrls: ['./candidate-card.component.scss'],
  imports: [SharedModule, MiniTagComponent, TranslateModule, TextButtonComponent, ModalComponent, StartChatButtonComponent, CheckboxComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CandidateCardComponent {
    private userService = inject(UserService);
    private router = inject(Router)
    

    candidate = input.required<any>();
    iconCheckCircle:string = "fits";
    iconNoCheckCircle:string = "no_fits";
    iconName:string = '';
    userLoggedSkills: any[] = [];
    sortUsersList: any[] = [];
    user: User;
    avatar: string;
    infoId:string;
    isPreselected:boolean = false;
    onGoCandidate= output<string>();
    textOnBtn1:string = '';
    textOnBtn2:string = '';
    typeOfCard: string = '';
    onClickFits = output<void>();
    onClickNoFits = output<void>();
    onClickUndefine = output<void>();
    onClickHire = output<void>();
    onToggle = output<Event>();
    candidateId:string = '';
    btnColor1:string = '';
    btnColor2:string = '';
    textBtnMinWidth:string = '';
    btn1Handler: Function;
    btn2Handler: Function;
    isModalOpen: boolean;  

    readonly candidatureStatus = CandidatureStatus;
    readonly offerStatus = OfferStatus;
    private resizeSubscription: Subscription;
    


  constructor(
    private platform: Platform,
  ) {

  }    

  ngOnInit(){
    if(this.candidate().status === this.candidatureStatus.PRESELECTED 
      || this.candidate().status === this.candidatureStatus.HIRED){
        this.infoId = 'fits';
        this.iconName = this.iconCheckCircle;
        this.isPreselected = true;
        this.textOnBtn1 = 'chat';
        this.textOnBtn2 = 'status-reassign';
        this.btnColor1 = 'accent';
        this.btnColor2 = 'accent';
        this.typeOfCard = 'card-container fits'
        this.btn2Handler = this.onClickUndefineHandler;
    } else if (this.candidate().status === this.candidatureStatus.REJECTED){
        this.infoId = 'no-fits';
        this.iconName = this.iconNoCheckCircle;
        this.isPreselected = false;
        this.textOnBtn1 = 'remove-candidate';
        this.textOnBtn2 = 'status-reassign';
        this.btnColor1 = 'accent';
        this.btnColor2 = 'accent';
        this.typeOfCard = 'card-container no-fits'
        this.btn1Handler = this.openRemoveCandidateModal;
        this.btn2Handler = this.onClickUndefineHandler;

    } else if (
        this.candidate().status !== this.candidatureStatus.HIRED 
        || this.candidate().status !== this.candidatureStatus.PRESELECTED 
        || this.candidate().status !== this.candidatureStatus.REJECTED ){
            this.textOnBtn1 = 'fits';
            this.textOnBtn2 = 'no-fits';
            this.btnColor1 = 'fits';
            this.btnColor2 = 'no-fits';
            this.typeOfCard = 'card-container no-evaluated'
            this.btn1Handler = this.onClickFitsHandler;
            this.btn2Handler = this.onClickNoFitsHandler;
    };
  }

  goCandidateProfile(){
    this.onGoCandidate.emit(this.candidateId);
  }

  onClickFitsHandler() {
    this.onClickFits.emit();
  }
  onClickNoFitsHandler() {
    this.onClickNoFits.emit();
  }
 
  onClickUndefineHandler() {
    console.log('reevaluar');
    this.onClickUndefine.emit();
  }

  openRemoveCandidateModal(event: Event) {
    console.log('intentando abrir modal');
    event.stopPropagation();
    this.isModalOpen = true;
  }
  onCloseEvent() {
    this.isModalOpen = false;
  }

  ngOnDestroy() {
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }
}


