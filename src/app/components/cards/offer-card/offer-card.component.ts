import { Component, effect, HostListener, inject, input } from "@angular/core";
import { Router } from "@angular/router";
// import { TranslateService } from "@ngx-translate/core";
import { TranslateModule } from "@ngx-translate/core";
import { Offer } from "src/app/models/offer";
import { UserService } from "src/app/services/user/user.service";
import { FabButtonComponent } from "../../buttons/fab-button/fab-button.component";
import { Project } from "src/app/models/project";
import { SharedModule } from "../../shared.module";
import { MiniTagComponent } from "../../tags/mini-tag/mini-tag.component";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { EditOfferModalComponent } from "../../modals/edit-offer-modal/edit-offer-modal.component";

@Component({
  standalone: true,
  selector: 'umu-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.scss'],
  imports: [
    SharedModule, 
    MiniTagComponent, 
    TranslateModule, 
    FabButtonComponent, 
    TextButtonComponent, 
    EditOfferModalComponent
  ]
})
export class OfferCardComponent {
  // private translate = inject(TranslateService);
  private userService = inject(UserService);
  private router = inject(Router)

  // item = input.required<Offer | Project>();
  item = input.required<any>();
  isCompanyOfferDetailPage = input<boolean>(false);
  offerStatus: string = '';
  userType: string = '';
  avatar: string;
  peopleEnrolled: string ='';
  isEnrolled: boolean;
  tagInfo: string;
  iconPeoplePath: string = "people";
  iconCheckCircle:string = "check_circle_icon";
  iconInfo: string = "info";
  isSmallScreen: boolean = false;
  iconPath: string = "../../../assets/icons/edit_icon.svg";
  currentPage: string = ''
  allProjectPage: string = 'all-projects-list'
  projectListPage:string = 'project-list'
  interestProjectPage:string = 'interest-projects'  
  companyOfferPage:string = 'company-offer'
  isProjectPage: boolean = false
  minWidth: '100%';
  isModalOpen: boolean = false;


  constructor(
  ){
    // Suscribe to observables
    this.userService.me.subscribe((user) => {
      this.userType = user.type;
      this.avatar = user.avatarB64;

    });
  
    effect(() => {
      this.peopleEnrolled = this.item().candidates? this.item().candidates.length.toString() : this.item().interestedUsers.length.toString();
      this.offerStatus = this.item().status;
    }) 
    this.checkScreenSize();
  }

  ngOnInit() {
    this.currentPage = this.router.url.slice(1,this.router.url.length)
    if(this.allProjectPage === this.currentPage) {
      this.isProjectPage = true
    } else if (this.projectListPage === this.currentPage) {
      this.isProjectPage === true
    } else if (this.interestProjectPage === this.currentPage) {
      this.isProjectPage === true
    }
  }


  @HostListener('window:resize', ['$event'])
  onResize() {
    this.checkScreenSize();
  }
  private checkScreenSize() {
    this.isSmallScreen = window.innerWidth <= 768; // Cambia el límite según tus necesidades
  }
  onClickOfferCard() {
    const id = this.item()._id;
    if(this.userType === "candidate" && this.item().candidates){
      this.router.navigate(['/offer-detail', id]);
    } else if(this.userType === "company" && this.item().candidates) {
      this.router.navigate(['/company-offer-detail', id])  
    } else if(this.item().interestedUsers){
      this.router.navigate(['/project-detail', id]);
    }
  }
  openEditItemModal(event: Event) {
    event.stopPropagation()
    this.isModalOpen = true;
  }
  onCloseEvent() {
    this.isModalOpen = false;
  }

}


