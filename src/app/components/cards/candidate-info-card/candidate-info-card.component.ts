import { Component, HostListener, inject, input, OnInit, output } from "@angular/core";
import { InfoCardComponent } from "../info-card/info-card.component";
import { InfoTextListComponent } from "../../info/info-text-list/info-text-list.component";
import { UserService } from "src/app/services/user/user.service";
import { Info } from "src/app/models/info.model";
import { User } from "src/app/models/user";
import { EditCandidateInfoModalComponent } from "../../modals/edit-candidate-info-modal copy/edit-candidate-info-modal.component";
import { InfoOptionsListComponent } from "../../info/info-options-list/info-options-list.component";
import { TitleComponent } from "../../title/title.component";
import { TextButtonComponent } from "../../buttons/text-button/text-button.component";
import { SharedModule } from "../../shared.module";
import { IonRouterOutlet, ModalController } from "@ionic/angular";
import { FileModalPageComponent } from "src/app/shared/modals/file-modal/file-modal.page";
import { UploadFileAlertComponent } from "../../modals/upload-file-alert/upload-file-alert.component";

@Component({
    standalone: true,
    selector: 'umu-candidate-info-card',
    templateUrl: './candidate-info-card.component.html',
    styleUrl: './candidate-info-card.component.scss',
    imports: [
        SharedModule, 
        InfoCardComponent,
        InfoTextListComponent,
        InfoOptionsListComponent,
        TitleComponent,
        TextButtonComponent,
        EditCandidateInfoModalComponent,
        UploadFileAlertComponent
    ]
})
export class CandidateInfoCardComponent implements OnInit{
    
    isOpen: boolean = false
    isOpenUploadCVAlert: boolean = false
    me: User;
    completeName: string;
    infoList: Info[] = [];
    langList: Info[] = [];
    isSmallScreen: boolean = false;

    private userService = inject(UserService)

    constructor(
        private modalController: ModalController,
        private routerOutlet: IonRouterOutlet,
    ){}

    ngOnInit(): void {
        this.userService.me.subscribe(me => {
            this.me = me;
            this.completeName = me.name + ' ' + me.surname;
            if(this.infoList.length === 0 && me._id){
              this.setUserInfo(me);
            }
            if(this.langList.length === 0 && me._id){
                this.setLangs(me.languages)
            }  
        })
    }

    @HostListener('window:resize', ['$event'])
    onResize() {
        this.isSmallScreen = window.innerWidth <= 576; // Cambia el límite según tus necesidades
    }

    setUserInfo(me){
        this.infoList.push(
          {name: 'email', content: me.email},
          {name: 'city', content: me.city},
          {name: 'qualification', content: me.eduLevel},
          {name: 'last-job', content: me.jobPosition},
          {name: 'sectors', content: me.sector},
          {name: 'experience-cap', content: me.experience},
          {name: 'description', content: me.lastJobTasks}
        );
    }

    setLangs(langs){
        for(let lang of langs){
          this.langList.push({name:lang})
        }
    }
    
    onEditHandler(){
        this.isOpen = true
    }

    onClose(){
        this.isOpen = false
    }

    openUploadCVAlert() {
      this.isOpenUploadCVAlert = true
    }

    onCloseUploadCVAlert(){
      this.isOpenUploadCVAlert = false
    }
   
      async downloadCV() {
        this.userService
          .qGetDownloadCVUrl()
          .subscribe(response => {
            window.open(response.data.downloadCvUrl.presignedUrl);
          });
      }
    
      async downloadCoverLetter() {
        this.userService
          .qGetDownloadCoverLetterUrl()
          .subscribe(response => {
            window.open(response.data.downloadCoverLetterUrl.presignedUrl);
          })
      }
}