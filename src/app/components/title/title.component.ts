import { Component, input } from "@angular/core";

@Component({
    standalone: true,
    selector:'umu-title',
    templateUrl: './title.component.html',
    styleUrl:'./title.component.scss'
})
export class TitleComponent {
    type = input.required<string>();
}