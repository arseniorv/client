import { Component, input } from "@angular/core";
import { TitleComponent } from "../../title/title.component";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "../../shared.module";

@Component({
    standalone: true,
    selector: 'umu-info-options-list',
    templateUrl: './info-options-list.component.html',
    styleUrl:'./info-options-list.component.scss',
    imports: [
        TitleComponent,
        SharedModule
    ]
})
export class InfoOptionsListComponent {

    title = input<string>('');
    options = input.required<any>();

}