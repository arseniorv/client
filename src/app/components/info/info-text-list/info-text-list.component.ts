import { Component, input } from "@angular/core";
import { TitleComponent } from "../../title/title.component";
import { TranslateModule } from "@ngx-translate/core";

interface Info {
    name: string;
    content?: any;
}

@Component({
    standalone: true,
    selector: 'umu-info-text-list',
    templateUrl: './info-text-list.component.html',
    styleUrl: './info-text-list.component.scss',
    imports: [
        TitleComponent,
        TranslateModule
    ]
})
export class InfoTextListComponent {
    title = input<string>('');
    infoList = input.required<Info[]>();

}