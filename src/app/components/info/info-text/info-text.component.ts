import { Component, input } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { TitleComponent } from "../../title/title.component";

@Component({
    standalone: true,
    selector: 'umu-info-text',
    templateUrl: './info-text.component.html',
    styleUrl: './info-text.component.scss',
    imports: [
        SharedModule,
        TitleComponent
    ]
})
export class InfoTextComponent {
    title = input<string>('');
}