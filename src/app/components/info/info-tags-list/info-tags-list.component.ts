import { Component, input } from "@angular/core";
import { SharedModule } from "../../shared.module";

interface Tags {
    iconName: string,
    label: string
}

@Component({
    standalone: true,
    selector:'umu-info-tags-list',
    templateUrl: './info-tags-list.component.html',
    styleUrl: './info-tags-list.component.scss',
    imports: [
        SharedModule
    ]
})
export class InfoTagsListComponent {
    tagsList = input.required<Tags[]>();
}