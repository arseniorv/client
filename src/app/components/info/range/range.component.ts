import { Component, input } from "@angular/core";
import { SharedModule } from "../../shared.module";

@Component({
    standalone: true,
    selector: 'umu-range',
    templateUrl: './range.component.html',
    styleUrl: './range.component.scss',
    imports: [
        SharedModule
    ]
})
export class RangeComponent {
    value =  input.required<number>();
    title = input<string>('');

}