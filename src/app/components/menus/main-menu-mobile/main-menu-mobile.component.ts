import { Component, inject } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user/user.service";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { MultilanguageModule } from "src/app/partials/multilanguage/multilanguage.module";
import { RouterModule } from "@angular/router";

const USER_TYPE_DERIVED_VALUES = {
  candidate: {
    profileRouterLink : '/user-profile',
    offerRouterLink : '/offer-list',
    textUndertakeOrProjects: 'undertake'
  },
  company: {
    profileRouterLink:'/company-profile', 
    offerRouterLink: '/company-offer',
    textUndertakeOrProjects: 'projects'
  }
}

@Component({
    standalone: true,
    selector: 'umu-main-menu-mobile',
    templateUrl: './main-menu-mobile.component.html',
    styleUrls: ['./main-menu-mobile.component.css'], 
    imports: [
      CommonModule,
      IonicModule,
      MultilanguageModule, 
      TranslateModule,
      RouterModule
    ],
  })
  export class MainMenuMobileComponent {
    userType: string = '';
    derivedValue = USER_TYPE_DERIVED_VALUES.candidate

    authService =  inject(AuthService);
    userService = inject(UserService);

    constructor() {
        this.userService.me.subscribe(me => {
          this.userType = me.type
          if (this.userType === 'candidate') {
            this.derivedValue = USER_TYPE_DERIVED_VALUES.candidate
          } else if (this.userType === 'company') {
            this.derivedValue = USER_TYPE_DERIVED_VALUES.company
          }
        })
    };
}


