import { Component, inject } from "@angular/core";
import { IonicModule, MenuController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user/user.service";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { Router, RouterModule } from "@angular/router";
import { MultilanguageModule } from "src/app/partials/multilanguage/multilanguage.module";
import { User } from "src/app/models/user";
import { MultilanguageComponent } from "src/app/partials/multilanguage/multilanguage.component";

interface Page {
  url: string
  title: string
  icon: string
}
@Component({
  standalone: true,
  selector: 'umu-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  imports: [
    CommonModule,
    IonicModule,
    MultilanguageModule,
    TranslateModule,
    RouterModule,
    MultilanguageComponent
  ],
})
export class MainMenuComponent {
  me: User;
  pages: Page[];

  authService = inject(AuthService);
  userService = inject(UserService);
  menuController = inject(MenuController);
  router = inject(Router)

  constructor() {
    this.userService.me.subscribe(me => {
      this.me = me
      // Set pages
      if (me.type === 'company') {
        this.pages = [
          { title: 'profile', url: '/company-profile', icon: 'person-outline' },
          { title: 'my-offers', url: '/company-offer', icon: 'briefcase-outline' },
          { title: 'projects', url: '/all-projects-list', icon: 'rocket-outline' },
          { title: 'info', url: '/legal-info', icon: 'information-circle-outline' }
        ];
      } else if (me.type === 'candidate') {
        this.pages = [
          { title: 'profile', url: '/user-profile', icon: 'person-outline' },
          { title: 'offers', url: '/offer-list', icon: 'briefcase-outline' },
          { title: 'undertake', url: '/all-projects-list', icon: 'rocket-outline' },
          { title: 'info', url: '/legal-info', icon: 'information-circle-outline' }
        ];
      }
    })
  };


  logOut() {
    this.menuController.enable(false);
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}


