import { Component, effect, EventEmitter, input, output, Output } from "@angular/core";
import { SharedModule } from "../shared.module";

@Component({
  standalone: true,
  selector: 'umu-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  imports: [SharedModule]
})
export class BannerComponent {
  contentText = input<string>('');
  backgroundImg = input<string>('');
  isOpen = input.required<boolean>();
  showCloseButton = input<boolean>(true);
  onClose = output<void>();
  modalBgStyle: object = {};
  modalStyle = input<object>({});

  constructor() {
    // cuando haya cambios en lso inputs puedo recalcular lo que haga falta
    effect(() => {
      this.modalBgStyle = this.backgroundImg() !== '' ? {'background-image': 'url(' + this.backgroundImg() + ')'} : {}
    } )
  }

  onCloseEvent() {
    this.onClose.emit();
  }
}