import { Component, effect, input, output } from "@angular/core";
import { InfiniteScrollCustomEvent, IonicModule } from "@ionic/angular";

export interface Pagination {
  currentPage: number,
  pageSize: number,
  skip: number,
  complete: Function,
}

@Component({
    standalone: true,
    selector: 'umu-infinite-scroll',
    templateUrl: './infinite-scroll.component.html',
    styleUrls: ['./infinite-scroll.component.scss'],
    imports: [
      IonicModule,
    ]
  })
  export class InfiniteScrollComponent {

    items = input<any[]>([])
    pageSize = input<number>(10)
    currentPage = input<number>(1)
    onNewPage = output<Pagination>()

    disable: boolean = false

    // Enable infinite scroll on page refresh
    constructor(){
      effect(() => {
        if(this.currentPage() === 1){
          this.disable = false
        }
      })
    }

    doInfinite(event: InfiniteScrollCustomEvent) {
      this.onNewPage.emit({
        currentPage: this.currentPage()+1, 
        pageSize: this.pageSize(), 
        skip: this.currentPage()*this.pageSize(),
        complete: () => { 
          event.target.complete() 
          // Diseable if last page
          if(this.items().length % this.pageSize() < this.pageSize()){
            this.disable = true
          }
        }
      })
    }
    
  }