import { Component, effect, ElementRef, inject, input, output, ViewChild } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@Component({
  standalone: true,
  imports: [ 
    TranslateModule,
    IonicModule
  ],
  selector: 'umu-mini-tag',
  templateUrl: './mini-tag.component.html',
  styleUrls: ['./mini-tag.component.css']
})
export class MiniTagComponent {

  iconName = input<string>('')
  color = input<string>('primary')
  infoId = input<string>('')
  info = input<string>('')

  translateService = inject(TranslateService);

}

