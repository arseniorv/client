import { Component, effect, input, output } from "@angular/core";
import { FabButtonComponent } from "../../buttons/fab-button/fab-button.component";
import { AvatarModalComponent } from "../../modals/avatar-modal/avatar-modal.component";
import { SharedModule } from "../../shared.module";


@Component({
    standalone: true,
    selector: 'umu-avatar',
    templateUrl: './avatar.component.html',
    styleUrl:'./avatar.component.css',
    imports: [
    SharedModule,
    // UIModule,
    AvatarModalComponent,
    FabButtonComponent
]
})
export class AvatarComponent {

    // Supports also a path to image for the placeholder (only on client)
    avatarB64 = input<string>("./../../../../assets/avatar.svg");
    isEditable = input<boolean>(false);
    onUpdateAvatar = output<string>();
    newAvatarB64: string;
    isModalOpen: boolean = false;

    
    constructor () {
        effect(() => {
            this.newAvatarB64 = this.avatarB64() == null || this.avatarB64() === '' ? "./../../../../assets/avatar.svg" : this.avatarB64()
        })
    }

    async openAvatarModal() {
        this.isModalOpen = true;
    }

    updateAvatar(avatarB64){        
        this.newAvatarB64 = avatarB64;
        this.onUpdateAvatar.emit(avatarB64);
    }

    onCloseEvent(){
        this.isModalOpen = false;
    }
}