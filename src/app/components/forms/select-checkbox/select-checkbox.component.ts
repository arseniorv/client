import { Component, inject, input, OnChanges, output } from "@angular/core";
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared.module";
import { TranslatedItem } from "src/app/shared/directives/translate.directive";

@Component({
    standalone: true,
    selector: 'umu-select-checkbox',
    templateUrl:'./select-checkbox.component.html',
    styleUrl: './select-checkbox.component.css',
    imports: [
        SharedModule,
        ReactiveFormsModule
    ]
})
export class SelectCheckboxComponent implements OnChanges {

    options = input<string[]>([]);
    optionsTranslated = input<TranslatedItem[]>([]);
    // expLabels = input<{label: string, value?: number, min?: number, max?: number}[]>()
    labelId = input<string>('');
    selectedValues = output<string[]>();
    onClear = input<boolean>(false);

    fBuilder = inject(FormBuilder);

    form: FormGroup

    ngOnInit(){
        this.initForm();
        this.formatValues();
    }

    ngOnChanges(){
        if(this.onClear() === true){
            this.initForm(); 
            this.formatValues();
        }  
    }

    initForm(){       
    // creates an specific form with form controls passed via signal
        let group = {}
        for (let o of this.options()){
           group[o] = [false] 
        }
        this.form = this.fBuilder.group(group)
    }

    formatValues(){
        const selectedValues = []
        this.form.valueChanges.subscribe(form => {
            // creates an array from object to make it iterable
            const formArray = Object.keys(form).map(key => ({
                key: key,
                value: form[key]
            }));
            for (let option of formArray){
                // if is checked and not present yet, add it to selectedValues array
                if(option.value === true && !selectedValues.includes(option.key)){
                    selectedValues.push(option.key)
                } else if(option.value === false && selectedValues.includes(option.key)){
                    // if it was checked before and its unchecked, delete from array
                    const index = selectedValues.findIndex(item => item === option.key);
                    selectedValues.splice(index, 1);
                }
            }
            this.selectedValues.emit(selectedValues);
        });
    }
}