import { Component, inject, input } from '@angular/core';
import { AbstractControl, ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  standalone: true,
  selector: 'umu-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css'],
  imports: [
    TranslateModule,
    ReactiveFormsModule,
  ],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: () => inject(ControlContainer, { skipSelf: true })
    }
  ]
})
export class TextAreaComponent {
  labelId = input.required<string>()
  placeholderId = input<string>('')
  name = input.required<string>()
  required = input<boolean>(false)
  minLength = input<number>(0)
  maxLength = input<number>(100)
  rows = input<number>(5)
  cols = input<number>(50)
  control = input.required<AbstractControl>()
}
