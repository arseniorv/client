import { Component, input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { CheckboxCustomEvent, IonicModule } from '@ionic/angular';
import { IonModal } from '@ionic/angular';
import { CheckboxComponent } from "../checkbox/checkbox.component";
import { LegalInfoComponent } from 'src/app/partials/legal-info/legal-info.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    IonicModule,
    CheckboxComponent,
    LegalInfoComponent
  ],
  selector: 'umu-privacy-checkbox',
  templateUrl: './privacy-checkbox.component.html',
  styleUrls: ['./privacy-checkbox.component.css'],
})
export class PrivacyCheckboxComponent implements OnInit {
  control = input.required<AbstractControl>()
  selectedLang = ''
  privacyPolicyOK: boolean = false;

  // Privacy policy modal area
  @ViewChild(IonModal) modal: IonModal;

  ngOnInit(): void {
    this.selectedLang = window.localStorage.getItem('language');
  }
 
  onToggle(event: Event) {
    const ev = event as CheckboxCustomEvent;
    this.privacyPolicyOK = ev.detail.checked;
  }

  exit() {
    this.modal.dismiss(true, 'confirm');
  }
}
