import { Component, inject, input } from '@angular/core';
import { AbstractControl, ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  standalone: true,
  selector: 'umu-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.css'],
  imports: [
    TranslateModule,
    ReactiveFormsModule,
    IonicModule
  ],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: () => inject(ControlContainer, { skipSelf: true })
    }
  ]
})
export class TextFieldComponent {
  labelId = input.required<string>()
  placeholderId = input<string>('text-field-placeholder')
  type = input<string>('text')
  name = input.required<string>()
  required = input<boolean>(false)
  minLength = input<number>(0)
  maxLength = input<number>(100)
  min = input<number>(0)
  max = input<number>(100)
  step = input<number>(1)
  unitsId = input<string>('')
  control = input.required<AbstractControl>()
  variant = input<'modal' | 'register' | ''>('')

  showPassword = false

  toggleShowPassword(){
    const password = document.querySelector("#" + this.name());
    const type = password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
    this.showPassword = !this.showPassword
  }

  add(step: number){
    if (this.control().value >= this.min() || this.control().value <= this.max())
      this.control().setValue(this.control().value + step);
  }
}
