import { Component, inject, input } from '@angular/core';
import { AbstractControl, ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

interface Option {
  value: string,
  label: string
}

@Component({
  standalone: true,
  selector: 'umu-select-dropdown',
  templateUrl: './select-dropdown.component.html',
  styleUrls: ['./select-dropdown.component.css'],
  imports: [
    IonicModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: () => inject(ControlContainer, { skipSelf: true })
    }
  ]
})
export class SelectDropdownComponent {
  multiple = input<boolean>(false)
  labelId = input.required<string>()
  placeholderId = input<string>('select')
  name = input.required<string>()
  options = input<string[]>([])
  optionsTranslated = input<Option[]>([])
  required = input<boolean>(false)
  minLength = input<number>(0)
  maxLength = input<number>(50)
  control = input.required<AbstractControl>()
  variant = input<'modal' | 'register' | ''>('')
}
