import { Component, inject, input } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { OffersService } from "src/app/services/offers/offers.service";

@Component({
    standalone: true, 
    selector: 'umu-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrl:'./search-bar.component.css',
    imports: [
        SharedModule,
        ReactiveFormsModule
    ]
})
export class SearchBarComponent {

    filters: FormGroup;
    placeholder = input<string>('');

    private fBuilder = inject(FormBuilder);
    private offersService = inject(OffersService);

    ngOnInit(){
        this.filters = this.fBuilder.group({
            search: [undefined]
        })
    }

    onFilter(){
        this.offersService.setFilters(this.formatFilters());
    }

    formatFilters(){
        const filters: any = {}

        if (this.filters.value.search)
            filters.search = this.filters.value.search

        if(Object.keys(filters).length==0)
            return undefined;
      
        return filters 
    }

    clearFilters() {
        this.filters.reset();
        this.offersService.setFilters();
      }

}