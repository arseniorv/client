import { Component, inject, input, output } from '@angular/core';
import { AbstractControl, ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  standalone: true,
  selector: 'umu-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
  imports: [
    TranslateModule,
    ReactiveFormsModule,
    IonicModule
  ],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: () => inject(ControlContainer, { skipSelf: true })
    }
  ]
})
export class CheckboxComponent {
  labelId = input<string>('')
  name = input.required<string>()
  required = input<boolean>(false)
  control = input.required<AbstractControl>()
  onToggle = output<Event>()
}
