import { Component, ElementRef, inject, input, output, ViewChild } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { Competency } from 'src/app/models/competency.model';

@Component({
  standalone: true,
  imports: [ 
    TranslateModule,
    IonicModule
  ],
  selector: 'umu-tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: ['./tag-input.component.css']
})
export class TagInputComponent {
  name = input.required<string>()
  labelId = input.required<string>()
  placeholderId = input<string>()
  autocomplete = input<string[]>([])
  message = input<string>('');
  errorMinTags = input.required<boolean>()
  onAdd = output<string>()
  onDelete = output<string>()
  variant = input<'modal' | 'register' | ''>('')
  previousValues = input<Competency[]>([])
  
  value: string = '' 
  tags: Set<string> = new Set();
  suggestions: string[] = [];

  translateService = inject(TranslateService)

  @ViewChild('input')input: ElementRef

  ngOnChanges(){
    if(this.previousValues()){
      for (let value of this.previousValues()){
        this.tags.add(value.name)
      }
    }
  }

  remove(tag: string) {
    this.tags.delete(tag);
    if (this.onDelete) {
      this.onDelete.emit(tag);
    }
  }

  add(tag: string) {
    this.suggestions = [];
    this.tags.add(tag)
    this.onAdd.emit(tag);
    this.input.nativeElement.value = ''
  }

  updateSuggestions(event) {
    if(!event.target.value){
      this.suggestions = [];
    } else {
      this.suggestions = this.autocomplete().filter(
        suggestion => !this.tags.has(suggestion) && this.translateService.instant(suggestion).toLowerCase().includes(event.target.value.toLowerCase())
      )
    }
  }
}
