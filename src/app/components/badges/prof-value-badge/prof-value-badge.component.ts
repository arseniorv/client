import { Component, effect, input } from "@angular/core";
import { BadgeComponent } from "../badge/badge.component";

@Component({
    standalone: true,
    selector: 'umu-prof-value-badge',
    templateUrl: './prof-value-badge.component.html',
    styleUrls: ['./prof-value-badge.component.css'],
    imports: [
      BadgeComponent
    ]
  })
  export class ProfValueBadgeComponent {
    name = input.required<string>()
    iconPath: string

    constructor(){
      effect(() => {
        this.iconPath = `../../../assets/icons/header_icons/${this.name()}.svg`;
      })
    }
  }