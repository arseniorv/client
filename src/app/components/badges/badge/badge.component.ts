import { CommonModule } from "@angular/common";
import { Component, inject, input } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

@Component({
    standalone: true,
    selector: 'umu-badge',
    templateUrl: './badge.component.html',
    styleUrls: ['./badge.component.css'],
    imports: [
      IonicModule,
      CommonModule,
      TranslateModule
    ]
  })
  export class BadgeComponent {
    private translate = inject(TranslateService)
    title = input<string>('')
    iconName = input<string>('')
    iconPath = input<string>('')
    color = input<string>('')
    fontSize = input<string>('')
  }