import { Component, ElementRef, inject, input, output, effect, Renderer2, ViewChild } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  standalone: true,
  selector: 'umu-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css'],
  imports: [TranslateModule]
})
export class StepperComponent{
  steps = input.required<Array<string>>()
  currentStep = input.required<number>()
  onStepClick = output<number>()

  @ViewChild('stepperContainer') stepperEl: ElementRef | undefined;
  gridCols = '';
  
  renderer = inject(Renderer2)
  
  constructor() {
    effect(() => {
      this.setGridCols()
    })
  }

  handleStepClick(index: number) {
    //user can only move through the stepper backwards
    if(index > this.currentStep()){
      return
    }
    this.onStepClick.emit(index)
  }

  // Set the number of columns based on the number of steps provided
  setGridCols() {
    this.gridCols = `repeat(${this.steps().length}, 1fr)`

    this.renderer.setStyle(
      this.stepperEl.nativeElement,
      'grid-template-columns',
      this.gridCols
    );
  }
}
