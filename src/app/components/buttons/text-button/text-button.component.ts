import { Component, input } from '@angular/core';
import { SharedModule } from '../../shared.module';

@Component({
  standalone: true,
  selector: 'umu-text-button',
  templateUrl: './text-button.component.html',
  styleUrls: ['./text-button.component.css'],
  imports: [SharedModule]
})
export class TextButtonComponent {

  color = input<string>('accent');
  shape = input<string>('round');
  minWidth= input<string>("none")
  enabled = input<boolean>(true);

  constructor() {}

}
