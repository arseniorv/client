import { Component, inject} from '@angular/core'
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat/chat.service';
import { SharedModule } from '../../shared.module';

@Component({
  standalone: true,
  selector: 'umu-chat-button',
  templateUrl: './chat-button.component.html',
  styleUrls: ['./chat-button.component.css'],
  imports: [SharedModule]
})
export class ChatButtonComponent {
  private chatService = inject(ChatService); 
  unreads: number = 0

  constructor(private router: Router){
    this.chatService.totalUnreads.subscribe(totalUnreads => this.unreads = totalUnreads >= 0 ? totalUnreads : 0)
  }

  goToConversations() {
    this.router.navigate(['/conversations']);
  }

}
