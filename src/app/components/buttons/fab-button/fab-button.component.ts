import { Component, effect, input} from '@angular/core';;
import { SharedModule } from '../../shared.module';

@Component({
  standalone: true,
  selector: 'umu-fab-button',
  templateUrl: './fab-button.component.html',
  styleUrls: ['./fab-button.component.css'],
  imports: [
 SharedModule  ]
})
export class FabButtonComponent {
  horizontal = input.required<string>();
  vertical = input.required<string>();
  color = input<string>('accent')
  disabled = input<boolean>(false);
  iconName = input<string>('');
  iconPath = input<string>('');
  
  position: string = ''

  constructor() {
    effect(() => {
      this.position = this.horizontal() + ' ' + this.vertical() 
    })
  }

}
