import { Component, inject, input} from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';
import { User } from 'src/app/models/user';
import { SharedModule } from '../../shared.module';
import { TextButtonComponent } from '../text-button/text-button.component';
import { FabButtonComponent } from '../fab-button/fab-button.component';

@Component({
  standalone: true,
  selector: 'umu-start-chat-button',
  templateUrl: './start-chat-button.component.html',
  styleUrls: ['./start-chat-button.component.css'],
  imports: [SharedModule, TextButtonComponent, FabButtonComponent]
})
export class StartChatButtonComponent {
  public chatService = inject(ChatService); 

  recipient = input.required<User>();
  isTextBtn = input.required<boolean>();


  color: string = "accent"
  shape: string = "round"

  horizontal: string = "end"
  slot: string = "fixed"
  vertical: string = "bottom"
  iconName: string = "chatbubble"

  constructor() {}

}
