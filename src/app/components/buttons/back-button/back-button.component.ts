import { CommonModule } from '@angular/common';
import { Component, inject, Input, OnChanges, OnInit} from '@angular/core'
import { Router } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';


@Component({
  standalone: true,
  selector: 'umu-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.css'],
  imports: [
    CommonModule,
    IonicModule,
  ]
})
export class BackButtonComponent implements OnChanges, OnInit {
  private authService = inject(AuthService); 

  defaultHref: string = '/login'; // Set a default value
  disabled: boolean = false
  private subscription: Subscription;


  constructor(private router: Router) {
    this.disabled = this.authService.disabledBackButtonPages.includes(router.url)
  }

  ngOnInit() {
    // Subscribe to the Observable to get the actual string value
    this.subscription = this.authService.defaultHref.subscribe(result => {
      this.defaultHref = result.route; // Assuming the result has a 'route' property as in your Apollo Observable setup
      console.log('defaultHref updated', this.defaultHref);
      this.updateDisabledState();
    }) as Subscription;;
  }


  ngOnChanges() {
    console.log("Changes!");
    this.updateDisabledState();
  }

  private updateDisabledState() {
    this.disabled = this.router.url === this.defaultHref || 
                    this.authService.disabledBackButtonPages.includes(this.router.url);

    console.log('disabled', this.disabled);
  }


}
