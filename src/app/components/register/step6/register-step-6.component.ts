import { Component, inject, input, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SoftSkill } from 'src/app/models/softskill.model';
import { SoftSkillsService } from 'src/app/services/softskills.service';
import { CheckboxComponent } from "../../forms/checkbox/checkbox.component";
import { PrivacyCheckboxComponent } from '../../forms/privacy-checkbox/privacy-checkbox.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    ReactiveFormsModule,
    IonicModule,
    CheckboxComponent,
    PrivacyCheckboxComponent,
  ],
  selector: 'umu-register-step-6',
  templateUrl: './register-step-6.component.html',
  styleUrls: ['./register-step-6.component.css'],
})
export class RegisterStep6Component implements OnInit {
  userType = input.required<string>()
  onValid = output<object>()
  softSkillsList = []
  userSkills: SoftSkill[] = []
  userSkillsIds: Set<string> = new Set()

  softSkillsService = inject(SoftSkillsService)

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    privacy: [false, [Validators.requiredTrue]],
    comms: [false, []]
  })

  get controls() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.softSkillsService.softSkills.subscribe((softSkills) => {
      this.softSkillsList = softSkills
    })

    this.form.valueChanges.subscribe(() => {
      if(this.form.valid && this.userSkills.length >= 6){
        let values = this.form.value;
        this.onValid.emit({
          privacyPolicyOK: this.form.value.privacy,
          commsOK: this.form.value.comms,
          softSkills: this.userSkills
        });
      }
    })
  }

  toggleSkill(skillId: string) {
    if (this.userSkillsIds.has(skillId)) {
      this.userSkillsIds.delete(skillId);
      this.userSkills = this.userSkills.filter(s => s._id !== skillId);
    } else {
      const skill = this.softSkillsList.find(s => s._id === skillId);
      if (skill) {
        this.userSkillsIds.add(skillId);
        this.userSkills.push({ _id: skill._id, name: skill.name });
      }
    }
  }
}
