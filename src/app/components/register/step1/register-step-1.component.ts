import { Component, output } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { TextButtonComponent } from '../../buttons/text-button/text-button.component';

@Component({
  standalone: true,
  imports: [
    IonicModule,
    TranslateModule,
    TextButtonComponent
  ],
  selector: 'umu-register-step-1',
  templateUrl: './register-step-1.component.html',
})
export class RegisterStep1Component {
  onSubmit = output<'candidate' | 'company'>()

  submit(userType: 'candidate' | 'company') {
    this.onSubmit.emit(userType)
  }
}
