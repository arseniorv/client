import { Component, inject, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CompetenciesService } from 'src/app/services/competencies.service';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { languageList } from '../../../utils/constants';
import { SelectDropdownComponent } from '../../forms/select-dropdown/select-dropdown.component';
import { TagInputComponent } from '../../forms/tag-input/tag-input.component';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    SelectDropdownComponent,
    TagInputComponent,
    ReactiveFormsModule,
    IonicModule
  ],
  selector: 'umu-register-step-7',
  templateUrl: './register-step-7.component.html',
  styleUrls: ['./register-step-7.component.css'],
})
export class RegisterStep7Component implements OnInit {
  umuTranslate = inject(UmuTranslateDirective)

  onValid = output<object>()

  readonly languageList = languageList;
  translatedAndOrderedLanguages = []

  educationMinLength = 2
  educationMaxLength = 100
  competenciesNames: string[] = []
  userCompetencies: string[] = []
  errorMinTags: boolean = true
  minTags: number = 2

  translateService = inject(TranslateService)
  competenciesService = inject(CompetenciesService)
  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    education: ['', [
      Validators.required,
      Validators.minLength(this.educationMinLength),
      Validators.maxLength(this.educationMaxLength)
    ]],
    languages: ['', [Validators.required]],
  })
  
  ngOnInit(): void {
    this.competenciesService.competencies.subscribe(competencies => {
      this.competenciesNames = [...competencies]
        .sort((a, b) => this.translateService.instant(a.name) > this.translateService.instant(b.name) ? 1 : -1).map(c => c.name);
    })
    this.umuTranslate.translateAndOrder(this.languageList).subscribe(list => this.translatedAndOrderedLanguages = list)
    
    this.form.valueChanges.subscribe(() => { this.checkValid()})
  }

  get controls() {
    return this.form.controls;
  }

  addCompetency(competency: string) {
    this.userCompetencies.push(competency)
    this.errorMinTags = this.userCompetencies.length < this.minTags
    this.checkValid()
  }

  removeCompetency(competency: string) {
    this.userCompetencies = this.userCompetencies.filter(c => c !== competency);
    this.errorMinTags = this.userCompetencies.length < this.minTags
  }

  checkValid() {
    if(this.form.valid && !this.errorMinTags){
      this.competenciesService.findOrCreateCompentencies(this.userCompetencies).subscribe(
        {
          next: (competencies) => {
            this.onValid.emit({
              eduLevel: this.form.value.education,
              languages: this.form.value.languages,
              competencies
            })
          },
          error: (error) => {
            console.log("Error creating competencies...")
            console.error(error)
          }
        }
      )
    }
  }

}
