import { Component, inject, input, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { TextAreaComponent } from '../../forms/textarea/textarea.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextAreaComponent,
    ReactiveFormsModule
  ],
  selector: 'umu-register-step-5',
  templateUrl: './register-step-5.component.html',
})
export class RegisterStep5Component implements OnInit{
  userType = input.required<string>()
  onValid = output<object>()

  lastJobMinLength = 20
  lastJobMaxLength = 1000
  lastJobRows = 10
  lastJobCols = 20

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    lastJobTasks: ['', [
      Validators.required,
      Validators.minLength(this.lastJobMinLength),
      Validators.maxLength(this.lastJobMaxLength)]],
  })

  get controls() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        this.onValid.emit(this.form.value);
      }
    })
  }
}
