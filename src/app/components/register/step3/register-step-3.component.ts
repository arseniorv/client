import { Component, inject, OnInit, output } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { matchValidator } from 'src/app/shared/directives/match-validator.directive';
import { passwordValidator } from 'src/app/shared/directives/password-validator.directive';
import { UniqueEmailValidator } from 'src/app/shared/directives/unique-email-validator.directive';
import { TextFieldComponent } from '../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    ReactiveFormsModule
  ],
  selector: 'umu-register-step-3',
  templateUrl: './register-step-3.component.html',
})
export class RegisterStep3Component implements OnInit{
  onValid = output<object>()

  emailMinLength = 8
  passwordMinLength = 8
  passwordMaxLength = 64
  passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$/

  uniqueEmailValidator = inject(UniqueEmailValidator)
  form: FormGroup

  ngOnInit(): void {
    this.form = new FormGroup(
      {
        email: new FormControl('', {
          validators: [
            Validators.required,
            Validators.email
          ],
          asyncValidators: [
            this.uniqueEmailValidator.validate.bind(this.uniqueEmailValidator)
          ],
          updateOn: 'blur'
        }),
        password: new FormControl('', {
          validators: [
            Validators.required,
            Validators.minLength(this.passwordMinLength),
            Validators.maxLength(this.passwordMaxLength),
            passwordValidator(this.passwordRegex)
          ],
          updateOn: 'change'
        }),
        confirmPassword: new FormControl('', {
          validators: [      
            Validators.required,
            Validators.minLength(this.passwordMinLength),
            Validators.maxLength(this.passwordMaxLength),
          ],
          updateOn: 'change'
        })
      },
      matchValidator('password', 'confirmPassword')
    )

    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        this.onValid.emit({
          email: this.form.value.email,
          password: this.form.value.password
      });
      }
    })
  }

  get controls() {
    return this.form.controls;
  }
}
