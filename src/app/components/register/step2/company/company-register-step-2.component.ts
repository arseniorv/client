import { Component, inject, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AvatarComponent } from 'src/app/components/forms/avatar/avatar.component';
import { LegalForm } from 'src/app/models/legal-form.model';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { cityList } from 'src/app/utils/constants';
import { SelectDropdownComponent } from '../../../forms/select-dropdown/select-dropdown.component';
import { TextFieldComponent } from '../../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    SelectDropdownComponent,
    ReactiveFormsModule,
    AvatarComponent
  ],
  selector: 'umu-company-register-step-2',
  templateUrl: './company-register-step-2.component.html',
})
export class CompanyRegisterStep2Component implements OnInit {
  umuTranslate = inject(UmuTranslateDirective)

  onValid = output<object>();
  avatarB64: string;

  readonly cityList = cityList;
  translatedAndOrderedCities = []
  readonly legalFormList = Object.values(LegalForm);
  translatedAndOrderedLegalForms = []

  companyNameMinLength = 2
  companyNameMaxLength = 31

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    avatarB64: [''],
    name: ['', [
      Validators.required,
      Validators.minLength(this.companyNameMinLength),
      Validators.maxLength(this.companyNameMaxLength),
    ]],
    city: ['', [Validators.required]],
    legalForm: ['', [Validators.required]]
  })

  get controls() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.umuTranslate.translateAndOrder(this.legalFormList).subscribe(list => this.translatedAndOrderedLegalForms = list)
    this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        let values = this.form.value;
        values.avatarB64 = this.avatarB64;
        this.onValid.emit(values);
      } else {
        this.onValid.emit(null);
      }
    })
  }

  onUpdateAvatar(newAvatarB64){
    this.avatarB64 = newAvatarB64;
  }

}
