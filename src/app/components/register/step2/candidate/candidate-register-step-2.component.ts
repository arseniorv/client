import { Component, inject, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { cityList } from 'src/app/utils/constants';
import { AvatarComponent } from "../../../forms/avatar/avatar.component";
import { SelectDropdownComponent } from '../../../forms/select-dropdown/select-dropdown.component';
import { TextFieldComponent } from '../../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    SelectDropdownComponent,
    ReactiveFormsModule,
    AvatarComponent,
],
  selector: 'umu-candidate-register-step-2',
  templateUrl: './candidate-register-step-2.component.html',
})
export class CandidateRegisterStep2Component implements OnInit {
  umuTranslate = inject(UmuTranslateDirective)

  onValid = output<object>();
  avatarB64: string;

  readonly cityList = cityList;
  translatedAndOrderedCities = []

  nameMinLength = 2
  nameMaxLength = 31

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    avatarB64: [''],
    name: ['', [
      Validators.required,
      Validators.minLength(this.nameMinLength),
      Validators.maxLength(this.nameMaxLength),
    ]],
    surname: ['', [
      Validators.required,
      Validators.minLength(this.nameMinLength),
      Validators.maxLength(this.nameMaxLength),
    ]],
    city: ['', [Validators.required]]
  })

  get controls() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.umuTranslate.translateAndOrder(this.cityList).subscribe(list => this.translatedAndOrderedCities = list)
    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        let values = this.form.value;
        values.avatarB64 = this.avatarB64;
        this.onValid.emit(values);
      } else {
        this.onValid.emit(null);
      }
    })
  }

  onUpdateAvatar(newAvatarB64){
    this.avatarB64 = newAvatarB64;
  }

}
