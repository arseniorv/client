import { Component, inject, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { Sector } from 'src/app/models/sector.model';
import { SectorsService } from 'src/app/services/sectors.service';
import { UmuTranslateDirective } from 'src/app/shared/directives/translate.directive';
import { SelectDropdownComponent } from '../../../forms/select-dropdown/select-dropdown.component';
import { TextFieldComponent } from '../../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    SelectDropdownComponent,
    ReactiveFormsModule
  ],
  selector: 'umu-company-register-step-4',
  templateUrl: './company-register-step-4.component.html',
})
export class CompanyRegisterStep4Component implements OnInit {
  umuTranslate = inject(UmuTranslateDirective)
  sectorsService = inject(SectorsService)
  sectorList = []
  translatedAndOrderedSectors = []

  onValid = output<object>()

  websiteMaxLength = 100

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    website: ['', [Validators.maxLength(this.websiteMaxLength)]],
    sector: [[] as string[], [Validators.required]],
    experience: [0, []],
  })

  get controls() {
    return this.form.controls;
  }

  submit() {
    
  }

  ngOnInit(): void {
    this.sectorsService.sectors.subscribe((sectors) => {
      if(sectors !== undefined){
        this.sectorList = sectors.map(sector => sector.name)
        this.umuTranslate.translateAndOrder(this.sectorList).subscribe(list => this.translatedAndOrderedSectors = list)
      }
    })

    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        let selectedSectors = []
        if(this.form.value.sector !== undefined){
          this.form.value.sector.forEach(sectorName => {
            const sector = this.sectorsService.getSectors().find((sector: Sector) => sectorName === sector.name)
            selectedSectors.push(sector)
          })
        }
        this.onValid.emit({
          website: this.form.value.website,
          sector: selectedSectors,
          experience: String(this.form.value.experience)
        })
      }
    })
  }
}
