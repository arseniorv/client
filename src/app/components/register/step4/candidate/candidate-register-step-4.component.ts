import { Component, inject, OnInit, output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { Sector } from 'src/app/models/sector.model';
import { SectorsService } from 'src/app/services/sectors.service';
import { SelectDropdownComponent } from '../../../forms/select-dropdown/select-dropdown.component';
import { TextFieldComponent } from '../../../forms/text-field/text-field.component';

@Component({
  standalone: true,
  imports: [
    TranslateModule,
    TextFieldComponent,
    SelectDropdownComponent,
    ReactiveFormsModule
  ],
  selector: 'umu-candidate-register-step-4',
  templateUrl: './candidate-register-step-4.component.html',
})
export class CandidateRegisterStep4Component implements OnInit{
  sectorsService = inject(SectorsService)
  sectorList = []

  onValid = output<object>()

  jobPosMinLength = 4
  jobPosMaxLength = 71

  formBuilder = inject(FormBuilder)
  form = this.formBuilder.group({
    jobPosition: ['', [
      Validators.required,
      Validators.minLength(this.jobPosMinLength),
      Validators.maxLength(this.jobPosMaxLength)
    ]],
    sector: [[] as string[], [Validators.required]],
    experience: [0, []],
  })

  get controls() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.sectorsService.sectors.subscribe((sectors) => this.sectorList = sectors.map(sector => sector.name))
    
    this.form.valueChanges.subscribe(() => {
      if(this.form.valid){
        let selectedSectors = []
        if(this.form.value.sector !== undefined){
          this.form.value.sector.forEach(sectorName => {
            const sector = this.sectorsService.getSectors().find((sector: Sector) => sectorName === sector.name)
            selectedSectors.push(sector)
          })
        }
        this.onValid.emit({
          jobPosition: this.form.value.jobPosition,
          sector: selectedSectors,
          experience: String(this.form.value.experience)
        })
      }
    })
  }
}
