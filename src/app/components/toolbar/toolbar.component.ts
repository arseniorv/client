import { Component } from "@angular/core";
import { MenuController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { SharedModule } from "../shared.module";
import { ChatButtonComponent } from "../buttons/chat-button/chat-button.component";

@Component({
    standalone: true,
    selector: 'umu-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    imports: [
      SharedModule,
      ChatButtonComponent
    ]
  })
  export class ToolbarComponent {
    constructor(
        public auth: AuthService,
        public menuCtrl: MenuController
    ){}

    
  }