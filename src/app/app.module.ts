import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouteReuseStrategy, RouterModule } from '@angular/router';
import { LayoutModule } from '@angular/cdk/layout';
import { IonModal, IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { UmuComponent } from './umu.component';

import { HttpClient, provideHttpClient } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { GraphQLModule } from './graphql.module';
import { AuthService } from './services/auth.service';

// import ngx translate and the http loader
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TagInputModule } from 'ngx-chips';
import { ImageCropperModule } from 'ngx-image-cropper';
import { routes } from './app-routing.module';
import { UIModule } from './components/ui.module';
import { MultilanguageModule } from './partials/multilanguage/multilanguage.module';

// Pages
import { CandidaturesPageComponent } from './pages/candidatures/candidatures.page';
import { ChatPageComponent } from './pages/chat/chat.page';
import { ConversationsPageComponent } from './pages/conversations/conversations.page';
import { CreateOfferPageComponent } from './pages/create-offer/create-offer.page';
import { DeleteAccountPageComponent } from './pages/delete-account/delete-account.page';
import { EditOfferPageComponent } from './pages/edit-offer/edit-offer.page';
import { EditUserPageComponent } from './pages/edit-user/edit-user.page';
import { AllProjectsListComponent } from './pages/enterprising/all-projects-list/all-projects-list.page';
import { CreateProjectComponent } from './pages/enterprising/create-project/create-project.component';
import { EditProjectComponent } from './pages/enterprising/edit-project/edit-project.component';
import { InterestProjectsComponent } from './pages/enterprising/interest-projects/interest-projects.page';
import { InterestedEntityComponent } from './pages/enterprising/interested-entity/interested-entity.page';
import { ProjectListComponent } from './pages/enterprising/project-list/project-list.page';
import { EntityListPageComponent } from './pages/entity-list/entity-list.page';
import { LegalInfoPageComponent } from './pages/legal-info/legal-info.page';
import { LoginPageComponent } from './pages/login/login.page';
import { NewUserComponent } from './pages/new-user/new-user/new-user.component';
import { PasswordResetPageComponent } from './pages/password-reset/password-reset.page';
import { RegisterPageComponent } from './pages/register/register.page';
import { LegalInfoComponent } from './partials/legal-info/legal-info.component';
import { FileModalPageComponent } from './shared/modals/file-modal/file-modal.page';
import { ValuesModalPageComponent } from './shared/modals/values-modal/values-modal.page';
import { DefaultLayoutComponent } from './templates/default-layout/default-layout';
import { OnlyToolbarLayoutComponent } from './templates/only-toolbar-layout/only-toolbar-layout';
import { ImageUtilsClass } from './utils/image-utils';
import { TitleHeader } from './partials/title-header/title-header';
import { MainMenuComponent } from './components/menus/main-menu/main-menu.component';
import { MiniTagComponent } from './components/tags/mini-tag/mini-tag.component';
import { CandidateCardComponent } from './components/cards/candidate-card/candidate-card.component';
import { MultilanguageComponent } from './partials/multilanguage/multilanguage.component';
import { InfoCardComponent } from "./components/cards/info-card/info-card.component";
import { TextFieldComponent } from './components/forms/text-field/text-field.component';


@NgModule({ 
    declarations: [
        UmuComponent,
        LoginPageComponent,
        EntityListPageComponent,
        EditUserPageComponent,
        EditOfferPageComponent,
        CreateOfferPageComponent,
        DeleteAccountPageComponent,
        CreateProjectComponent,
        InterestProjectsComponent,
        EditProjectComponent,
        InterestedEntityComponent,
        ChatPageComponent,
        RegisterPageComponent,
        FileModalPageComponent,
        PasswordResetPageComponent,
        ConversationsPageComponent
    ],
    bootstrap: [UmuComponent], 
    schemas:[CUSTOM_ELEMENTS_SCHEMA],
    imports: [
        CandidaturesPageComponent,
        ValuesModalPageComponent,
        LegalInfoPageComponent,
        CandidateCardComponent,
        TextFieldComponent,
        ProjectListComponent,
        AllProjectsListComponent,
        MiniTagComponent,
        TitleHeader,
        LayoutModule,
        LegalInfoComponent,
        MainMenuComponent,
        DefaultLayoutComponent,
        OnlyToolbarLayoutComponent,
        MultilanguageComponent,
        BrowserModule,
        MultilanguageModule,
        IonicModule.forRoot(),
        GraphQLModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            // registrationStrategy: 'registerWhenStable:30000'
    }),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }),
    ModalModule.forRoot(),
    UIModule,
    // PartialsModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    ImageCropperModule,
    InfoCardComponent
], 
    providers: [
        ImageUtilsClass,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        AuthService,
        IonModal,
        provideHttpClient(),
        ] })
export class AppModule {}

//required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
    return new TranslateHttpLoader(http);
}