import { inject, Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { forkJoin, map, Observable } from "rxjs";

export interface TranslatedItem {
    value: string
    label: string
}

@Injectable({providedIn: "root"})
export class UmuTranslateDirective {

    translateService = inject(TranslateService)

    translateAndOrder(list: string[]): Observable<TranslatedItem[]> {  

        let translatedList: TranslatedItem[] = []
   
        return this.translateService.get(list).pipe(map(items => {
            const labels = Object.keys(items)
            labels.forEach(key => {
                translatedList.push({value: key, label: items[key]})
            })

            // Order Alphabetically
            translatedList.sort((a, b) => new Intl.Collator(this.translateService.currentLang).compare(a.label, b.label));

            return translatedList
        }))
    } 

    translateTextWithParams(list: any[]): Observable<TranslatedItem[]> { 
        // Observable waits for translation to be loaded 
        return this.translateService.get('init').pipe(map(() => {
            let translateList: TranslatedItem[] = []

            list.forEach(item => {
                let params = {...item}
                delete params.label
                translateList.push({
                    label: this.translateService.instant(item.label, params),
                    value: item.label
                })
            })
            
            return translateList
        }))
       
   
    } 
}