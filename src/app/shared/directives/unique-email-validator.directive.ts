import { inject, Injectable } from "@angular/core";
import { AbstractControl, AsyncValidator, ValidationErrors } from "@angular/forms";
import { catchError, map, Observable, of } from "rxjs";
import { UserService } from "src/app/services/user/user.service";

@Injectable({providedIn: 'root'})
export class UniqueEmailValidator implements AsyncValidator {

    userService = inject(UserService)

    validate(control: AbstractControl): Observable<ValidationErrors | null> {
        return this.userService.checkEmail(control.value).pipe(
            map((emailExists) => emailExists ? { emailExists: true } : null),
            catchError(() => of(null))
        )
    }
}