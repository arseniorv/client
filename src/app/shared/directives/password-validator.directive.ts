import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function passwordValidator(passwordRe: RegExp): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => { 
        const valid = passwordRe.test(control.value) 
        return !valid ? { pattern: !valid } : null
    } 
}