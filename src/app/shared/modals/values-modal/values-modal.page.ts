// ------ Deprecated DO NOT USE --------
// ------ Deprecated DO NOT USE --------
// ------ Deprecated DO NOT USE --------

import { Component, EventEmitter, HostListener, input, OnDestroy, OnInit, output, Output } from '@angular/core';
import { IonicModule, ModalController } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { SoftSkill } from 'src/app/models/softskill.model';
import { SoftSkillsService } from 'src/app/services/softskills.service';
import { UserService } from 'src/app/services/user/user.service';
import { MultilanguageComponent } from '../../../partials/multilanguage/multilanguage.component';

@Component({
  standalone: true,
  selector: 'umu-values-modal',
  templateUrl: './values-modal.page.html',
  styleUrls: ['./values-modal.page.scss'],
  imports: [IonicModule, TranslateModule]
})
export class ValuesModalPageComponent implements OnInit, OnDestroy {
  skillsList: any[] = [];
  maxElementCheckbox = 6;
  skillsSelected: any[] = [];
  currentModal = null;
  userID = sessionStorage.getItem('userId');
  skills: any[] = [];
  userSkills: any[] = [];
  selected = 0;
  selectedSkills: Set<String> = new Set();
  finalSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
  skillsListTranslated: {name: any , output: any, _id: any} [] = [];
  isModalOpen= input.required<boolean>();
  onClose = output<void>();
  
  @Output()
  sendSelectedSkills: EventEmitter<any> = new EventEmitter<any>();

  constructor(private modalController: ModalController,
              private softSkillsService: SoftSkillsService,
              private uService: UserService,
              private translate: TranslateService, 
              private translateList: MultilanguageComponent) {}

  ngOnInit() {
    this.qsoftSkillsQuery();
    this.getLoggedUser();
    // this.userSkills = JSON.parse(sessionStorage.getItem('userSoftSkills'));
    
  }

  /**
   * Get softSkills from DB.
   */
  qsoftSkillsQuery() {
    this.softSkillsService.qGetAllSkills().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.skillsList = item.getSoftskills;
      this.finalSkillsList = this.translateList.translateSkillsLists(this.skillsList)
    });

  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userSkills = item.me.softSkills;
      this.selectedSkills = new Set(this.userSkills.map(s => s._id));
    this.skillsSelected = this.userSkills;
      
    });
  }
 
  saveSkills() {
    this.useSessionStorage(this.skillsSelected);
    this.skillsSelected.forEach(element => {
      this.sendSelectedSkills.emit(element);
      this.skills.push({"_id": element._id, "name": element.name});
    });
    this.mEditUserSkills(this.skills);
  }

  /**
   * Save new values for "Skills"
   * @param skills
   */
  mEditUserSkills(skillsList: SoftSkill[]) {
    if(this.userID) {
      this.uService.mUserSkillUpdate(this.userID, skillsList)
      .subscribe((response) => {
        console.log('Edition Done!');
      });
    }

    this.dismissEditModal(this.skillsSelected);
  }

  /**
  * Close modal when update
  */
  async dismissEditModal(data) {
    this.modalController.dismiss(data);
  }

  /**
   * Selection of skills
   * @param $event 
   * @param item 
   */
  changeSelection(event, item) {
      if (event.target.checked) {
        this.skillsSelected.push(item);
        this.selectedSkills.add(item._id)
      } else {
        this.skillsSelected.splice(this.skillsSelected.indexOf(item), 1);
        this.selectedSkills.delete(item._id)
      }
  }

  disableCheckbox(id): boolean {
    return (
      this.selectedSkills.size >= this.maxElementCheckbox &&
      !this.selectedSkills.has(id)
    );
  }

  isChecked(id): boolean {
    return this.selectedSkills.has(id)
  }

  /**
   * Use LocalStogare for save an array of selected skills
   * @param uSkills 
   */
  useSessionStorage(uSkills) {
    sessionStorage.setItem('userSoftSkills', JSON.stringify(uSkills));
  }

  /** Modal functions if "button back" is clicked */
  ngOnDestroy() {
    if (window.history.state.modal) {
      history.back();
    }
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.modalController.dismiss();
  }

// ¿Necesitaremos esto?
  // onCloseEvent(){
  //   this.isModalOpen = false;
  // }

}

