import { inject } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ImageCroppedEvent } from "ngx-image-cropper";

export class ImageUtilsClass {
    croppedImage: any = '';
    private sanitizer = inject(DomSanitizer) 

    async imageCroppedHandler(event: ImageCroppedEvent) {
        let avatar
        this.croppedImage = this.sanitizer.bypassSecurityTrustUrl(event.objectUrl);
        return await this.convertBase64(event.blob).then((imagen: any) => {
            avatar = imagen.base;
            return avatar;
        })
    }
    
    convertBase64 = async ($event: any) => new Promise ((resolve, reject) => {
        try {
        const unsafeImg = window.URL.createObjectURL($event);
        const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
        const reader = new FileReader();
        reader.readAsDataURL($event);
        reader.onload = () => {
            resolve({
            base: reader.result
            });
        };
        reader.onerror = error => {
            resolve({
            base: null
            })
        }
        } catch(e){
        return null;
        }
    })
}

