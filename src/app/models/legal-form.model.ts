export enum LegalForm {
    ASSOCIATION="association",
    COOPERATIVE="cooperative",
    INSERTIONCOMPANY="insertioncompany",
    FOUNDATION="foundation",
    LABOURSOCIETY="laboursociety",
    MUTUALS="mutuals",
    SPECIALEMPLOYMENTCENTERS="specialemploymentcenters",
    OTHEREESENTITY="othereesentity"
}