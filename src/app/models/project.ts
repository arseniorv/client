import { Competency } from "./competency.model";
import { InterestedUser } from "./interested-user.model";
import { Match } from "./match.model";
import { OfferStatus } from "./offer-status.model";
import { ProjectStatus } from "./project-status.model";
import { Sector } from "./sector.model";
import { SoftSkill } from "./softskill.model";
import { User } from "./user";


export class Project {
        _id: string;
        userId: string;
        title: string;
        description: string;
        sector: Array<Sector>;
        competencies: Array<Competency>;
        city: string;
        user: User;
        softSkills: Array<SoftSkill>;
        status: ProjectStatus;
        createdDate: string;
        files: Array<string>;
        interestedUsers: Array<InterestedUser>;
        state: OfferStatus;
        admins: Array<Member>;
        adminsInfo: Array<User>;
        enrolled: boolean;
        match: Match;
        
}

export interface InputProject {
        id?: string;
        userId?: string;
        title: string;
        description: string;
        sector: Array<Sector>;
        competencies: Array<Competency>;
        city: string;
        softSkills?: Array<SoftSkill>;
        state: OfferStatus;
        status: string;
        files?: Array<string>;
        createdDate?: string;
        admins: Array<Member>;
}

export class Member {
        _id: string;
        email: string;
}
