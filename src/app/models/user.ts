import { Candidatures } from "./candidatures.model";
import { Competency } from "./competency.model";
import { Room } from "./room";
import { Sector } from "./sector.model";
import { SoftSkill } from "./softskill.model";

/* eslint-disable @typescript-eslint/naming-convention */
export class User {
     _id: string;
     name: string;
     surname: string;
     legalForm: string;
     email: string;
     website: string;
     eduLevel: string;
     city: string;
     sector: Array<Sector>;
     experience: string;
     jobPosition: string;
     lastJobTasks: string;
     languages: Array<string>;
     createdDate: string;
     type: string;
     competencies: Array<Competency>;
     softSkills: Array<SoftSkill>;
     avatarB64: string;
     coverLetter: string;
     cv: string;
     video: string;
     commsOK: Boolean;
     privacyPolicyOK: Boolean;

     chatUserId: string;
     chatAuthToken: string;
     rooms: Array<Room>;
}

export class InputUser {
     name: string;
     surname: string;
     password: string;
     legalForm: string;
     email: string;
     website: string;
     eduLevel: string;
     city: string;
     sector: Array<Sector>;
     experience: string;
     jobPosition: string;
     lastJobTasks: string;
     languages: Array<string>;
     createdDate: string;
     type: string;
     competencies: Array<Competency>;
     softSkills: Array<SoftSkill>;
     commsOK: Boolean;
     privacyPolicyOK: Boolean;
     avatarB64: string;
     video: string;
     coverLetter: string;

     constructor() {
          this.name = ''
          this.surname = ''
          this.password = ''
          this.legalForm = null
          this.email = ''
          this.website = ''
          this.eduLevel = ''
          this.city = ''
          this.sector = []
          this.experience = ''
          this.jobPosition = ''
          this.lastJobTasks = ''
          this.languages = []
          this.type = ''
          this.competencies = []
          this.softSkills = []
          this.commsOK = false
          this.privacyPolicyOK = false
          this.createdDate = ''
          this.avatarB64 = ''
          this.video = ''
          this.coverLetter = ''
     }
}

