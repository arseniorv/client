import { Message } from "./message.model";
import { User } from "./user";

export class Conversation {
    _id: string;
    messageCount: number;
    unreadsCount: number;
    recipient: User;
    lastMessage: Message;
}
