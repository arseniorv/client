import { ProjectStatus } from "./project-status.model";
import { RequiredExperience } from "./required-experience.model";
import { Sector } from "./sector.model";

export class Filters {
    userId?: string
    createdDate?: Date
    city?: string[]
    salaryRange?: string[]
    remote?: string[]
    contractType?: string[]
    workHours?: string[]
    search?: string
    requiredExperience?: RequiredExperience[]
    status?: ProjectStatus[];
    sector?: Sector[];
}