import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { BehaviorSubject, forkJoin, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Competency } from '../models/competency.model';

const GET_ALL_COMPET = gql`
query {
  getCompetencies {
  	_id
    name
  }
}
`;

const GET_COMPET = gql`
query getDetailCompet($id: String!) {
  getCompetence(id: $id) {
    _id
    name
  }
}
`;

const MUT_CREATE_COMPET = gql`
mutation createCompet($name: String!) {
  createCompetence(
    competenciesDto: { 
      name: $name
    }    
  ) {
    _id
    name
  }
}
`;

@Injectable({
  providedIn: 'root'
})
export class CompetenciesService {

  private _competencies: BehaviorSubject<Competency[]>
  private _competenciesQuery: QueryRef<any>

  constructor(private apollo: Apollo) {
    this._competencies = new BehaviorSubject(undefined)
    this.init()
  }

  init() {
    // Set competencies query
    this._competenciesQuery = this.apollo.watchQuery({
      query: GET_ALL_COMPET
    });
    this.fetchCompetencies()
  }

  get competencies(): Observable<Competency[]> {
    return this._competencies.asObservable()
  }

  getCompetencies(): Competency[] {
    return this._competencies.value
  }
    
  fetchCompetencies(callback: Function = () => {}) {
    if(this._competencies.getValue() === undefined){
      // Initialize observable
      this._competenciesQuery.valueChanges.pipe(
        map((response: any) => response.data.getCompetencies.map(competency => ({ _id: competency._id, name: competency.name})))
      ).subscribe((competencies: Competency[]) => {
          // Update competencies
          this._competencies.next(competencies)
        })
      // Trigger callback once
      this._competenciesQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
    } else {
      // Trigger callback after refetch
      this._competenciesQuery.refetch().then(response => callback(response))
    }
  }

  checkCompetency(name: string): Competency | undefined {
    return this._competencies.value.find(c => c.name.trim().toLowerCase() === name.trim().toLowerCase())
  }

  createCompetencies(competenciesNames: string[]): Observable<Competency[]> {
    let newCompetencies:Array<Observable<Competency>> = []
    for (let i=0; i<competenciesNames.length; i++) {
      const newCompetency = this.createCompetency(competenciesNames[i]);
      newCompetencies.push(newCompetency);
    }
    return forkJoin(newCompetencies)
  }

  createCompetency(name: string): Observable<Competency> {
    return this.apollo.mutate({
        mutation: MUT_CREATE_COMPET,
        refetchQueries: [{ query: GET_ALL_COMPET }],
        variables: {
          name
        }
    }).pipe(
      map((data: any) => {
        return { 
          _id: data.data.createCompetence._id, 
          name: data.data.createCompetence.name
        }
      })
    );
  }

  findOrCreateCompentencies(competenciesNames: string[]): Observable<Competency[]>{
    let existingCompetencies: Competency[] = []
    let competenciesToCreate: string[] = []
    competenciesNames.forEach(name => {
      let competency = this.checkCompetency(name)
      if(competency !== undefined){
        existingCompetencies.push(competency)
      } else {
        competenciesToCreate.push(name)
      }
    })
    if(competenciesToCreate.length > 0){
      return this.createCompetencies(competenciesToCreate).pipe(map(
        newCompetencies => [
          ...newCompetencies,
          ...existingCompetencies
      ]))
    } else {
        return of(existingCompetencies)
    }
    
  }

  /**
   * Get All Competencies from DB.
  */
  private _competsWatchQuery: QueryRef<any, EmptyObject>;

  qGetCompetencies(): QueryRef<any, EmptyObject> {
    this._competsWatchQuery = this.apollo.watchQuery({
        query: GET_ALL_COMPET
    });

    return this._competsWatchQuery;
  }

  /**
   * Get one Competence (by ID) from DB.
  */
  private _competWatchQuery: QueryRef<any, EmptyObject>;

  qGetCompetence(userId: any): QueryRef<any, EmptyObject> {
    this._competWatchQuery = this.apollo.watchQuery({
        query: GET_COMPET,
        variables: {
          id: userId,
        }
    });
    return this._competWatchQuery;
  }

  /**
   * Mutation to Create a Compentence.
   * @returns new Competence
  */
  mCreateCompetence(iName: any) {
    return this.apollo.mutate({
        mutation: MUT_CREATE_COMPET,
        refetchQueries: [{ query: GET_ALL_COMPET }],
        variables: {
          name: iName
        }
    }).pipe(map((data: any) => {
        this._competsWatchQuery?.refetch();
        console.log(data);
        return { _id: data.data.createCompetence._id, name: data.data.createCompetence.name}
      })
    );
  }
}
