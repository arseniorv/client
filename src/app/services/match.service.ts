import { Injectable } from "@angular/core";
import { Apollo, gql, QueryRef } from "apollo-angular";
import { EmptyObject } from "apollo-angular/types";
// import gql from "graphql-tag";
import { map, take } from "rxjs/operators";
import { Match } from "../models/match.model";
import { BehaviorSubject, Observable } from "rxjs";
import { UserService } from "./user/user.service";
import { User } from "../models/user";

const GET_MATCHES = gql `
    query getMatches ($id: String!){
        getMatches (userID: $id){
            _id
            userID
            externalID
            userType
            matchScore {
                competencies
                softSkills
                sectors
                city
                content
                total
            }
        }
    }
`

const GET_MATCH = gql `
    query getMatch ($userID: String!,
                    $externalID: String!){
        getMatch (userID: $userID,
                    externalID: $externalID){
            _id
            userID
            externalID
            userType
            matchScore {
                competencies
                softSkills
                sectors
                city
                content
                total
            }
        }
    }
`

const MUT_CALCULATE_MATCH_BY_USER = gql`
    mutation calculateMatchMut($userID: String!){
        calculateMatchByUser(userID: $userID){
            calculatedMatches {
                uId
                eId
                match
            }
        }
    }
`

const MUT_CALCULATE_MATCH_BY_PROJECT = gql`
    mutation calculateMatchMut($projectID: String!){
        calculateMatchByProject(projectID: $projectID){
            calculatedMatches {
                uId
                eId
                match
            }
        }
    }
`

const MUT_DELETEPROJECTMATCHES = gql`
  mutation deleteProjectMatchesMut($projectID: String!) {
    deleteProjectMatches(projectID: $projectID) {
        calculatedMatches {
            uId
            eId
            match
        }
    }
  }`;

@Injectable({
    providedIn: 'root'
})
export class MatchService {

    private _me: User
    private _myMatches: BehaviorSubject<Match>
    private _match: BehaviorSubject<Match>
    private _myMatchesQuery: QueryRef<any>
    private _matchQuery: QueryRef<any>
    private _externalID: string;
    private _userID: string;
  
    constructor(
        private apollo: Apollo,
        private userService: UserService
    ) {
      this._myMatches = new BehaviorSubject(undefined);
      this._match = new BehaviorSubject(undefined);
      this.init()
    }
  
    init() {
        this.userService.me.subscribe(me => {
            if(me._id){
                this._me = me
                this._myMatchesQuery = this.apollo.watchQuery({
                    query: GET_MATCHES,
                    variables: {
                        id: this._me._id
                    }
                });
                // this.fetchMyMatches()
            }
        })
       
    }
    
    setMatchQuery(){
        this._matchQuery = this.apollo.watchQuery({
            query: GET_MATCH,
            variables: {
                userID: this._userID,
                externalID: this._externalID
            }
        });
    }
  
    get myMatches(): Observable<Match> {
      return this._myMatches.asObservable()
    }

    get match(): Observable<Match>{
        return this._match.asObservable();
    }
  
    getMyMatches(): Match {
      return this._myMatches.value
    }

    getUserMatches(id: string): QueryRef <any, EmptyObject>{
        return this.apollo.watchQuery({
            query: GET_MATCHES,
            variables: {
                id
            }
        });
    }


    fetchMyMatches(callback: Function = () => {}) {
        if(this._me){
            if(this._myMatches.getValue() === undefined){
            this._myMatchesQuery.valueChanges.pipe(
                map((response: any) => response.data.getMatches)
            ).subscribe((match: Match) => {
                this._myMatches.next(match)
                })
            // Trigger callback once
            this._myMatchesQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
            } else {
            // Trigger callback after refetch
            this._myMatchesQuery.refetch().then(response => callback(response))
            }
        }
        
    }

    fetchMatch(externalID: string, userID?: string, callback: Function = () => {}){
        this._externalID = externalID;
        if(userID && userID !== ''){
          this._userID = userID;  
        } else {
            this._userID = this._me._id;
        }

        this.setMatchQuery();
        if(this._match.value === undefined){
            // Initialize observable
            this._matchQuery.valueChanges.subscribe((response: any) => {
                console.log('[matchService] match Query response: ', response.data.getMatch);
                const match = response.data.getMatch[0].matchScore
                this._match.next(match);
            })
            this._matchQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
        } 
        else {
            console.log("[matchService] Refetching match...", this._matchQuery);
            // Trigger callback after refetch
            this._matchQuery.refetch().then((response) => callback(response))
            console.log("[matchService] Refetched: ", this._match.value);       
            
        }
    }

    calculateMyMatches() {
        console.log("Calculating matched ofr user",this._me)
        return this.apollo.mutate({
            mutation: MUT_CALCULATE_MATCH_BY_USER,
            variables: {
                userID: this._me._id
            }
        }).pipe(
            map((data) => {
                console.log("Match calculated", data)
                this._myMatchesQuery?.refetch();
                return data
            })
        )
    }

    mCalculateMatchByProject(projectID: string){
        return this.apollo.mutate({
            mutation: MUT_CALCULATE_MATCH_BY_PROJECT,
            variables: {
                projectID: projectID
            }
        }).pipe(
            map((data) => {
                this._myMatchesQuery?.refetch();
            })
        )
    }

    mDeleteProjectMatches(projectID: string){
        return this.apollo.mutate({
            mutation: MUT_DELETEPROJECTMATCHES,
            variables: {
              projectID: projectID
            }
          }).pipe(
            map((data) => {
              this._myMatchesQuery?.refetch();
            })
          )
    }
}
