import { inject, Injectable } from "@angular/core";
import { Apollo, QueryRef } from "apollo-angular";
import { BehaviorSubject, map, Observable, take } from "rxjs";
import { Filters } from "src/app/models/filters.model";
import { InputProject, Project } from "src/app/models/project";
import * as requests from './requests'
import { User } from "src/app/models/user";
import { UserService } from "../user/user.service";
import { MatchService } from "../match.service";


@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

    private _me: User;
    private _projectsQuery: QueryRef<any>;
    private _currentProjectQuery: QueryRef<any>;
    private _createProjectQuery: QueryRef<any>;
    private _projects: BehaviorSubject<Project[]>;
    private _currentProject: BehaviorSubject<Project>;
    private _filter: BehaviorSubject<Filters>;
    private _limit: number;
    private _defaultLimit: number = 10;
    private _skip: number;
    private _defaultSkip: number = 0;
    private _projectId: string;
    private _myMatches: any;
    private _updateProjectFlag: boolean = false;
    private _inputProject: InputProject;

    private apollo = inject(Apollo);
    private userService = inject(UserService);
    private matchService = inject(MatchService);

    constructor(){
        this._me = {} as User;
        this._projects = new BehaviorSubject(undefined);
        this._currentProject = new BehaviorSubject(undefined);
        this._filter = new BehaviorSubject({} as Filters);
        this.userService.me.subscribe(me => this._me = me);
        this.matchService.myMatches.subscribe(matches => {
            if(matches){
                this._myMatches = Object.values(matches);
            }
        })
        // tengo puesto esto para intentar que al traer todos los proyectos, vaya uno por uno comparando con este array y cogiendo el match en cada caso

    }

    setProjectsQuery(){
        this._projectsQuery = this.apollo.watchQuery({
            query: requests.GET_ALL_PROJECTS,
            variables: {
              userID: this._me._id,
              filter: this._filter.value,
              limit: this._limit,
              skip: this._skip
            }
          });
    }

    setCurrentProjectQuery(){
        this._currentProjectQuery = this.apollo.watchQuery({
            query: requests.GET_PROJECT,
            variables: {
                id: this._projectId
            }
        })

    }

    setCreateProjectQuery(){
        this._createProjectQuery = this.apollo.watchQuery({
            query: requests.MUT_CREATE_PROJECT,
            variables: this._inputProject
        })
    }

    get filters(): Observable<Filters>{
        return this._filter.asObservable();
      }
    
    setFilters(newFilters?: Filters){
        this._filter.next(newFilters);
        console.log('this._filter: ', this._filter.value)
    }

    fetchProjects(
        limit: number = this._defaultLimit, 
        skip: number = this._defaultSkip, 
        callback: Function = () => {},
        updateQuery?: boolean       
    ){
        
        this._limit = limit
        this._skip = skip
        this.setProjectsQuery()
        // TODO: refactor updateQuery as in offersService
        if(this._projects.value === undefined || this._updateProjectFlag || updateQuery){
            // Initialize observable
            this._projectsQuery.valueChanges.subscribe((response: any) => {
                // If new project or project updated, clear projects array before refetching
                if (this._updateProjectFlag){    
                    this._projects.next([]);
                    this._updateProjectFlag = false;
                }
                console.log("[projectsService] Response data: ", response.data)
                const projects = response.data.getAllProjects
                    .map((project)=>({
                        ...project,
                        enrolled: project.interestedUsers.findIndex(item => item._id === this._me._id) !== -1
                    }))
                    if(this._me){
                        this.matchService.fetchMyMatches((matches) => {
                            this._myMatches = matches.data.getMatches;
                            for (let project of projects){
                                for (let match of this._myMatches){
                                    if (project._id === match.externalID){
                                        project.match = Math.round(match.matchScore.total)
                                    }
                                }
                            }
                        }) 
                    }
                this._projects.next(projects);    
            })
            this._projectsQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
        } else {
            console.log("[projectsService] Refetching...", this._projectsQuery);
            // Trigger callback after refetch
            this._projectsQuery.refetch().then(response => callback(response));
            console.log("[projectsService] Refetched: ", this._projects.value);
        }

    }

    fetchCurrentProject(projectID: string, callback: Function = () => {}){
        this._projectId = projectID;
        if(this._projects.value !== undefined){
            let project = this._projects.value.find((project) => project._id === projectID);
            let currentProject = {
                ...project,
                user: project.user,
                adminsInfo: [],
                enrolled: project.interestedUsers.findIndex(item => item._id === this._me._id) !== -1
            }
            this.getAdminsInfo(currentProject);
            this.matchService.fetchMatch(currentProject._id, '', (match)=>{
                currentProject.match = match.data.getMatch[0].matchScore.total;
                this._currentProject.next(currentProject);
            })
            
        } else {
            this.setCurrentProjectQuery(); 
            if(this._currentProject.value === undefined){
                // Initialize observable
                this._currentProjectQuery.valueChanges.subscribe((response: any) => {
                    console.log("[projectsService] Response data CurrentProjectQuery: ", response.data)
                    const project = response.data.getProject
                    this.userService.me.subscribe(()=>{
                        const currentProject = {
                            ...project,
                            user: project.user,
                            adminsInfo: [],
                            enrolled: project.interestedUsers.findIndex(item => item._id === this._me._id) !== -1
                        }
                        this.getAdminsInfo(currentProject);
                        this.matchService.fetchMatch(currentProject._id, '', (match)=>{
                            currentProject.match = match.data.getMatch[0].matchScore.total;
                            this._currentProject.next(currentProject);
                        })
                    })
                        
                         
                })
                this._currentProjectQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
            } else {
                console.log("[projectsService] Refetching current project...", this._currentProjectQuery);
                // Trigger callback after refetch
                this._currentProjectQuery.refetch().then(response => callback(response));
                console.log("[projectsService] Refetched: ", this._currentProject.value);
            }
            
        }    
    }

    get projects(): Observable<Array<Project>>{
        return this._projects.asObservable();
    }

    get currentProject(): Observable<Project> {
        return this._currentProject.asObservable();
    }

    getAdminsInfo(currentProject){
        currentProject.admins.map((admin) => {
            this.userService.qGetUser(admin._id).valueChanges.subscribe((response)=>{
                currentProject.adminsInfo.push(response.data.getUser);
            })
        })
    }

    createProject(inputProject: InputProject){
        this._inputProject = inputProject
        return this.apollo.mutate({
            mutation: requests.MUT_CREATE_PROJECT,
            variables: inputProject
        }).pipe(
            map((result) => {
                this._updateProjectFlag = true;
                this.fetchProjects();
                const project: any = result.data
                return project
            })
        )
    }

    editProject(inputProject: InputProject){
        this._inputProject = inputProject

        return this.apollo.mutate({
            mutation: requests.MUT_EDIT_PROJECT,
            variables: this._inputProject
        }).pipe(
            map((result) => {
                this._updateProjectFlag = true;
                this.fetchProjects();
                const project: any = result.data
                return project
            })
        )
    }
    

    addInterestedUser(pId: string, uId: string) {
        return this.apollo.mutate({
            mutation: requests.MUT_ADD_INTERESTED_USER,
            variables: {
                projectId: pId,
                interestedUserId: uId,
            }
        }).pipe(
            map((data) => {
                this._currentProjectQuery?.refetch();
                this._projectsQuery?.refetch();
            })
        );
    }

    removeInterestedUser(pId: string, uId: string) {
        return this.apollo.mutate({
            mutation: requests.MUT_REMOVE_INTERESTED_USER,
            variables: {
                projectId: pId,
                interestedUserId: uId
            }
        }).pipe(
            map((data) => {
                this._currentProjectQuery?.refetch();
                this._projectsQuery?.refetch();
            })
        );
    }
}