import { gql } from "apollo-angular";

export const GET_ALL_PROJECTS = gql`
query getAllProjects(
                  $userID: String,
                  $filter: ProjectFilter, 
                  $limit: Float, 
                  $skip: Float) {
  getAllProjects(
              userID: $userID, 
              filter: $filter, 
              limit: $limit, 
              skip: $skip) {
    _id
    userId
    title
    description
    sector {
        _id
        name
    }
    competencies {
        _id
        name
    }
    city
    user {
      _id
      name
      avatarB64
    }
    softSkills {
        _id
        name
    }
    status
    state
    createdDate
    files
    interestedUsers {
      _id
      name
    }
    admins {
      _id
      email
    }
  }
}
`;

export const GET_PROJECT = gql`
query getDetailProject($id: String!) {
  getProject(id: $id) {
    _id
    userId
    title
    description
    sector {
      _id
      name
    }
    competencies {
      _id
      name
    }
    city
    user {
      _id
      name
      avatarB64
    }
    softSkills {
      _id
      name
    }
    status
    createdDate
    files
    interestedUsers
      {
        _id
        name
      }
    state
    admins {
      _id
      email
    }
  } 
}
`;

export const MUT_CREATE_PROJECT = gql`
    mutation createProjectMut(
        $userId: String!,
        $title: String!,
        $description: String!,
        $sector: [EmbeddedSector!]!,
        $competencies: [EmbeddedCompetency!]!,
        $city: String!,
        $softSkills: [EmbeddedSoftSkill!]!,
        $status: ProjectStatus,
        $createdDate: DateTime!,
        $files: [String!]!,
        $admins: [Members!]!
    ){
        createProject(
            createProjectDto: {
                userId: $userId,
                title: $title,
                description: $description,
                sector: $sector,
                competencies: $competencies,
                city: $city,
                user: $userId,
                softSkills: $softSkills,
                status: $status,
                createdDate: $createdDate,
                files: $files,
                admins: $admins
            }
        ){
            _id
            userId
            title
            description
            sector {
                _id
                name
            }
            competencies {
                _id
                name
            }
            city
            softSkills {
                _id
                name
            }
            status
            createdDate
            files
            admins {
              _id
              email
            }
        }
    }
`;

export const MUT_EDIT_PROJECT = gql`
    mutation editProjectMut(
        $id: String!,
        $userId: String!,
        $title: String!,
        $description: String!,
        $sector: [EmbeddedSector!]!,
        $competencies: [EmbeddedCompetency!]!,
        $city: String!,
        $softSkills: [EmbeddedSoftSkill!]!,
        $status: ProjectStatus,
        $files: [String!]!,
        $admins: [Members!]!
    ){
        editProject(
          id: $id
          userId: $userId
          projectInputs: {
              title: $title,
              description: $description,
              sector: $sector,
              competencies: $competencies,
              city: $city,
              softSkills: $softSkills,
              status: $status,
              files: $files,
              admins: $admins
          }
        ){
            _id
            userId
            title
            description
            sector {
                _id
                name
            }
            competencies {
                _id
                name
            }
            city
            softSkills {
                _id
                name
            }
            status
            files
            admins {
              _id
              email
            }
        }
    }
`;

export const MUT_ADD_INTERESTED_USER = gql`
  mutation addInterestedUser (
    $projectId: String!,
    $interestedUserId: String!
  ) {
    addInterestedUser(
      projectId: $projectId,
      interestedUserId: $interestedUserId
    ) {
      title
    }
  }
`;

export const MUT_REMOVE_INTERESTED_USER = gql`
  mutation removeInterestedUser (
    $projectId: String!,
    $interestedUserId: String!
  ) {
    removeInterestedUser(
      projectId: $projectId,
      interestedUserId: $interestedUserId
    ) {
      title
    }
}
`;