import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route, Router, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, from, map } from 'rxjs';
import { UserService } from './user/user.service';
import { setContext } from '@apollo/client/link/context';
import { Apollo, gql } from 'apollo-angular';
import { Observable } from '@apollo/client/utilities';

interface Credentials {
  email: string, 
  password: string
}

interface RouteResult { route: string };


export const GET_JWT = gql`
  query getJWTOfUser($email: String!, $password: String!) {
    getJWTByEmailAndPassword(email: $email, password: $password) {
      token
    }
  }
`;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private currentUser: BehaviorSubject<any> = new BehaviorSubject(null);
  private _isLoggedIn = new BehaviorSubject(window.localStorage.getItem('token') ? true : false);
  userType = '0';
  defaultHref: Observable<RouteResult>;
  disabledBackButtonPages: Array<string>

  constructor(
    private router: Router,
    private apollo: Apollo,
    private userService: UserService
  ) {
    // If logged, fetch me
    if(!!this._isLoggedIn.value){
      this.userService.fetchMe()
    }
    // Update default ref depending on user type
    this.defaultHref = new Observable<RouteResult>(observer => {
      // Subscribe to your user service
      this.userService.me.subscribe(me => {
        console.log('me', me);
        const href = me.type === undefined ? '/login' : 
                    me.type === 'candidate' ? '/offer-list' : '/company-offer';
        
        // Emit the value through the observer
        observer.next({ route: href });
      });

    })

    // Disable back button on certain pages
    this.disabledBackButtonPages = ['/offer-list', '/company-offer']
  }

  //TODO: Implement CanLoad to avoid a module to be loaded without Logging In.
  canLoad(route: Route): boolean {
    if (this.isLogged()) {
      return true;
    }
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.isLogged()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

  isLogged() {
    return this._isLoggedIn.value;
  }

  get isLoggedIn() {
    return this._isLoggedIn.asObservable();
  }

  onLogin() {
    this._isLoggedIn.next(true);
    this.setTokenHeader()
  }

  async logout() {
    await sessionStorage.clear();
    await localStorage.clear();
    this._isLoggedIn.next(false);
    this.userService.forgetMe();
    window.location.assign('/');
  }

  setTokenHeader() {
    console.log("Setting token header")
    setContext(async (_, { headers }) => {
      // Grab token if there is one in storage or hasn't expired
      let token = window.localStorage.getItem('token')
    
      // Return the headers as usual
      return {
        headers: {
          Authorization: token === '' ? '' : `Bearer ${token}`,
        },
      };
    });
  }

  login(credentials: Credentials, callback: Function = () => {} ): void{
    this.getToken(credentials).subscribe((token) => {
      console.log("Got token, fetching me...", token)
      if(token){
        this._isLoggedIn.next(true)
        this.userService.fetchMe(response => {
          const me = response.data.me;
          console.log("Got me:", me)
          this.useSessionStorage(me._id, me.name, me.email, me.type, me.softSkills.map(s => s.name));
          // if (window.sessionStorage) {
          //   sessionStorage.removeItem('userSoftSkills');
          // }
          callback(response.data.me)
        })
      } else {
        callback(false)
      }
    })
  }

  getToken(credentials: Credentials) {
    return this.apollo.query<string>({
      query: GET_JWT,
      variables: {
        ...credentials
      }
    }).pipe(
      map((response: any) => {
        let token: string = response.data.getJWTByEmailAndPassword.token
        localStorage.setItem('token', token);
        this.setTokenHeader()
        return token
      })
    )
  }

  useSessionStorage(id, name, mail, type, softskills) {
    sessionStorage.setItem('userid', id);
    sessionStorage.setItem('user', name);
    sessionStorage.setItem('email', mail);
    sessionStorage.setItem('type', type);
    sessionStorage.setItem('userSoftSkills', softskills);
  }

}
