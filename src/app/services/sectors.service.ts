import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { BehaviorSubject, map, Observable, take } from 'rxjs';
import { Sector } from '../models/sector.model';

const GET_ALLSECTORS = gql`
query {
  getSectors {
    _id
    name
  }
}
`;

const GET_SECTOR = gql`
query getSectorDetail($id: String!){
  getSector(id: $id) {
    _id
    name
  }
}
`;

@Injectable({
  providedIn: 'root'
})
export class SectorsService {

  private _sectors: BehaviorSubject<Sector[]>
  private _sectorsQuery: QueryRef<any>

  constructor(private apollo: Apollo) {
    this._sectors = new BehaviorSubject(undefined)

    this.init()
  }

  init() {

    this._sectorsQuery = this.apollo.watchQuery({
      query: GET_ALLSECTORS,
    });
    this.fetchSectors()
  }

  get sectors(): Observable<Sector[]> {
    return this._sectors.asObservable()
  }

  getSectors(): Sector[] {
    return this._sectors.value
  }

  fetchSectors(callback: Function = () => {}) {
    if(this._sectors.getValue() === undefined){
      // Initialize observable
      this._sectorsQuery.valueChanges.pipe(
        map((response: any) => response.data.getSectors.map(sector => ({ _id: sector._id, name: sector.name})))
      ).subscribe((sectors: Sector[]) => {
          // Update sectors
          this._sectors.next(sectors)
        })
      // Trigger callback once
      this._sectorsQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
    } else {
      // Trigger callback after refetch
      this._sectorsQuery.refetch().then(response => callback(response))
    }
  }

  /**
   * Get All Sector from DB.
   */

  qGetAllSectors(): QueryRef<any, EmptyObject> {
    this._sectorsQuery = this.apollo.watchQuery({
      query: GET_ALLSECTORS,
    });

    return this._sectorsQuery;
  }

  /**
   * Get one Sector (by ID) from DB.
   */
  private _sectorWatchQuery: QueryRef<any, EmptyObject>;

  qGetSector(idValue: any): QueryRef<any, EmptyObject> {
    this._sectorWatchQuery = this.apollo.watchQuery({
      query: GET_SECTOR,
      variables: {
        id: idValue,
      }
    });
    return this._sectorWatchQuery;
  }
}
