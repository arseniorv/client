import { Injectable } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import { EmptyObject, MutationResult } from 'apollo-angular/types';
import { Observable, BehaviorSubject, take, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import * as requests from './requests'
import { InputUser, User } from 'src/app/models/user';
import { SoftSkill } from 'src/app/models/softskill.model';

interface Options<T> {
  variables: T,
  success: Function,
  error: Function
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _meQuery: QueryRef<any>
  private _me: BehaviorSubject<User>
  private _tokenQuery: QueryRef<any>
  private _refetchMeFlag: boolean

  constructor(private apollo: Apollo) {
    this._me = new BehaviorSubject({} as User)
    // Get me query
    this._meQuery = this.apollo.watchQuery({
      query: requests.GET_ME
    });
    // Get token query
    this._tokenQuery = this.apollo.watchQuery({
      query: requests.GET_CHAT_TOKEN
    });
    this._refetchMeFlag = false
  }

  getMe(): User {
    return this._me.value
  }

  get me(): Observable<User> {
    return this._me.asObservable()
  }

  // Actions
  fetchMe(callback: Function = () => { }): void {
    if (this._me.getValue()._id === undefined || this._refetchMeFlag) {
      // Initialize me observable
      this._meQuery.valueChanges
        .subscribe((response: any) => {
          // Get chat token
          const me = response.data.me
          this._tokenQuery.valueChanges
            .subscribe((response: any) => {
              const newMe = { ...me }
              newMe.chatAuthToken = response.data.getChatToken.chatAuthToken
              this._me.next(newMe)
            })
        })
      // Trigger callback once
      this._meQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
    } else {
      // Trigger callback after refetch
      this._meQuery.refetch().then(response => callback(response))
    }
  }
  
  forgetMe() {
    this._meQuery = undefined;
    this._me.complete()
  }

  createUser(inputUser: InputUser): Observable<MutationResult<any>> {
    return this.apollo.mutate({
      mutation: requests.MUT_CREATEUSER,
      variables: inputUser
    })
      // .pipe(take(1))
      // .subscribe({
      //   next: (response) => {
      //     console.log("[createNewUser] response: ", response)
      //     options.success()
      //   },
      //   error: async (error) => {
      //     options.error(error)
      //   }
      // });
  }

  checkEmail(email: string): Observable<any> {
    return this.apollo.query({
      query: requests.GET_EMAIL_EXISTS,
      variables: {
        email
      }
    }).pipe(map((response: any) => response.data?.emailExists ))
  }

  updateUserSkills(skillsList: SoftSkill[]) {
    return this.apollo.mutate({
      mutation: requests.MUT_EDIT_USERSKILLS,
      variables: {
        id: this._me.getValue()._id,
        softSkills: skillsList
      }
    }).pipe(
      map(() => {
        this._refetchMeFlag = false
        this.fetchMe()
      })
    );
  }
  
  
  // Upload filename (CV or cover letter), filename is passed
  uploadFile(filename: String, file: Blob, type: 'CV' | 'Cover_Letter' | 'other', callback: Function = () => {}){
    
    // Call apollo client - ask for Backblace/Storage pre-signed URL
    console.log(`GraphQL query getUploadUrl : filename : ${filename}`)
    this.apollo.query({
      query: requests.GET_UPLOAD_FILE_URL,
      variables: {
        filename
      }
    })
    .subscribe((response: any) => {
      
      console.log(`Apollo Response (getUploadUrl):`);console.log(response);
      console.log(`Storage URL:`);console.log(response.data?.uploadUrl?.presignedUrl);
      const formData = new FormData(); // Browser API
      formData.append('file', file)
      
      //PUT request
      fetch(response.data?.uploadUrl?.presignedUrl, { method: 'PUT', body: formData })
        .then((response) => {
          if (response.ok) {
            if (type === 'CV')
              this.updateCV(filename).subscribe(() => callback())
            else if (type === 'Cover_Letter')
              this.updateCoverLetter(filename).subscribe(() => callback())
          }
          // If not OK
          else {
            console.error(`PUT failed:`);console.error(response);
              
          }
        })
        .catch((error) => {
            console.error(`PUT error:`);console.error(error);
        })
    })
    // TODO - how to handle GQL error (??)
    // .catch((error) => { 
        // console.error(`Apollo error:`);console.error(error);
    // })
  }

  
  // Store (new) filename into MongoDB?
  updateCV(filename: String) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDATE_CV,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this.fetchMe()
      })
    );
  }

  updateCoverLetter(filename: String) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDATE_COVER_LETTER,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this.fetchMe()
      })
    );
  }

  // ----------------------------------------------------------------- 
  // The code under this comment need to be reviewd and/or refactored
  // ----------------------------------------------------------------- 

  /**
   * Get Users Login from DB.
   */
  private _allUsersWatchQuery: QueryRef<any, EmptyObject>;

  /*qGetUsersLog(): QueryRef<any, EmptyObject> {
    this._allUsersWatchQuery = this.apollo.watchQuery({
        query: requests.GET_USERLOGIN
    });

    return this._allUsersWatchQuery;
  }*/

  qGetAllUsers(): QueryRef<any, EmptyObject> {
    this._allUsersWatchQuery = this.apollo.watchQuery({
      query: requests.GET_ALLUSERS
    });

    return this._allUsersWatchQuery;
  }

  /**
   * Get one User (by ID) from DB.
   */
  private _oneUserWatchQuery: QueryRef<any, EmptyObject>;

  qGetUser(userId: any): QueryRef<any, EmptyObject> {
    this._oneUserWatchQuery = this.apollo.watchQuery({
      query: requests.GET_USER,
      variables: {
        id: userId,
      }
    });
    return this._oneUserWatchQuery;
  }

  qGetUserByEmail(email: any): QueryRef<any, EmptyObject> {
    this._oneUserWatchQuery = this.apollo.watchQuery({
      query: requests.GET_USER_BY_EMAIL,
      variables: {
        email: email
      }
    });
    return this._oneUserWatchQuery;
  }

  // private _meQuery: QueryRef<any, EmptyObject>;
  private _tokenWatchQuery: QueryRef<any, EmptyObject>;

  qGetMe(): QueryRef<any, EmptyObject> {
    if (!this._meQuery) {
      this._meQuery = this.apollo.watchQuery({
        query: requests.GET_ME
      });
    }
    return this._meQuery;
  }


  // TODO: remove, moved to auth service
  qGetJWT(email: String, password: String): Observable<any> {
    return this.apollo.query({
      query: requests.GET_JWT,
      variables: {
        email,
        password
      }
    });
  }

  qGetUploadUrl(filename: String): Observable<any> {
    return this.apollo.query({
      query: requests.GET_UPLOAD_FILE_URL,
      variables: {
        filename
      }
    })
  }

  qGetDownloadCVUrl(id?: String): Observable<any> {
    return this.apollo.query({
      query: requests.GET_DOWNLOAD_CV_URL,
      variables: {
        id
      }
    })
  }

  qGetDownloadCoverLetterUrl(id?: String): Observable<any> {
    return this.apollo.query({
      query: requests.GET_DOWNLOAD_COVER_LETTER_URL,
      variables: {
        id
      }
    })
  }

  mUserAvatarUpdate(userId, avatarB64: string) {

    return this.apollo.mutate({
      mutation: requests.MUT_EDIT_AVATAR,
      variables: {
        id: userId,
        avatarB64: avatarB64
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );

  }

  mUserSkillUpdate(userId, skillsList) {
    return this.apollo.mutate({
      mutation: requests.MUT_EDIT_USERSKILLS,
      variables: {
        id: userId,
        softSkills: skillsList
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );
  }

  mUserPreferencesUpdate(userId, preferences) {

    return this.apollo.mutate({
      mutation: requests.MUT_EDIT_PREFERENCES,
      variables: {
        id: userId,
        preferences: preferences
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );

  }

  /**
  * Mutation to Edit user profile
  */
  mEditUser(userId: string, userInput: InputUser) {
    return this.apollo.mutate({
      mutation: requests.MUT_EDIT_USER,
      variables: {
        id: userId,
        userInput: userInput
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );
  }

  /**
   * Mutation to create a new user
   */
  mCreateUser(inputUser: InputUser) {
    return this.apollo.mutate({
      mutation: requests.MUT_CREATEUSER,
      variables: inputUser
    })
    // .pipe(
    //   map((data) => {
    //     this._allUsersWatchQuery?.refetch();
    //   })
    // );
  }

  mUpdateCV(filename: String) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDATE_CV,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );
  }

  mUpdateCoverLetter(filename: String) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDATE_COVER_LETTER,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meQuery?.refetch();
      })
    );
  }

  mGeneratePasswordResetToken(email: String): Observable<any> {
    return this.apollo.mutate({
      mutation: requests.MUT_GENERATE_PASSWORD_RESET_TOKEN,
      variables: {
        email: email
      }
    }).pipe(
      map((data) => {
        data
      })
    );
  }

  mResetPassword(token: String, password: String): Observable<any> {
    return this.apollo.mutate({
      mutation: requests.MUT_RESET_PASSWORD,
      variables: {
        token,
        password
      }
    }).pipe(
      map((data) => {
        data
      })
    );
  }

  mCreateChatRoomIfNotExists(loggedUserID: string, interlocutorID: string): Observable<any> {
    return this.apollo.mutate({
      mutation: requests.MUT_CREATE_CHAT,
      variables: {
        loggedUserID,
        interlocutorID
      }
    }).pipe(
      map((data) => {
        data
        this._oneUserWatchQuery?.refetch();
        this._tokenWatchQuery?.refetch();
      })
    );
  };

}
