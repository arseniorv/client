import { gql } from 'apollo-angular';

// TODO: remove from this service
export const GET_JWT = gql`
  query getJWTOfUser($email: String!, $password: String!) {
    getJWTByEmailAndPassword(email: $email, password: $password) {
      token
    }
  }
`;

export const GET_USER = gql`
  query getOneUser($id: String!) {
    getUser(id: $id) {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
      cv
      coverLetter
      avatarB64
      rooms {
        userId
        roomId
      }
    }
  }
`;

export const GET_USER_BY_EMAIL = gql`
  query getUserByEmail($email: String!) {
    getUserByEmail(email: $email) {
      _id
      email
    }
  }
`;

export const GET_ME = gql`
  query {
    me {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      website
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
      cv
      coverLetter
      avatarB64
      chatUserId
      chatAuthToken
      rooms {
        userId
        roomId
      }
    }
  }
`

export const GET_CHAT_TOKEN = gql`
  query {
    getChatToken {
        chatUserId
        chatAuthToken
    }
  }
`

export const GET_UPLOAD_FILE_URL = gql`
  query getUploadUrl($filename: String!) {
    uploadUrl(filename: $filename) {
      presignedUrl
    }
  }
`

export const GET_DOWNLOAD_CV_URL = gql`
  query getDownloadCVUrl($id: String) {
    downloadCvUrl(id: $id) {
      presignedUrl
    }
  }
`

export const GET_DOWNLOAD_COVER_LETTER_URL = gql`
  query getDownloadCoverLetterUrl($id: String) {
    downloadCoverLetterUrl(id: $id) {
      presignedUrl
    }
  }
`

export const GET_ALLUSERS = gql`
  query {
    getUsers {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
    }
  }
`;

export const MUT_EDIT_AVATAR = gql`
mutation editUserAvatar(
                          $id: String!,
                          $avatarB64: String!
                        ){
  updateUser(
    id: $id,
    userInputs: {
      avatarB64: $avatarB64
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
    avatarB64
  }
}
`;

export const MUT_EDIT_USERSKILLS = gql`
mutation editUserSkills(
  $id: String!,
  $softSkills: [EmbeddedSoftSkill!]
) {
  updateUser(
    id: $id,
    userInputs: {
      softSkills: $softSkills
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
  }
}
`;

export const MUT_EDIT_PREFERENCES = gql`
mutation editUserPreferences(
                          $id: String!,
                          $preferences: PreferencesInput
                        ) {

  updateUser(
    id: $id,
    userInputs: {
      preferences: $preferences
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
  }
}
`;

export const MUT_EDIT_USER = gql`
mutation editUser (
                    $id: String!,
                    $userInput: UpdateUserInput!
                  ) {

  updateUser(
    id: $id,
    userInput: $userInput
  ) {
      _id
      name
      surname
      email
      city
      website
      sector {
        _id
        name
      }
      eduLevel
      jobPosition
      lastJobTasks
      experience
      languages
      competencies {
        _id
        name
      }
      video
    }
}
`;

export const MUT_CREATEUSER = gql`
mutation createUserMut( $name: String!,
                        $surname: String!,
                        $email: String!,
                        $password: String!,
                        $type: String!,
                        $legalForm: LegalForm,
                        $city: String!,
                        $website: String,
                        $sector: [EmbeddedSector!]!,
                        $eduLevel: String!,
                        $jobPosition: String!,
                        $lastJobTasks: String!,
                        $experience: String!,
                        $languages: [String!]!,
                        $competencies: [EmbeddedCompetency!]!,
                        $softSkills: [EmbeddedSoftSkill!]!,
                        $commsOK: Boolean!,
                        $privacyPolicyOK: Boolean!,
                        $createdDate: DateTime!,
                        $avatarB64: String) {
  createUser(
      createUserDto: { 
        name: $name
        surname: $surname
        email: $email
        password: $password
        type: $type
        legalForm: $legalForm
        city: $city
        website: $website
        sector: $sector
        eduLevel: $eduLevel
        jobPosition: $jobPosition
        lastJobTasks: $lastJobTasks
        experience: $experience
        languages: $languages
        competencies: $competencies
        softSkills: $softSkills
        commsOK: $commsOK
        privacyPolicyOK: $privacyPolicyOK,
        createdDate: $createdDate,
        avatarB64: $avatarB64
      }    
  ) {
      _id
      name
      surname
      email
      website
      type
      legalForm    
      city
      website
      sector {
        _id
        name
      }
      eduLevel
      jobPosition
      lastJobTasks
      experience
      languages
      competencies {
        _id
        name
      }
      softSkills {
        _id
        name
      }
      commsOK
      privacyPolicyOK
      createdDate
      avatarB64
    }
}
`;

export const GET_EMAIL_EXISTS = gql`
  query emailExists($email: String!) {
    emailExists(email: $email)
  }
`

export const MUT_UPDATE_CV = gql`
  mutation updateCV($filename: String) {
    cv(filename: $filename) {
      _id
      cv
    }
  }
`

export const MUT_UPDATE_COVER_LETTER = gql`
  mutation updateCoverLetter($filename: String) {
    coverLetter(filename: $filename) {
      _id
      coverLetter
    }
  }
`

export const MUT_GENERATE_PASSWORD_RESET_TOKEN = gql`
  mutation generatePasswordResetToken($email: String!){
    resetToken(email: $email) 
  }
`

export const MUT_RESET_PASSWORD = gql`
  mutation resetPasswordMut($token: String!, $password: String!) {
    resetPassword(token: $token, password: $password)
  }
`

export const MUT_CREATE_CHAT = gql`
  mutation createChatMut($loggedUserID: String!, $interlocutorID: String!) {
    createChatRoomIfNotExists(loggedUserID: $loggedUserID, interlocutorID: $interlocutorID)
  }
`