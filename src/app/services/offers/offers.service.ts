import { Injectable } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { map, take } from 'rxjs/operators';
import * as requests from './requests'
import { BehaviorSubject, Observable } from 'rxjs';
import { InputOffer, Offer } from 'src/app/models/offer';
import { User } from 'src/app/models/user';
import { UserService } from '../user/user.service';
import { Filters } from 'src/app/models/filters.model';


@Injectable({
  providedIn: 'root'
})
export class OffersService {

  private _me: User

  private _offersQuery: QueryRef<any>;
  private _offers: BehaviorSubject<Offer[]>;
  private _myCandidatures: BehaviorSubject<Offer[]>;
  private _updateOffersQueryFlag: boolean = true;
  private _updatedFilterFlag: boolean = false;
  private _updatedOfferFlag: boolean = false; 

  private _offerQuery: QueryRef<any>;
  private _offerId: string;
  private _currentOffer: BehaviorSubject<Offer>;

  private _filter: BehaviorSubject<Filters>;
  private _limit: number;
  private _defaultLimit: number = 10;
  private _skip: number;
  private _defaultSkip: number = 0;

  private _offerWatchQuery: QueryRef<any, EmptyObject>;
  private _candidaturesWatchQuery: QueryRef<any, EmptyObject>;


  constructor(
    private apollo: Apollo,
    private userService: UserService
  ) {
    this._me = {} as User
    this._offers = new BehaviorSubject(undefined)
    this._myCandidatures = new BehaviorSubject(undefined)
    this._currentOffer = new BehaviorSubject(undefined)
    this._filter = new BehaviorSubject({} as Filters)
    this._limit = this._defaultLimit
    this._skip = this._defaultSkip
    this.init()
  }

  init() {
    // Depending on the type of user, offers will contain all
    // offers (candidate) or only "my" offers (company)
    this.userService.me.subscribe(me => {
      this._me = me
      if (me.type == 'candidate') {
        this.fetchOffers()
      } else if (me.type == 'company') {
        this.setFilters({
          userId: this._me._id
        })
        this.fetchOffers()
      }
    })

    this._offers.asObservable().subscribe(offers => {
      let myCandidatures = []
      if(offers.length > 0){
        myCandidatures = offers.filter((c) => (JSON.stringify(c.candidates).includes(this._me._id) === true ))
      }
      this._myCandidatures.next(myCandidatures)
    })
  }


  qGetMyCandidatures(): QueryRef<any, EmptyObject> {
    if (!this._candidaturesWatchQuery) {
      this._candidaturesWatchQuery = this.apollo.watchQuery({
        query: requests.GET_CANDIDATURES
      });
    }
    return this._candidaturesWatchQuery;
  }

  get filters(): Observable<Filters> {
    return this._filter.asObservable();
  }

  setFilters(newFilters?: Filters) {
    this._updatedFilterFlag = true
    this._updateOffersQueryFlag = true
    this._limit = this._defaultLimit
    this._skip = this._defaultSkip
    this._filter.next(newFilters);
  }

  setOffersQuery() {
    this._updateOffersQueryFlag = true
    this._offersQuery = this.apollo.watchQuery({
      query: requests.GET_ALLOFFERS,
      variables: {
        filter: this._filter.value,
        limit: this._limit,
        skip: this._skip
      }
    });
  }

  fetchOffers(
    limit: number = this._defaultLimit,
    skip: number = this._defaultSkip,
    callback: Function = () => { },
  ): void {

    // Detect change
    if (limit != this._limit || skip != this._skip || this._updateOffersQueryFlag) {
      this._limit = limit
      this._skip = skip
      this.setOffersQuery()
    }

    // Only fetch on query on init or query changed 
    if (this._offers.getValue() === undefined || this._updateOffersQueryFlag) {
      // Initialize observable
      this._offersQuery.valueChanges
        .subscribe((response: any) => {
          // If new filter, clear offers
          if (this._updatedFilterFlag) {
            this._offers.next([])
            this._updatedFilterFlag = false
          }
          // If new offer or offer updated, clear offers array before refetching
          if (this._updatedOfferFlag){    
            this._offers.next([]);
            this._updatedOfferFlag = false;
          }
          // Update offers
          const offers: Offer[] = response.data.getCompanyOffers
            .filter(offer => (offer.status === 'ACTIVE' || offer.status === null))
            .map(offer => ({
              ...offer,
              match: Math.round(offer.match),
              enrolled: offer.candidates && offer.candidates.findIndex(candidate => candidate.user._id === this._me._id) !== -1
            }))
            .sort((a, b) => (a.match > b.match) ? -1 : 1)
          this._offers.next(this._offers.value ? [...this._offers.value, ...offers] : offers)
        })
      // Trigger callback once
      this._offersQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
      this._updateOffersQueryFlag = false
    } else {
      // Trigger callback after refetch
      this._offersQuery.refetch().then(response => callback(response))
    }
  }

  get offers(): Observable<Offer[]> {
    return this._offers.asObservable()
  }

  getOffers(): Offer[] {
    return this._offers.value
  }

  get myCandidatures(): Observable<Offer[]> {
    // return this._myCandidatures.asObservable()
    return this.apollo.watchQuery({query: requests.GET_ALLOFFERS}).valueChanges.pipe(
      map((result: any) => {
        let candidatures = result.data.getCompanyOffers
        return candidatures.filter((c) => (JSON.stringify(c.candidates).includes(this._me._id) === true ));
      })
    )
  }

  // getMyCandidatures(): Offer[] {
  //   return this._myCandidatures.value
  // }

  // Warning: don't use for refetching, the setFilter call wipes the offers array each time
  fetchMyOffers(
    limit: number = this._defaultLimit,
    skip: number = this._defaultSkip,
    callback: Function = () => { },
  ): void {
    this.setFilters({
      userId: this._me._id
    })
    this.fetchOffers(limit, skip, callback)
  }

  setOfferQuery() {
    this._offerQuery = this.apollo.watchQuery({
      query: requests.GET_OFFER,
      variables: {
        id: this._offerId
      }
    })
  }

  fetchOffer(offerID: string) {
    this._offerId = offerID;
    this.setOfferQuery();
    this._offerQuery.valueChanges.subscribe((response: any) => {
      // console.log('[offersService] Current Offer response data: ', response.data);
      let offer = response.data.getOffer;
      let currentOffer = {
        ...offer,
        match: Math.round(offer.match),
        user: offer.user
      }
      this._currentOffer.next(currentOffer);
    })
  }

  get currentOffer(): Observable<Offer> {
    return this._currentOffer.asObservable()
  }

  getCurrentOffer(offerID: string) {
    // If the offers array is already loaded, pick the current offer from it
    if (this._offers.value !== undefined) {
      let offer = this._offers.value.filter(offer => offer._id === offerID)[0]
      let currentOffer = {
        ...offer,
        match: Math.round(offer.match),
        user: offer.user
      }
      this._currentOffer.next(currentOffer);
    } else {
      this.fetchOffer(offerID);
    }
  }

  createOffer(inputOffer: InputOffer): Observable<Offer> {
    return this.apollo.mutate<Offer>({
      mutation: requests.MUT_CREATEOFFER,
      variables: inputOffer
    }).pipe(
      map(result => {
        this._updatedOfferFlag = true;
        this.fetchOffers()
        return result.data
      })
    );
  }

  editOffer(inputOffer: InputOffer): Observable<Offer> {
    return this.apollo.mutate<Offer>({
      mutation: requests.MUT_EDITOFFER,
      variables: inputOffer
    }).pipe(
      map((result) => {
        this._updatedOfferFlag = true;
        this.fetchOffers()
        return result.data
      })
    );
  }


  uploadOfferFile(id: String, filename: String): Observable<any> {
    return this.apollo.query({
      query: requests.GET_UPLOAD_FILE_URL,
      variables: {
        id,
        filename
      }
    })
  }

  updateCandidateStatus(offerId: string, candidateId: string, status: string){
    return this.apollo.mutate<Offer>({
      mutation: requests.MUT_UPDOFFER_UPDATE_CANDIDATE,
      variables: {
        offerId,
        candidateId,
        status
      }
    }).pipe(
      map(() => {
        this.fetchOffer(offerId);
        this.fetchOffers();// ¿o this.getCurrentOffer()?
        this._candidaturesWatchQuery?.refetch(); 
      })
    )
  }

  /**
  * Get All Users-Offers from DB.
  */
  private _usoffsWatchQuery: QueryRef<any, EmptyObject>;

  qGetUsersOffers(): QueryRef<any, EmptyObject> {
    this._usoffsWatchQuery = this.apollo.watchQuery({
      query: requests.GET_USOFRS,
    });

    return this._usoffsWatchQuery;
  }

  cUsersOffers(created: any) {
    return this.apollo.mutate({
      mutation: requests.MUT_NEW_USERSOFFERS,
      variables: {
        createUsersOffersInput: created
      },
    }).pipe(
      map((data) => {
        this._usoffsWatchQuery?.refetch();
      })
    );
  }

  addCandidate(oId: string, cId: string) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDOFFER_ADD_CANDIDATE,
      variables: {
        offerId: oId,
        candidateId: cId
      }
    }).pipe(
      map((data) => {
        this._offerQuery?.refetch();
        this._offersQuery?.refetch();
      })
    );
  }

  removeCandidate(oId: string, cId: string) {
    return this.apollo.mutate({
      mutation: requests.MUT_UPDOFFER_REMOVE_CANDIDATE,
      variables: {
        offerId: oId,
        candidateId: cId
      }
    }).pipe(
      map((data) => {
        this._offerQuery?.refetch();
        this._offersQuery?.refetch();
      })
    );
  }

}
