import { gql } from 'apollo-angular';

export const GET_USOFRS = gql`
query {
  getUsersOffers {
    _id
    offer_id
    user_id
  }
}
`;

export const MUT_NEW_USERSOFFERS = gql`
mutation newRegister($createUsersOffersInput: CreateUsersOffersInput!) {
  createItem(input: $createUsersOffersInput) {
    _id
    offer_id
    user_id
  }
}
`;

export const GET_ALLOFFERS = gql`
query getOffers($filter: OfferFilter, $limit: Float, $skip: Float){
  getCompanyOffers(filter: $filter, limit: $limit, skip: $skip) {
    _id
    userId
    title
    eduLevel
    city
    workHours
    competencies {
      _id
      name
    }
    salaryRange
    remote
    enrolled
    contractType
    description
    requirements
    requiredExperience
    match
    createdDate
    candidates {
      status
      user {
        _id
      }
    }
    status
    user {
      _id
      name
      avatarB64
      softSkills {
        _id
        name
      }
      sector {
        name
      }
    }
    file
  }
}
`;

export const GET_OFFER = gql`
query getDetailOffer($id: String!) {
  getOffer(id: $id) {
    _id
    userId
    title
    eduLevel
    city
    workHours
    competencies {
      _id
      name
    }
    salaryRange
    remote
    enrolled
    contractType
    description
    requiredExperience
    requirements
    createdDate
    status
    user {
      _id
      name
      avatarB64
      softSkills {
        _id
        name
      }
      sector {
        name
      }
      website
      rooms {
        userId
        roomId
      }
    }
    match
    candidates {
      user {
        _id
        avatarB64
        name
        surname
        email
        city
        jobPosition
        softSkills {
          _id
          name
        }
      }
      status
      match {
        total 
      }
    }
    file
  }
}
`;


export const GET_CANDIDATURES = gql`
  query {
    myCandidatures {
      _id
      userId
      title
      eduLevel
      city
      workHours
      competencies {
        _id
        name
      }
      salaryRange
      remote
      enrolled
      contractType
      requiredExperience
      description
      requirements
      createdDate
      user {
        _id
        name
        avatarB64
        softSkills {
          _id
          name
        }
        sector {
          name
        }
      }
    }
  }
`

export const MUT_CREATEOFFER = gql`
  mutation createOfferMut($userId: String!, 
                          $title: String!, 
                          $eduLevel: String!,
                          $competencies: [EmbeddedCompetency!]!,
                          $requiredExperience: RequiredExperience!,
                          $city: String!,
                          $workHours: String!, 
                          $salaryRange: String!, 
                          $remote: String!,
                          $enrolled: Float!, 
                          $contractType: String!,
                          $description: String!,
                          $requirements: String!,
                          $createdDate: DateTime!,
                          $status: OfferStatus,
                          $file: [String!]!
                          ) {
    createOffer(
      createOfferDto: { 
        userId: $userId
        title: $title
        eduLevel: $eduLevel
        competencies: $competencies
        city: $city
        workHours: $workHours
        salaryRange: $salaryRange
        requiredExperience: $requiredExperience
        remote: $remote
        enrolled: $enrolled
        contractType: $contractType
        description: $description
        requirements: $requirements
        createdDate: $createdDate
        user: $userId
        status: $status
        file: $file
      }    
    ) {
      _id
      userId
      title
      eduLevel
      competencies {
        _id
        name
      }
      city
      workHours
      salaryRange
      remote
      enrolled
      contractType
      description
      requirements
      createdDate
      status
      file
    }
  }
`;

export const MUT_EDITOFFER = gql`
  mutation editOfferMut(  $id: String!, 
                          $title: String!, 
                          $eduLevel: String!,
                          $city: String!, 
                          $workHours: String!, 
                          $competencies: [EmbeddedCompetency!]!,
                          $salaryRange: String!,
                          $requiredExperience: RequiredExperience!,
                          $remote: String!,
                          $contractType: String!
                          $description: String!
                          $requirements: String!
                          $file: [String!]!
                        ) {
    updateOffer( 
      id: $id
      offerInputs: { 
            title: $title
            eduLevel: $eduLevel
            city: $city
            workHours: $workHours
            competencies: $competencies
            salaryRange: $salaryRange
            requiredExperience: $requiredExperience
            remote: $remote
            contractType: $contractType
            description: $description
            requirements: $requirements
            file: $file
      }    
    ) {
      _id
      userId
      title
      eduLevel
      city
      workHours
      competencies {
        _id
        name
      }
      salaryRange
      remote
      enrolled
      contractType
      description
      requirements
      createdDate
      status
      file
    }
  }
`;

export const MUT_DELETEOFFER = gql`
  mutation deleteOfferMut($id: String!) {
    deleteOffer(id: $id) {
      _id
      title
    }
  }`;

export const MUT_UPDOFFER_ADD_CANDIDATE = gql`
  mutation submitAddCandidate (
    $offerId: String!,
    $candidateId: String!
  ) {
    addCandidate(
      offerId: $offerId,
      candidateId: $candidateId
    ) {
      title
    }
  }
`;

export const MUT_UPDOFFER_REMOVE_CANDIDATE = gql`
mutation submitRemoveCandidate (
  $offerId: String!,
  $candidateId: String!
) {
  removeCandidate(
    offerId: $offerId,
    candidateId: $candidateId
  ) {
    title
  }
}
`;

  export const MUT_UPDOFFER_UPDATE_CANDIDATE = gql`
    mutation submitUpdateCandidate (
      $offerId: String!,
      $candidateId: String!,
      $status: CandidatureStatus!
    ) {
      updateCandidateStatus(
        offerId: $offerId,
        candidateId: $candidateId,
        status: $status
      ) {
        title
      }
    }
  `;

export const MUT_UPDOFFER_ENROLL = gql`
  mutation submitUpdateEnroll(
                                $id: String!, 
                                $updateValue: UpdateOfferInput!
                              ) {
    updateOffer(
      id: $id, 
      offerInputs: $updateValue
    ) {
      title
      city
      enrolled
    }
  }
`;

export const MUT_UPDATE_OFFER_STATUS = gql`
    mutation submitUpdateOffer (
      $offerId: String!,
      $status: OfferStatus!
    ) {
      updateOfferStatus(
        offerId: $offerId,
        status: $status
      ) {
        title
      }
    }
  `;

  export const GET_UPLOAD_FILE_URL = gql`
  query getUploadOfferUrl($id: String!,
                          $filename: String!) {
    uploadOfferUrl(id: $id, 
                  filename: $filename) {
      presignedUrl
    }
  }
`

export const GET_DOWNLOAD_FILE = gql`
  query getDownloadFile($id: String!,
                        $file: String!) {
    downloadFile(id: $id,
                 file: $file) {
      presignedUrl
    }
  }
`
