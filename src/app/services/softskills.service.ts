/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SoftSkill } from '../models/softskill.model';

const GET_ALL_SOFTSKILLS = gql`
query {
  getSoftskills {
    _id
    name
  }
}
`;

const GET_SOFTSKILL = gql`
query getSkillDetail($id: String!){
  getSkill(id: $id) {
    _id
    name
  }
}
`;

const REFRESH = 1000;

@Injectable({
  providedIn: 'root'
})
export class SoftSkillsService {

  private _softSkills: BehaviorSubject<SoftSkill[]>
  private _softSkillsQuery: QueryRef<any>

  constructor(private apollo: Apollo) {
    this._softSkills = new BehaviorSubject(undefined)
    this.init()
  }

  init() {
    // Set softSkills query
    this._softSkillsQuery = this.apollo.watchQuery({
      query: GET_ALL_SOFTSKILLS
    });
    this.fetchSoftSkills()
  }

  get softSkills(): Observable<SoftSkill[]> {
    return this._softSkills.asObservable()
  }

  getSoftSkills(): SoftSkill[] {
    return this._softSkills.value
  }
    
  fetchSoftSkills(callback: Function = () => {}) {
    if(this._softSkills.getValue() === undefined){
      // Initialize observable
      this._softSkillsQuery.valueChanges.pipe(
        map((response: any) => response.data.getSoftskills)
      ).subscribe((softSkills: SoftSkill[]) => {
          // Update softSkills
          this._softSkills.next(softSkills)
        })
      // Trigger callback once
      this._softSkillsQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
    } else {
      // Trigger callback after refetch
      this._softSkillsQuery.refetch().then(response => callback(response))
    }
  }

  // ----------------------------------------------------------------- 
  // The code under this comment need to be reviewd and/or refactored
  // ----------------------------------------------------------------- 

  /**
   * Get All Skills from DB.
   */
   private _skillsWatchQuery: QueryRef<any, EmptyObject>;

   qGetAllSkills(): QueryRef<any, EmptyObject> {
     this._skillsWatchQuery = this.apollo.watchQuery({
         query: GET_ALL_SOFTSKILLS,
         //pollInterval: REFRESH
     });

     return this._skillsWatchQuery;
   }

  /**
   * Get one Skill (by ID) from DB.
   */
  private _skillWatchQuery: QueryRef<any, EmptyObject>;

  qGetSkill(idValue: any): QueryRef<any, EmptyObject> {
    this._skillWatchQuery = this.apollo.watchQuery({
        query: GET_SOFTSKILL,
        //pollInterval: REFRESH,
        variables: {
          id: idValue,
        }
    });
    return this._skillWatchQuery;
  }

}