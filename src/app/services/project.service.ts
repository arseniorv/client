import { Injectable } from "@angular/core";
import { Apollo, QueryRef } from "apollo-angular";
import { EmptyObject } from "apollo-angular/types";
import gql from "graphql-tag";
import { map, Observable } from "rxjs";


const GET_PROJECT = gql`
query getDetailProject($id: String!) {
  getProject(id: $id) {
    _id
    userId
    title
    description
    sector {
      _id
      name
    }
    competencies {
      _id
      name
    }
    city
    user {
      _id
      name
      avatarB64
    }
    softSkills {
      _id
      name
    }
    status
    createdDate
    files
    interestedUsers
      {
        _id
        name
      }
    state
    admins {
      _id
      email
    }
  } 
}
`;

const GET_UPLOAD_FILE_URL = gql`
  query getUploadUrl($id: String!,
                          $filename: String!) {
    uploadProjectUrl(id: $id, 
                  filename: $filename) {
      presignedUrl
    }
  }
`;


const GET_ALL_PROJECTS = gql`
query getAllProjects(
                  $userID: String,
                  $filter: ProjectFilter, 
                  $limit: Float, 
                  $skip: Float) {
  getAllProjects(
              userID: $userID, 
              filter: $filter, 
              limit: $limit, 
              skip: $skip) {
    _id
    userId
    title
    description
    sector {
        _id
        name
    }
    competencies {
        _id
        name
    }
    city
    user {
      _id
      name
      avatarB64
    }
    softSkills {
        _id
        name
    }
    status
    state
    createdDate
    files
    interestedUsers {
      _id
      name
    }
    admins {
      _id
      email
    }
  }
}
`;

const GET_INTEREST_PROJECTS = gql`
  query($userId: String!) {
    getInterestProjects (userId: $userId) {
      _id
      userId
      title
      city
      competencies {
        _id
        name
      }
      softSkills {
        _id
        name
      }
      description
      createdDate
      sector {
        name
      }
      status
      user {
        _id
        name
        avatarB64
      }
    }
  }
`

const GET_DOWNLOAD_FILE = gql`
  query getDownloadFile($id: String!,
                        $file: String!) {
    downloadFile(id: $id,
                 file: $file) {
      presignedUrl
    }
  }
`

const MUT_CREATE_PROJECT = gql`
    mutation createProjectMut(
        $userId: String!,
        $title: String!,
        $description: String!,
        $sector: [EmbeddedSector!]!,
        $competencies: [EmbeddedCompetency!]!,
        $city: String!,
        $softSkills: [EmbeddedSoftSkill!]!,
        $status: ProjectStatus,
        $createdDate: DateTime!,
        $files: [String!]!,
        $admins: [Members!]!
    ){
        createProject(
            createProjectDto: {
                userId: $userId,
                title: $title,
                description: $description,
                sector: $sector,
                competencies: $competencies,
                city: $city,
                user: $userId,
                softSkills: $softSkills,
                status: $status,
                createdDate: $createdDate,
                files: $files,
                admins: $admins
            }
        ){
            _id
            userId
            title
            description
            sector {
                _id
                name
            }
            competencies {
                _id
                name
            }
            city
            softSkills {
                _id
                name
            }
            status
            createdDate
            files
            admins {
              _id
              email
            }
        }
    }
`;

const MUT_EDIT_PROJECT = gql`
    mutation editProjectMut(
        $id: String!,
        $userId: String!,
        $title: String!,
        $description: String!,
        $sector: [EmbeddedSector!]!,
        $competencies: [EmbeddedCompetency!]!,
        $city: String!,
        $softSkills: [EmbeddedSoftSkill!]!,
        $status: ProjectStatus,
        $files: [String!]!,
        $admins: [Members!]!
    ){
        editProject(
          id: $id
          userId: $userId
          projectInputs: {
              title: $title,
              description: $description,
              sector: $sector,
              competencies: $competencies,
              city: $city,
              softSkills: $softSkills,
              status: $status,
              files: $files,
              admins: $admins
          }
        ){
            _id
            userId
            title
            description
            sector {
                _id
                name
            }
            competencies {
                _id
                name
            }
            city
            softSkills {
                _id
                name
            }
            status
            files
            admins {
              _id
              email
            }
        }
    }
`;

const MUT_ADD_INTERESTED_USER = gql`
  mutation addInterestedUser (
    $projectId: String!,
    $interestedUserId: String!
  ) {
    addInterestedUser(
      projectId: $projectId,
      interestedUserId: $interestedUserId
    ) {
      title
    }
  }
`;

const MUT_REMOVE_INTERESTED_USER = gql`
  mutation removeInterestedUser (
    $projectId: String!,
    $interestedUserId: String!
  ) {
    removeInterestedUser(
      projectId: $projectId,
      interestedUserId: $interestedUserId
    ) {
      title
    }
}
`;

const MUT_UPDATE_PROJECT_STATE = gql`
    mutation submitUpdateProject (
      $pId: String!,
      $state: OfferStatus!
    ) {
      updateProjectState(
        pId: $pId,
        state: $state
      ) {
        title
      }
    }
  `;

  const MUT_DELETEPROJECT = gql`
  mutation deleteProjectMut($id: String!) {
    deleteProject(id: $id) {
      _id
      title
    }
  }`;

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    constructor(private apollo: Apollo){}

    private _projectWatchQuery: QueryRef<any, EmptyObject>;

    qGetProject(userId: any): QueryRef<any, EmptyObject> {
      this._projectWatchQuery = this.apollo.watchQuery({
        query: GET_PROJECT,
        //pollInterval: REFRESH,
        variables: {
          id: userId,
        }
      });
      return this._projectWatchQuery;
    }

    qGetUploadProjectUrl(id: String, filename: String): Observable<any> {
        return this.apollo.query({
          query: GET_UPLOAD_FILE_URL,
          variables: {
            id,
            filename
          }
        })
      }
    
    private _projectsWatchQuery: QueryRef<any, EmptyObject>;

    qGetAllProjects(filter?: any, uId?: string,  limit?: number, skip?: number): QueryRef<any, EmptyObject> {
      this._projectsWatchQuery = this.apollo.watchQuery({
        query: GET_ALL_PROJECTS,
        variables: {
          userID: uId,
          filter,
          limit,
          skip
        }
      });
      return this._projectsWatchQuery;
    }

    private _interestedUsersWatchQuery: QueryRef<any, EmptyObject>;

    qGetInterestProjects(uId: any): QueryRef<any, EmptyObject> {
      if (!this._interestedUsersWatchQuery) {
        this._interestedUsersWatchQuery = this.apollo.watchQuery({
          query: GET_INTEREST_PROJECTS,
          variables: {
            userId: uId
          }
        });
      }
      return this._interestedUsersWatchQuery;
    }

    qGetDownloadFile(id?: String, file?: String): Observable<any> {
      return this.apollo.query({
        query: GET_DOWNLOAD_FILE,
        variables: {
          id, 
          file
        }
      })
    }

    //qGetInterestedUsers() {}

    mCreateProject(uId: any, iTitle: any, iDescription, iSector, iCompetence: any, 
        iCity: any, iValues: any, iStatus: string, iDate: string, iFiles: any, iAdmins: any){
        return this.apollo.mutate({
            mutation: MUT_CREATE_PROJECT,
            variables: {
                userId: uId,
                title: iTitle,
                description: iDescription,
                sector: iSector,
                competencies: iCompetence,
                city: iCity,
                softSkills: iValues,
                status: iStatus,
                createdDate: iDate,
                files: iFiles,
                admins: iAdmins
            }
        }).pipe(
            map((data) => {
              this._projectWatchQuery?.refetch();
                this._projectsWatchQuery?.refetch();
                return data
            })
        )
      }

      mEditProject(pId: any, uId:any, iTitle: any, iDescription, iSector, iCompetence: any, 
        iCity: any, iValues: any, iStatus: string, iFiles: any, iAdmins: any){
        return this.apollo.mutate({
            mutation: MUT_EDIT_PROJECT,
            variables: {
                id: pId,
                userId: uId,
                title: iTitle,
                description: iDescription,
                sector: iSector,
                competencies: iCompetence,
                city: iCity,
                softSkills: iValues,
                status: iStatus,
                files: iFiles,
                admins: iAdmins
            }
        }).pipe(
            map((data) => {
                this._projectWatchQuery?.refetch();
            })
        )
      }


      mAddInterestedUser(pId: string, uId: string) {
        return this.apollo.mutate({
          mutation: MUT_ADD_INTERESTED_USER,
          variables: {
            projectId: pId,
            interestedUserId: uId,
          }
        }).pipe(
          map((data) => {
            this._projectWatchQuery?.refetch();
            this._projectsWatchQuery?.refetch();
            this._interestedUsersWatchQuery?.refetch();
          })
        );
      }

      mRemoveInterestedUser(pId: string, uId: string) {
        return this.apollo.mutate({
          mutation: MUT_REMOVE_INTERESTED_USER,
          variables: {
            projectId: pId,
            interestedUserId: uId
          }
        }).pipe(
          map((data) => {
            this._projectWatchQuery?.refetch();
            this._projectsWatchQuery?.refetch();
            this._interestedUsersWatchQuery?.refetch();
          })
        );
      }

      mUpdateProjectState(pId: String, state: String) {
        return this.apollo.mutate({
          mutation: MUT_UPDATE_PROJECT_STATE,
          variables: {
            pId,
            state
          }
        }).pipe(
          map(() => {
            this._projectWatchQuery?.refetch();
            this._projectsWatchQuery?.refetch();
            this._interestedUsersWatchQuery?.refetch();
          })
        )
      }

      mDeleteProject(pId:any){
        return this.apollo.mutate({
          mutation: MUT_DELETEPROJECT,
          variables: {
            id: pId
          }
        }).pipe(
          map((data) => {
            this._projectsWatchQuery?.refetch();
          })
        )
      }
    
        

    
}