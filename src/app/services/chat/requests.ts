import { gql } from 'apollo-angular';

export const GET_CONVERSATIONS = gql`
  query getConversations {
    conversations {
      _id
      unreadsCount
      messageCount
      recipient {
        _id
        avatarB64
        name
      }
      lastMessage {
        text
        timeSent
      }
    }
  }
`;

export const MUT_CREATE_CHAT = gql`
  mutation createChatMut($loggedUserID: String!, $interlocutorID: String!) {
    createChatRoomIfNotExists(loggedUserID: $loggedUserID, interlocutorID: $interlocutorID)
  }
`