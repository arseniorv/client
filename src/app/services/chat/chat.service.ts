import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { Room } from 'src/app/models/room';
import { UserService } from '../user/user.service';
import { Apollo, QueryRef } from 'apollo-angular';
import { BehaviorSubject, Observable, finalize, take } from 'rxjs';
import * as requests from './requests'
import { Conversation } from 'src/app/models/conversation.model';

@Injectable({
  providedIn: 'root',
})
export class ChatService {

  private _me: User
  private _conversations: BehaviorSubject<Conversation[]>
  private _conversationsQuery: QueryRef<any>
  private _totalUnreads: BehaviorSubject<number>

  constructor(
    private apollo: Apollo,
    private userService: UserService,
    public router: Router,
  ) {
    this._me = {} as User
    this._conversations = new BehaviorSubject(undefined)
    this._totalUnreads = new BehaviorSubject(0)
    this.init()
  }

  init() {
    // Suscribe to me updates
    this.userService.me
    .pipe(finalize(() => {
      // Stop polling conversations when user logs out
      this.stopPollingConversations()
    }))
    .subscribe(me => this._me = me)

    // Set conversations query
    this._conversationsQuery = this.apollo.watchQuery({
      query: requests.GET_CONVERSATIONS
    });

    this.startPollingConversations()
  }

  getChatUrl(user: User, recipientId: string): string {
    return `/chat/${this.getRoom(user, recipientId)?.roomId}`
  }

  getRoom(user: User, recipientId: string): Room | undefined {
    return user.rooms.find(room => room.userId === recipientId)
  }

  roomExists(rooms: Room[], recipientId: string): boolean {
    return !!rooms.find(room => room.userId === recipientId)
  }

  openChat(recipientId: string): void {
    // If the room doesn't exist, create it and then open the chat 
    if (!this.roomExists(this._me.rooms, recipientId)) {
      this.mCreateChatRoomIfNotExists(this._me._id, recipientId).subscribe(async (data) => {
        // Trigger fetch, then open the chat
        this.userService.fetchMe(() =>
          this.router.navigate([this.getChatUrl(this._me, recipientId)])
        )
      })
      // Otherwise open the chat 
    } else {
      this.router.navigate([this.getChatUrl(this._me, recipientId)])
    }
  }

  get totalUnreads(): Observable<number> {
    return this._totalUnreads.asObservable()
  }
  
  get conversations(): Observable<Array<Conversation>> {
    return this._conversations.asObservable()
  }

  getConversations(): Array<Conversation> {
    return this._conversations.value
  }

  fetchConversations(callback: Function = () => {}) {
    if(this._conversations.getValue() === undefined){
      // Initialize observable
      this._conversationsQuery.valueChanges
        .subscribe((response: any) => {
          // Sort by last message time
          const sortedChats = [...response.data.conversations].sort((c1,c2) => c1.lastMessage.timeSent < c2.lastMessage.timeSent ? 1 : -1)
          // Update unreads
          let totalUnreads = 0
          sortedChats.forEach((c: Conversation) => totalUnreads += c.unreadsCount)
          this._totalUnreads.next(totalUnreads)
          // Update conversations
          this._conversations.next(sortedChats)
        })
      // Trigger callback once
      this._conversationsQuery.valueChanges.pipe(take(1)).subscribe(response => callback(response))
    } else {
      // Trigger callback after refetch
      this._conversationsQuery.refetch().then(response => callback(response))
    }
  }

  startPollingConversations(){
    this.fetchConversations()
    this._conversationsQuery.startPolling(5000) // Update every 5 seconds 
  }

  stopPollingConversations(){
    this._conversationsQuery.stopPolling() 
    this._conversations.complete()
  }

  mCreateChatRoomIfNotExists(loggedUserID: string, interlocutorID: string): Observable<any> {
    return this.apollo.mutate({
      mutation: requests.MUT_CREATE_CHAT,
      variables: {
        loggedUserID,
        interlocutorID
      }
    })
  }
}
