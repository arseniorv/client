import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { InfoCardComponent } from 'src/app/components/cards/info-card/info-card.component';
import { SharedModule } from 'src/app/components/shared.module';
import { User } from 'src/app/models/user';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { TextButtonComponent } from "../../components/buttons/text-button/text-button.component";
import { TitleComponent } from "../../components/title/title.component";
import { AuthService } from '../../services/auth.service';
import { CandidateInfoCardComponent } from 'src/app/components/cards/candidate-info-card/candidate-info-card.component';
import { AlertComponent } from 'src/app/components/modals/alert/alert.component';

@Component({
  standalone: true,
  selector: 'umu-candidate-profile',
  templateUrl: './candidate-profile.page.html',
  styleUrl:'./candidate-profile.page.scss',
  imports: [
    DefaultLayoutComponent,
    InfoCardComponent,
    SharedModule,
    TitleComponent,
    TitleComponent,
    TextButtonComponent,
    MultilanguageComponent,
    CandidateInfoCardComponent,
    AlertComponent
]
})
export class CandidateProfilePageComponent implements OnInit {
  me: User;
  isOpenDeleteAccountAlert: boolean = false

  userService = inject(UserService)
  authService = inject(AuthService)
  modal = inject(ModalController)
  menuController = inject(MenuController)
  router = inject(Router)

  ngOnInit() {
    this.userService.me.subscribe(me => this.me = me)
  }

  openDeleteAccountAlert() {
    this.isOpenDeleteAccountAlert = true
  }

  closeDeleteAccountAlert() {
    this.isOpenDeleteAccountAlert = false
  }

  goToDeleteAccount() {
    this.isOpenDeleteAccountAlert = false
    this.modal.dismiss() // Needed to avoid modal getting stuck due to navigation
    this.router.navigate(['/delete-account'])
  }

  logOut() {
    this.menuController.enable(false);
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
