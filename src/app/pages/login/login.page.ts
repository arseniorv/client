import { AfterViewInit, Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'umu-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPageComponent implements AfterViewInit {
  failedLogin = false;

  userService = inject(UserService)
  authService = inject(AuthService)
  formBuilder = inject(FormBuilder)

  form = this.formBuilder.group({
    email: ['', [
      Validators.required,
      Validators.email
    ]],
    password: ['', [Validators.required]],
  })
  
  @ViewChild('loginForm') loginFormEl: ElementRef | undefined;

  get controls() {
    return this.form.controls;
  }

  constructor(
    private menu: MenuController,
    private router: Router,
    public loadingCtrl: LoadingController
  ) {}

  ngAfterViewInit() {
    // Login on 'enter' key press in the password input
    if(this.loginFormEl.nativeElement){
      let passwordEl = this.loginFormEl.nativeElement.querySelector('#password')
      passwordEl.addEventListener('keyup',(e) => {
        e.keyCode == 13 ? this.doLogin() : null
      })
    }
  }

  doLogin() {
    if(this.form.valid){
      this.failedLogin = false
      const credentials = { email: this.form.value.email, password: this.form.value.password }
      this.authService.login(
        credentials,
        (user) => {
          if (!user) {
            this.failedLogin = true
          } else if (user.type === 'candidate') {
            this.router.navigate(['/offer-list']);
            this.menu.enable(true);
          } else if (user.type === 'company') {
            this.router.navigate(['/company-offer']);
            this.menu.enable(true);
          } 
        }
      )
    }
  }
}
