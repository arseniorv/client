/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { User } from 'src/app/models/user';

import { InfoCardComponent } from 'src/app/components/cards/info-card/info-card.component';
import { RangeComponent } from 'src/app/components/info/range/range.component';
import { SharedModule } from 'src/app/components/shared.module';
import { MiniTagComponent } from 'src/app/components/tags/mini-tag/mini-tag.component';
import { TitleComponent } from 'src/app/components/title/title.component';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { OffersService } from 'src/app/services/offers/offers.service';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { TextButtonComponent } from "../../components/buttons/text-button/text-button.component";
import { InfoTagsListComponent } from "../../components/info/info-tags-list/info-tags-list.component";
import { InfoTextComponent } from "../../components/info/info-text/info-text.component";
import { Candidate, Offer } from '../../models/offer';

interface Tags {
  iconName: string,
  label: string
}


@Component({
  standalone: true,
  selector: 'umu-candidate-offer-detail',
  templateUrl: './candidate-offer-detail.page.html',
  styleUrl: './candidate-offer-detail.page.scss',
  imports: [
    DefaultLayoutComponent,
    InfoCardComponent,
    SharedModule,
    InfoTagsListComponent,
    MiniTagComponent,
    TitleComponent,
    RangeComponent,
    InfoTextComponent,
    InfoTextComponent,
    TextButtonComponent
]
})
export class CandidateOfferDetailPageComponent implements OnInit {
  userLoggedID = sessionStorage.getItem('userId');
  me: User;
  offerID: any;
  // userID: any;
  // enrolledValue = '';
  userInfo: User;
  // userSkills: any[] = [];
  // skillsName: any;
  // skillsIco: any[] = [];
  // usersOffersList: any[] = [];
  // userCompets: any[] = [];
  // allOffers: any[] = [];
  // match: any;
  // files: any[];
  // candidature: Candidate;
  currentOffer: Offer;
  extTitle: string;
  peopleEnrolled: string ='';
  tagsList: Tags[] = [];
  isEnrolled: any = false;

  offersService = inject(OffersService);
  userService = inject(UserService)

  constructor(
    private aRoute: ActivatedRoute,
    private router: Router,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,
    private cOfferService: CompanyOffersService,
  ) {}

  ngOnInit() {
    this.userService.me.subscribe((me) => {
      this.me = me;
      this.offersService.currentOffer.subscribe(offer => {
        if(offer){
          this.currentOffer = offer;
          this.userInfo = offer.user;
          this.peopleEnrolled = offer.candidates.length.toString();
          if(this.tagsList.length === 0){
            this.setTags();
          }
          // If user is already enrolled in currentOffer, set variable isEnrolled to true;
          if(this.currentOffer.candidates.map((candidate) => {candidate.user.toString().includes(this.me._id) === true}).length !== 0){
            this.isEnrolled = true;
          }
        }
      })

    })
    this.offerID = this.aRoute.snapshot.params.id;
    this.offersService.getCurrentOffer(this.offerID);
    
    
    // this.qGetOffer(this.offerID);
  }

  setTags(){
    this.tagsList.push(
      {iconName: 'calendar', label: this.currentOffer.createdDate},
      {iconName: 'location', label: this.currentOffer.city},
      {iconName: 'work', label: this.currentOffer.remote},
      {iconName: 'salary', label: this.currentOffer.salaryRange},
      {iconName: 'clock', label: this.currentOffer.workHours},
      {iconName: 'contract', label: this.currentOffer.contractType}
    )
  }

  addEnrrolled(ofID) {
    this.offersService.addCandidate(ofID, this.me._id).subscribe(()=>{
      // Message Alert!
      this.enrolled();
      this.goToList();
      this.isEnrolled = true;
    })
  }

  goToList() {
    this.router.navigate(['/candidatures']);
  }


  async enrolled() {
    this.translateList.translate.stream('toast-offer-enrolled').subscribe(async (value) => {
      const toast = await this.toastCtrl.create({
        color: 'dark',
        message: value,
        duration: 3000,
        position: 'middle'
      });
      toast.present();
    });  
  }

  removeCandidate(ofID) {
    this.offersService.removeCandidate(ofID, this.me._id).subscribe(()=> {
      this.removed();
      this.isEnrolled = false;
    })
  }

  async removed() { 
     this.translateList.translate.stream('toast-candidate-removed').subscribe(async (value) => {
             const toast = await this.toastCtrl.create({
               color: 'dark',
               message: value,
               duration: 3000,
               position: 'bottom'
             });
             toast.present();
           });  
  }

  // TODO: se usa?
  // goToWebsite(){
  //   // console.log(`Current userInfo: ${JSON.stringify(this.offer)}`)
  //   const website = this.offer.user.website
    
  //   const url = website.substring(0,4) === 'http' // http / https
  //       ? this.offer.user.website
  //       : `https://`+ this.offer.user.website // default to https
  //   // console.log(`goToLink; ${url}`)
  //   window.open(url, "_blank");
  // }

  // async onDownloadFile(offerID: string, file: string) {
  //   this.cOfferService.qGetDownloadFile(offerID, file)
  //     .subscribe(response => {
  //       window.open(response.data.downloadFile.presignedUrl);
  //     });
  // }

}

