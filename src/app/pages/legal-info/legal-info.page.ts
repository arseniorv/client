import { Component } from "@angular/core";
import { LegalInfoComponent } from "src/app/partials/legal-info/legal-info.component";
import { OnlyToolbarLayoutComponent } from "src/app/templates/only-toolbar-layout/only-toolbar-layout";

@Component({
  standalone: true,
  selector: 'umu-legal-info-page',
  templateUrl: './legal-info.page.html',
  styleUrls: ['./legal-info.page.css'],
  imports: [OnlyToolbarLayoutComponent, LegalInfoComponent]
})
export class LegalInfoPageComponent {
  titleText = "legal-info"
}