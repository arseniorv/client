import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Offer } from 'src/app/models/offer';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { User } from 'src/app/models/user';

import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { OfferFiltersModalComponent } from 'src/app/components/modals/offer-filters-modal/offer-filters-modal.component';
import { SharedModule } from 'src/app/components/shared.module';
import { Filters } from 'src/app/models/filters.model';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { OffersService } from 'src/app/services/offers/offers.service';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { cityList, contractTypeOptions, remoteOptions, workHourOptions } from 'src/app/utils/constants';
import { SearchBarComponent } from "../../components/forms/search-bar/search-bar.component";
import { TitleComponent } from 'src/app/components/title/title.component';
import { InfiniteScrollComponent, Pagination } from 'src/app/components/infinite-scroll/infinite-scroll.component';
import { CardListComponent } from 'src/app/components/cards/card-list/card-list.component';

@Component({ 
  standalone: true,
  selector: 'umu-candidate-offers',
  templateUrl: './candidate-offers.page.html',
  styleUrls: ['./candidate-offers.page.css'],
  imports: [
    DefaultLayoutComponent,
    SharedModule,
    SearchBarComponent,
    TextButtonComponent,
    OfferFiltersModalComponent,
    CardListComponent,
    TitleComponent,
    InfiniteScrollComponent
]
})
export class CandidateOffersPageComponent implements OnInit {
  private userService = inject(UserService);
  private offersService = inject(OffersService); 

  me: User
  offers: Offer[] = [];
  filters: Filters;
  
  pageSize: number = 10;
  currentPage: number = 1;

  activeOffers: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  isModalOpen: boolean = false;

  readonly cityList = cityList;
  readonly workHourOptions = workHourOptions;
  readonly contractTypeOptions = contractTypeOptions;
  readonly remoteOptions = remoteOptions;

  readonly RequiredExperience = RequiredExperience;

  constructor(
    private router: Router,
    private translateList: MultilanguageComponent,
  ) {
    // Suscribe to observables
    this.userService.me.subscribe(me => this.me = me)
    this.offersService.offers.subscribe(offers => this.offers = offers)
    this.offersService.filters.subscribe(filters => {
      this.filters = filters  
    })
  }

  ngOnInit() {
    this.finalCityList = this.translateList.translateCityList();
  }

  onNewPageHanlder(event: Pagination){
    this.currentPage = event.currentPage
    this.offersService.fetchOffers(event.pageSize, event.skip, event.complete)
  }

  toCandidatures() {
    this.router.navigate(['/candidatures']);
  }

  // Go to offer detail
  goOfferDetail(offer) {
    const id = offer._id;
    this.router.navigate(['/offer-detail', id]);
  }

  openFiltersModal() {
    this.isModalOpen = true;
  }

  onCloseEvent(){
    this.currentPage = 1;
    this.isModalOpen = false;
  }

}

