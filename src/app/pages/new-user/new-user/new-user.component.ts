import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { SharedModule } from 'src/app/components/shared.module';
import { TitleComponent } from 'src/app/components/title/title.component';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';

@Component({
  standalone: true,
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
  imports: [
    TitleComponent,
    DefaultLayoutComponent,
    TextButtonComponent,
    SharedModule
  ]
})
export class NewUserComponent implements OnInit {
  me: User;
  userData: any = '';
  error: any = '';
  initForm: UntypedFormGroup;
  isSubmitted = false;
  emailRegex = '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/';
  user = { email: '', pw: '' };

  constructor(private router: Router,
              private userService: UserService,
              public fBuilder: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.userService.me.subscribe(me => {
      this.me = me;
      console.log('me', me);

      // Create string of sift skills
      const arrSoftskill = me.softSkills.map(s => s.name)
      const stringSoftskill = JSON.stringify(arrSoftskill)
      sessionStorage.setItem('userId', me._id);
      sessionStorage.setItem('userName', me.name);
      sessionStorage.setItem('userEmail', me.email);
      sessionStorage.setItem('userType', me.type);
      sessionStorage.setItem('userSoftSkills', stringSoftskill);
    }) 
  }

   async goToProfile() {
    
      if (window.sessionStorage) {
        sessionStorage.removeItem('userSoftSkills');
      }

      if (this.me.type === 'candidate') {
        this.router.navigate(['/user-profile']);
      } else if (this.me.type === 'company') {
        this.router.navigate(['/company-profile']);
      }
  }

  goToCreateOffer(){

      if (window.sessionStorage) {
        sessionStorage.removeItem('userSoftSkills');
      }
    
      this.router.navigate(['/company-offer']);
  
  }


  goToOfferList(){
   
      if (window.sessionStorage) {
        sessionStorage.removeItem('userSoftSkills');
      }
      
      this.router.navigate(['/offer-list']);

  }

  goToProjectList(){
   
    if (window.sessionStorage) {
      sessionStorage.removeItem('userSoftSkills');
    }
    
    this.router.navigate(['/all-projects-list']);

}

}
