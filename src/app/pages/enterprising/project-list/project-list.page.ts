
import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { OfferCardComponent } from 'src/app/components/cards/offer-card/offer-card.component';
import { SharedModule } from 'src/app/components/shared.module';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { TitleComponent } from "../../../components/title/title.component";
import { CreateProjectComponent } from '../create-project/create-project.component';
import { EditProjectComponent } from '../edit-project/edit-project.component';
import { TextButtonComponent } from "../../../components/buttons/text-button/text-button.component";
import { CardListComponent } from 'src/app/components/cards/card-list/card-list.component';
import { NewProjectModalComponent } from "../../../components/modals/new-project-modal/new-project-modal.component";



@Component({
  standalone: true,
  selector: 'app-project-list',
  templateUrl: './project-list.page.html',
  styleUrls: ['./project-list.page.scss'],
  imports: [
    OfferCardComponent,
    SharedModule,
    DefaultLayoutComponent,
    TitleComponent,
    TextButtonComponent,
    CardListComponent,
    NewProjectModalComponent
]
})
export class ProjectListComponent implements OnInit{

  isModalOpen: boolean = false;

  private userService = inject(UserService);
  private pService = inject(ProjectService);
  private projectsService = inject(ProjectsService);
  
  userInfo: any
  userType: any
  avatar: any
  userID: string;
  allProjects: any[] = [];
  adminProjects: any[] = [];
  sharedProjects: any[] = [];
  projects: Project[] = [];
  me: User;
  pageSize = 10;
  currentPage: number = 1;



  constructor(
              public mController: ModalController,
              private router: Router){
    
    this.projectsService.fetchProjects();
    this.projectsService.projects.subscribe((projects) => {
      this.projects = projects;
      this.userService.me.subscribe((me) => {
        this.me = me
        this.getAllProjects();
      })
    })
  }

  ngOnInit(): void {
    
    // this.getLoggedUser();
    // this.userID = sessionStorage.getItem('userId');
    
  }

  // getLoggedUser() {
  //   this.uService.qGetMe().valueChanges.pipe(
  //     map(result => result.data)
  //   ).subscribe((item) => {
  //     this.userInfo = item.me;
  //     this.userType = item.me.type
  //     this.avatar = item.me.avatarB64;
  //     // this.candidatures = item.me.candidatures;
  //   });    
  // }

  getAllProjects(filter?: string){
    for (let project of this.projects){
      this.sharedProjects = [project];
      if(filter === 'all' || !filter){
        this.adminProjects = this.projects
        .filter(project => (JSON.stringify(project.admins).includes(this.me._id)));
      }
      if(filter === 'owned'){
        this.adminProjects = this.projects
        .filter(project => (project.user._id === this.me._id));
      }
      if(filter === 'shared'){
        this.adminProjects = this.projects
        .filter(project => (JSON.stringify(project.admins).includes(this.userID) && project.user._id !== this.me._id));
        this.sharedProjects = this.adminProjects;
      }
      if(filter === 'inactive'){
        this.adminProjects = this.projects
        .filter(project => (JSON.stringify(project.admins).includes(this.me._id) && project.state === 'INACTIVE'));
      }
    }
      

  }

  async createModal() {
    const createModal = await this.mController.create({
      component: CreateProjectComponent,
      animated: true,
      cssClass: 'modalCss'
    });
    await createModal.present();
  }

  goProjectDetail(project) {
    const id = project._id;
    this.router.navigate(['/project-detail', id]);
  }

  async editModal(projectId, event) {
    event.stopPropagation();
    const project = this.adminProjects.find((p) => p._id === projectId)

    if (project) {
      const editModal = await this.mController.create({
        component: EditProjectComponent,
        componentProps: {
          projectData: project
        },
        animated: true,
        cssClass: 'modalCss'
      });
      
      await editModal.present();
    }
  }

  openNewProjectModal() {
    this.isModalOpen = true;
  }

  onCloseEvent(){
    this.currentPage = 1;
    this.isModalOpen = false;
  }

}
