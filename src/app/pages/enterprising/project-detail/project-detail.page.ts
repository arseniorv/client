import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { EditProjectComponent } from '../edit-project/edit-project.component';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { MatchService } from 'src/app/services/match.service';
import { User } from 'src/app/models/user';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { InfoCardComponent } from 'src/app/components/cards/info-card/info-card.component';
import { TitleComponent } from 'src/app/components/title/title.component';
import { MiniTagComponent } from 'src/app/components/tags/mini-tag/mini-tag.component';
import { InfoTagsListComponent } from 'src/app/components/info/info-tags-list/info-tags-list.component';
import { RangeComponent } from 'src/app/components/info/range/range.component';
import { InfoTextComponent } from 'src/app/components/info/info-text/info-text.component';
import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { SharedModule } from 'src/app/components/shared.module';
import { InfoOptionsListComponent } from 'src/app/components/info/info-options-list/info-options-list.component';
import { ProfileCardComponent } from "../../../components/cards/profile-card/profile-card.component";
import { EditProjectModalComponent } from 'src/app/components/modals/edit-project-modal/edit-project-modal.component';

interface Tags {
  iconName: string,
  label: string
}

@Component({
  standalone: true,
  selector: 'app-project-detail',
  templateUrl: './project-detail.page.html',
  styleUrls: ['./project-detail.page.scss'],
  imports: [
    DefaultLayoutComponent,
    InfoCardComponent,
    TitleComponent,
    MiniTagComponent,
    InfoTagsListComponent,
    RangeComponent,
    InfoTextComponent,
    TextButtonComponent,
    SharedModule,
    InfoOptionsListComponent,
    ProfileCardComponent,
    EditProjectModalComponent
]
})
export class ProjectDetailComponent implements OnInit {

  isModalOpen: boolean = false;

  projectID: any;
  project: Project;
  userID: any;
  isEnrolled: number = -1;
  enrolledUsers: any[] = [];
  enrolledUsersInfo: any[] = [];
  adminsInfo: any [] = [];
  isAdmin: boolean;
  allMatches: any[] = [];
  userInfo: User;
  match: number;

  me: User;
  currentProject: Project;
  peopleEnrolled: string;
  tagsList: Tags[] = [];

  userService = inject(UserService);
  projectsService = inject(ProjectsService);
  matchService = inject(MatchService);

  constructor(
    private aRoute: ActivatedRoute,
    public pService: ProjectService,
    public mController: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,
    private uService: UserService,
    private router: Router,
    private mService: MatchService
  ) {
   }

  ngOnInit(): void {
    this.userService.me.subscribe((me) => {
      this.me = me;
      this.projectsService.currentProject.subscribe((project)=>{
        if(project){
          console.log('proyecto: ', project)
          this.currentProject = project;
          this.match = Math.round(Number(project.match));
          this.userInfo = project.user;
          this.peopleEnrolled = project.interestedUsers.length.toString();
          if(this.tagsList.length === 0){
            this.setTags();
          }
          if (project.admins.findIndex((admin) => admin._id === me._id) !== -1){
            this.isAdmin = true;
          }
        }
      })
    })

    this.projectID = this.aRoute.snapshot.params.id;
    this.projectsService.fetchCurrentProject(this.projectID);
  }

  setTags(){
    this.tagsList.push(
      {iconName: 'calendar', label: this.currentProject.createdDate},
      {iconName: 'location', label: this.currentProject.city},
      {iconName: 'rocket', label: this.currentProject.status},
      {iconName: 'sectors', label: 'sectores'},
    )
  }


  onDownloadFile(pId: string, file: string){
    this.pService.qGetDownloadFile(pId, file)
      .subscribe(response => {
        window.open(response.data.downloadFile.presignedUrl);
      });
  }

  async openEditModal() {
    this.isModalOpen = true;
  }

  addEnrolled(pID) {
    this.projectsService.addInterestedUser(pID, this.me._id).subscribe(() => {
      this.enrolled();
    });   
  }

  async enrolled() {
   // this.loadingCtrl.dismiss();
    this.translateList.translate.stream('toast-project-added').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          });  
  }
  removeCandidate(pID) {
    console.log('borrando usuaria')
    this.projectsService.removeInterestedUser(pID, this.me._id).subscribe(() => {
      this.removed();  
    });  
  }
  async removed() {
    // this.loadingCtrl.dismiss();
     this.translateList.translate.stream('toast-project-removed').subscribe(async (value) => {
             const toast = await this.toastCtrl.create({
               color: 'dark',
               message: value,
               duration: 3000,
               position: 'bottom'
             });
             toast.present();
           });  
   }

  goCandidateProfile(userid){
    this.router.navigate(["interested-entity"], {state: {userid: userid }})
  }

  createChat(adminID){
    this.uService.mCreateChatRoomIfNotExists(this.userID, adminID).subscribe(() => {
      this.qGetLoggedUser(this.userID, adminID);
    }); 
  }

  qGetLoggedUser(userID, adminID){
    this.uService.qGetUser(userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
        this.userInfo = item.getUser;
        this.getChatUrl(adminID);
    })
  }

  getChatUrl(adminID) {
    this.router.navigate([`/chat/${this.userInfo?.rooms?.find(room => room.userId === adminID)?.roomId}`])
  }

  onCloseEvent(){
    this.isModalOpen = false;
  }

}
