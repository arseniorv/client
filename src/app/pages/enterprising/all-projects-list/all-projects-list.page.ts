import { Component, inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { CardListComponent } from 'src/app/components/cards/card-list/card-list.component';
import { SearchBarComponent } from 'src/app/components/forms/search-bar/search-bar.component';
import { InfiniteScrollComponent, Pagination } from 'src/app/components/infinite-scroll/infinite-scroll.component';
import { ProjectFiltersModalComponent } from 'src/app/components/modals/project-filters-modal/project-filters-modal.component';
import { Filters } from 'src/app/models/filters.model';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { MatchService } from 'src/app/services/match.service';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { cityList, statusOptions } from 'src/app/utils/constants';
import { TitleComponent } from "../../../components/title/title.component";

@Component({
  standalone: true,
  selector: 'umu-all-projects-list',
  templateUrl: './all-projects-list.page.html',
  styleUrls: ['./all-projects-list.page.scss'],
  imports: [
    DefaultLayoutComponent,
    IonicModule,
    TranslateModule,
    TextButtonComponent,
    SearchBarComponent,
    TitleComponent,
    ProjectFiltersModalComponent,
    CardListComponent,
    InfiniteScrollComponent
]
})
export class AllProjectsListComponent implements OnInit {

  private userService = inject(UserService)
  private projectsService = inject(ProjectsService)
  private matchService = inject(MatchService)

  userInfo: any
  userType: any
  avatar: any
  allProjects: any[] = []
  interestProjects: any[];
  userID: any;
  allMatches: any[] = [];
  sharedProjects: boolean;
  ownedProjects: boolean;


  me: User;
  projects: Project[];
  pageSize = 10;
  currentPage: number = 1;
  matches: any
  isModalOpen: boolean = false;




  public filters: UntypedFormGroup;
  readonly cityList = cityList;
  readonly statusOptions = statusOptions;
  sectorList : any[];
  finalCityList: any[] = [];
  finalSectorList: any[] = [];
  filter: Filters;

  constructor(
    public router: Router,
    public fBuilder: UntypedFormBuilder,
    private translateList: MultilanguageComponent,
  ) {
    this.userService.me.subscribe(me => {
      this.me = me;
      this.projectsService.projects.subscribe(projects => {
        // Include only projects where user is NOT admin
        this.projects = projects.filter((p) => !p.admins.map(a => a._id).toString().includes(me._id))
      })
    })
    
    this.projectsService.filters.subscribe(filters => {
      this.filter = filters  
      this.currentPage = 0;
      this.projectsService.fetchProjects(this.pageSize, this.pageSize*this.currentPage, ()=>{}, true);
    })
    
  }
  

  ngOnInit(){
    this.projectsService.fetchProjects(this.pageSize, this.pageSize*this.currentPage);
    this.matchService.fetchMyMatches();
  }

  async openFiltersModal() {
    this.isModalOpen = true;
}

  onCloseEvent(){
    this.currentPage = 0;
    this.isModalOpen = false;
  }

  onNewPageHanlder(event: Pagination){
      this.currentPage = event.currentPage
      this.projectsService.fetchProjects(event.pageSize, event.skip, event.complete)
    }

  toProjectList() {
    this.router.navigate(['/project-list']);
  }

  toInterestProjects() {
    this.router.navigate(['/interest-projects']);
  }

  // goProjectDetail(project) {
  //   const id = project._id;
  //   this.router.navigate(['/project-detail', id]);
  // }

  // formatFilters() {
  //   const filters: any = {}

  //   if (this.filters.value.search)
  //     filters.search = this.filters.value.search
  //   if (this.filters.value.city)
  //     filters.city = this.filters.value.city
  //   if (this.filters.value.createdDate) {
  //     filters.createdDate = new Date();
  //     filters.createdDate.setHours(0,0,0,0)
  //     switch(this.filters.value.createdDate) {
  //       case 'last-24':
  //         filters.createdDate.setDate(filters.createdDate.getDate() - 1);
  //         break;
  //       case 'last-7':
  //         filters.createdDate.setDate(filters.createdDate.getDate() - 7);
  //         break;
  //       case 'last-15':
  //         filters.createdDate.setDate(filters.createdDate.getDate() - 15);
  //         break;
  //     }
  //   }
  //   if (this.filters.value.sector)
  //     filters.sector = this.filters.value.sector
  //   if (this.filters.value.status)
  //     filters.status = this.filters.value.status


  //   if(Object.keys(filters).length==0)
  //     return undefined;

  //   return filters 
  // }

  // onFilter(){
  //   this.currPage = 0;
  //   // this.infiniteScroll.disabled = false;
  //   this.getAllProjects(this.formatFilters());
  // }

  // clearFilters() {
  //   this.currPage = 0;
  //   // this.infiniteScroll.disabled = false;
  //   this.filters.reset();
  //   this.getAllProjects();
  // }

  // redirectToProjectList() {
  //   this.router.navigate(['/project-list']);
  // }

}
