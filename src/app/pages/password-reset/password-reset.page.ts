/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { matchValidator } from 'src/app/shared/directives/match-validator.directive';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';

const passwordRegex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$';  // Same as register page

@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.page.html',
    styleUrls: ['./password-reset.page.scss']
})
export class PasswordResetPageComponent implements OnInit {

    tokenForm: UntypedFormGroup;
    passwordForm: UntypedFormGroup;
    submited: Boolean = false;
    error: Boolean = false;
    token: String;
    resetError: String;
    emailRegex = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,10}$';  // Same as register page


    constructor(private menu: MenuController,
        private aRoute: ActivatedRoute,
        public fBuilder: UntypedFormBuilder,
        private router: Router,
        private userService: UserService,
        private translateList: MultilanguageComponent,
        public loadingCtrl: LoadingController) { }


    ngOnInit(): void {
        this.token = this.aRoute.snapshot.params.token;
        this.validation()
    }

    validation() {
        this.tokenForm = this.fBuilder.group({
            userEmail: ['', [Validators.required, Validators.pattern(this.emailRegex)]]
        });

        this.passwordForm = this.fBuilder.group({
            password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(16), Validators.pattern(passwordRegex)])],
            confirmPassword: ['', Validators.required]
        },
            {
                validators: [matchValidator('password', 'confirmPassword')]
            });
    }

    submitTokenForm() {
        if (this.tokenForm.valid) {
            this.userService.mGeneratePasswordResetToken(this.tokenForm.value.userEmail).subscribe(
                {
                    next: () => {
                        this.submited = true;
                        this.error = false;
                    },
                    error: async (error) => {
                        this.error = true
                        this.translateList.translate.stream(error.message).subscribe(async (value) => {
                            this.resetError = value
                        });

                    }
                }
            )
        }
    }

    submitPasswordForm() {
        if (this.passwordForm.valid) {
            this.userService.mResetPassword(this.token, this.passwordForm.value.password).subscribe(
                {
                    next: () => {
                        this.submited = true;
                        this.error = false;
                        this.router.navigate(['/login'])
                    },
                    error: async (error) => {
                        this.error = true;
                        this.translateList.translate.stream(error.message).subscribe(async (value) => {
                            this.resetError = value
                        });
                    }
                }
            )
        }

    }

    showPass(){
        const password = document.querySelector("#password");
        const type = password.getAttribute("type") === "password" ? "text" : "password";
        password.setAttribute("type", type);
        
      }

    showConfirmedPass(){
        const password = document.querySelector("#password-repeat");
        const type = password.getAttribute("type") === "password" ? "text" : "password";
        password.setAttribute("type", type);
      }

}
