import { Component, inject } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { Offer } from 'src/app/models/offer';
import { TranslateModule } from '@ngx-translate/core';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { OffersService } from 'src/app/services/offers/offers.service';
import { TextButtonComponent } from "../../components/buttons/text-button/text-button.component";
import { NewOfferModalComponent } from 'src/app/components/modals/new-offer-modal/new-offer-modal.component';
import { InfiniteScrollComponent, Pagination } from 'src/app/components/infinite-scroll/infinite-scroll.component';
import { CardListComponent } from 'src/app/components/cards/card-list/card-list.component';

@Component({
  standalone: true,
  selector: 'umu-company-offers',
  templateUrl: './company-offers.page.html',
  styleUrl: './company-offers.page.scss',
  imports: [
    TranslateModule, 
    CardListComponent,
    DefaultLayoutComponent, 
    IonicModule, 
    TextButtonComponent,
    NewOfferModalComponent,
    InfiniteScrollComponent
  ]
})
export class CompanyOffersPageComponent {
  private offersService = inject(OffersService);

  myOffers: Offer[] = [];

  pageSize: number = 10;
  currentPage: number = 1;

  isModalOpen: boolean = false;

  constructor() {
    this.offersService.offers.subscribe((offers) => this.myOffers = offers)
  }

  openNewOfferModal() {
    this.isModalOpen = true;
  }

  onCloseEvent(){
    this.currentPage = 1;
    this.isModalOpen = false;
  }

  onNewPageHanlder(event: Pagination){
    this.currentPage = event.currentPage
    this.offersService.fetchOffers(event.pageSize, event.skip, event.complete)
  }

}

