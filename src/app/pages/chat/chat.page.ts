import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user/user.service";
import { environment } from "src/environments/environment";


@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss']
})
export class ChatPageComponent implements OnInit, AfterViewInit {

    @ViewChild('rocketchat', { static: false, read: ElementRef })
    private _rocketChat: ElementRef<HTMLIFrameElement>
    
    private _chatId: string;
    me: User

    constructor(
        private aRoute: ActivatedRoute,
        private userService: UserService
    ) {
        // Suscribe to observables
        this.userService.me.subscribe(me => {
            this.me = me
        })
    }

    ngAfterViewInit(): void {
        this._rocketChat.nativeElement.src = `${environment.ROCKETCHAT_URL}?layout=embedded`
        this.setChatServiceIframe()
    }

    ngOnInit(): void {
        this._chatId = this.aRoute.snapshot.params.chatId;
    }

    setChatServiceIframe() {
        const authToken = this.me.chatAuthToken
        const rocket = this._rocketChat
        const chatId = this._chatId
        window.addEventListener('message', function (e) {
            if (e.data.eventName === 'startup' && rocket.nativeElement.contentWindow) {
                rocket.nativeElement.contentWindow.postMessage({
                    externalCommand: 'login-with-token',
                    token: authToken
                }, '*');
                rocket.nativeElement.contentWindow.postMessage({
                    externalCommand: 'go',
                    path: `/direct/${chatId}`
                }, '*');

                rocket.nativeElement.style.height = rocket.nativeElement.parentElement.clientHeight + 'px';
            }
        });
    }

}
