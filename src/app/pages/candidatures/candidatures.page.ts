import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { CardListComponent } from 'src/app/components/cards/card-list/card-list.component';
import { SearchBarComponent } from 'src/app/components/forms/search-bar/search-bar.component';
import { InfiniteScrollComponent, Pagination } from 'src/app/components/infinite-scroll/infinite-scroll.component';
import { OfferFiltersModalComponent } from 'src/app/components/modals/offer-filters-modal/offer-filters-modal.component';
import { SharedModule } from 'src/app/components/shared.module';
import { TitleComponent } from 'src/app/components/title/title.component';
import { Filters } from 'src/app/models/filters.model';
import { Offer } from 'src/app/models/offer';
import { User } from 'src/app/models/user';
import { OffersService } from 'src/app/services/offers/offers.service';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';

@Component({
  standalone: true,
  selector: 'umu-candidatures',
  templateUrl: './candidatures.page.html',
  styleUrls: ['./candidatures.page.scss'],
  imports: [
    DefaultLayoutComponent,
    SharedModule,
    OfferFiltersModalComponent,
    CardListComponent,
    TitleComponent,
    InfiniteScrollComponent
]
})
export class CandidaturesPageComponent implements OnInit {

  private userService = inject(UserService);
  private offersService = inject(OffersService); 
  private router = inject(Router); 

  me: User
  myCandidatures: Offer[] = []
  isModalOpen: boolean = false
  filters: Filters
  
  pageSize: number = 10
  currentPage: number = 1

  ngOnInit() {
    this.userService.me.subscribe(me => this.me = me)
    this.offersService.myCandidatures.subscribe(myCandidatures => this.myCandidatures = myCandidatures)
  }

  // Get offers detail
  goOfferDetail(offer) {
    const id = offer._id;
    this.router.navigate(['/offer-detail', id]);
  }

  onNewPageHanlder(event: Pagination){
    this.currentPage = event.currentPage
    this.offersService.fetchOffers(event.pageSize, event.skip, event.complete)
  }

  openFiltersModal() {
    this.isModalOpen = true;
  }

  onCloseEvent(){
    this.currentPage = 1;
    this.isModalOpen = false;
  }

}
