/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable no-underscore-dangle */
import { Component, OnInit } from '@angular/core';
import { Conversation } from 'src/app/models/conversation.model';
import { ChatService } from 'src/app/services/chat/chat.service';
import { UserService } from 'src/app/services/user/user.service';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';



@Component({ 
  selector: 'umu-conversations',
  templateUrl: './conversations.page.html',
  styleUrls: ['./conversations.page.scss'],
})
export class ConversationsPageComponent implements OnInit{
  
  conversations: Array<Conversation> = [];
  userType: string = '';
  
  constructor(
    private userService: UserService,
    private chatService: ChatService,
    private translateList: MultilanguageComponent){}
  
  ngOnInit(): void {
    this.userService.me.subscribe(me => {this.userType = me.type})

    this.chatService.conversations.subscribe(conversations => this.conversations = conversations)
    // Update conversations on page load
    this.chatService.fetchConversations()
  }
}
