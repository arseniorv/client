import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InfoCardComponent } from 'src/app/components/cards/info-card/info-card.component';
import { SharedModule } from 'src/app/components/shared.module';
import { TitleComponent } from 'src/app/components/title/title.component';
import { User } from 'src/app/models/user';
import { MultilanguageComponent } from 'src/app/partials/multilanguage/multilanguage.component';
import { UserService } from 'src/app/services/user/user.service';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { AuthService } from '../../services/auth.service';
import { TextButtonComponent } from 'src/app/components/buttons/text-button/text-button.component';
import { CompanyInfoCardComponent } from 'src/app/components/cards/company-info-card/company-info-card.component';

@Component({
  standalone: true,
  selector: 'umu-company-profile',
  templateUrl: './company-profile.page.html',
  styleUrls: ['./company-profile.page.scss'],
  imports: [
    DefaultLayoutComponent,
    InfoCardComponent,
    SharedModule,
    CompanyInfoCardComponent,
    MultilanguageComponent,
    TitleComponent,
    TextButtonComponent
  ]
})
export class CompanyProfilePageComponent implements OnInit {
  userSkills: any[] = [];
  sectorName = '';
  skillsName: any;
  skillsIco: any[] = [];
  userType: string;
  avatar: string;
  me: User;

  private userService = inject(UserService)
  private auth = inject(AuthService)
  private router = inject(Router) 

  ngOnInit() {
    this.userService.me.subscribe(me => {
      this.me = me;
    })
    
    if(!this.router.getCurrentNavigation().extras.state){   
    }
    else {
      this.router.navigate(['/mp1'], {state: {button: '1'}});
    }
  }

  logOut() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
  
  goToWebsite(){
    const website = this.me.website
    
    const url = website.substring(0,4) === 'http' // http / https
        ? this.me.website
        : `https://`+ this.me.website // default to https
    window.open(url, "_blank");
  }

}
