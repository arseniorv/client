import { Component, CUSTOM_ELEMENTS_SCHEMA, inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { CheckboxCustomEvent, IonModal, ModalController } from '@ionic/angular';
import { CandidatureStatus } from 'src/app/models/candidature-status.model';
import { User } from 'src/app/models/user';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { UserService } from 'src/app/services/user/user.service';
import { Offer } from '../../models/offer';
import { EditOfferPageComponent } from '../edit-offer/edit-offer.page';
import { OfferStatus } from 'src/app/models/offer-status.model';
import { DefaultLayoutComponent } from 'src/app/templates/default-layout/default-layout';
import { CandidateCardComponent } from 'src/app/components/cards/candidate-card/candidate-card.component';
import { ModalComponent } from "../../components/modals/modal/modal.component";
import { InfoTagsListComponent } from "../../components/info/info-tags-list/info-tags-list.component";
import { OffersService } from 'src/app/services/offers/offers.service';
import { SharedModule } from 'src/app/components/shared.module';

const GET_USOFRS = gql`
  query {
    getUsersOffers {
      _id
      offer_id
      user_id
    }
  }
`;

interface Tags {
  iconName: string,
  label: string
}

@Component({
  standalone: true,
  selector: 'app-company-offer-detail',
  templateUrl: './company-offer-detail.page.html',
  styleUrls: ['./company-offer-detail.page.scss'],
  imports: [DefaultLayoutComponent, CandidateCardComponent, InfoTagsListComponent, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CompanyOfferDetailPageComponent implements OnInit, OnDestroy {
  // private compOfService = inject(CompanyOffersService);
  offersService = inject(OffersService);
  compOfService = inject(CompanyOffersService);
  userService = inject(UserService);

  offerID: any;
  offer: Offer;
  userOfferList: any[] = [];
  usersList: any[] = [];
  usersListData: any[] = [];
  uDataList: any[] = [];
  enrolledUser: any[] = [];
  uSkills: any[] = [];
  oSkill: any[] = [];
  userLoggedSkills: any[] = [];
  sortUsersList: any[] = [];
  avatar: string;
  showOption: any = 'false';
  openProcess: any = 'false';
  isCompanyOfferDetail:boolean = true;
  candidateHired: boolean;
  tagsList: Tags[] = [];
  error: any;
  loading = true;
  me: User;

  readonly candidatureStatus = CandidatureStatus;
  readonly offerStatus = OfferStatus;
  currentOffer: Offer;

  constructor(
    private aRoute: ActivatedRoute,
    private apollo: Apollo,
    private mController: ModalController,
    public router: Router,
    public modal: IonModal) { }

  ngOnInit() {
    this.offerID = this.aRoute.snapshot.params.id;
    this.userService.me.subscribe((me) => {
      this.me = me;
      this.avatar = me.avatarB64;
      //¿se está usando?
      this.userLoggedSkills = me.softSkills;
      this.offersService.currentOffer.subscribe(offer => {
        if(offer){
          this.offer = offer;
          this.usersListData = this.offer.candidates.map(offer => ({ ...offer, match: { ...offer.match, total: Math.round(offer.match.total) } }));
          this.usersListData.sort((a) => (a.status === "REJECTED" ) ? 1 : -1)  
          if(this.tagsList.length === 0){
            this.setTags();
          }
          // If user is already enrolled in currentOffer, set variable isEnrolled to true;
          if(this.offer.candidates.map((candidate) => {candidate.user.toString().includes(this.me._id) === true}).length !== 0){
          }
        }
      })
    })    
    this.offersService.getCurrentOffer(this.offerID);
  }
 
  setTags(){
    console.log('offer dentro de set tags', this.offer);
    this.tagsList.push(
      {iconName: 'calendar', label: this.offer.createdDate},
      {iconName: 'location', label: this.offer.city},
      {iconName: 'work', label: this.offer.remote},
      {iconName: 'salary', label: this.offer.salaryRange},
      {iconName: 'clock', label: this.offer.workHours},
      {iconName: 'contract', label: this.offer.contractType}
    )
  }

// TODO: Refactor the mutation
  updateCandidateStatus(candidateId: string, candidatureStatus: CandidatureStatus) {
    console.log('update candidateStatus', candidatureStatus);
   this.offersService.updateCandidateStatus(this.offerID, candidateId, candidatureStatus).subscribe();
  }

  onToggle(event: Event, candidateId:string) {
    const ev = event as CheckboxCustomEvent;
    this.candidateHired = ev.detail.checked;
    this.updateHiring(candidateId);
  }


  updateHiring(candidateId: string){
    for (let candidate of this.offer.candidates){
      if (candidateId === candidate.user._id){
        if (candidate.status === "PRESELECTED"){
          this.updateCandidateStatus(candidateId, CandidatureStatus.HIRED);
        }
        else if (candidate.status === "HIRED"){
          this.updateCandidateStatus(candidateId, CandidatureStatus.PRESELECTED);
        }
      }
    }
  }


  /**
   * Call modal to edit offer values
   */
  async editModal() {

    const editModal = await this.mController.create({
      component: EditOfferPageComponent,
      componentProps: {
        offerData: this.offer
      },
      animated: true,
      cssClass: 'modalCss'
    });

    await editModal.present();

  }

  showOptions(x){
      this.showOption = x;
  }

  disableOffer(offerId: String) {
      this.updateOfferStatus(offerId, OfferStatus.INACTIVE);
  }

  enableOffer(offerId: String) {
    this.updateOfferStatus(offerId, OfferStatus.ACTIVE);
  }

  updateOfferStatus(OfferId: String, offerStatus: OfferStatus) {
    this.compOfService.mUpdateOfferStatus(this.offerID, offerStatus).subscribe();
   }

  deleteOffer(ofId){
    this.compOfService.mDeleteOffer(ofId)
    .subscribe(() => {
      console.log('Offer deleted!');
    });
    this.mController.dismiss()
    this.router.navigate(["company-offer"])
  }

  cancel() {
    this.mController.dismiss()
  }

  confirm() {
    this.modal.dismiss();
  }

  goCandidateProfile(userid){
    this.router.navigate(["interested-entity"], {state: {userid: userid }})
  }

  ngOnDestroy() {
    /* this.querySubscription.unsubscribe();*/
  }
}
