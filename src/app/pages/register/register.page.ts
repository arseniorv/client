import { Component, inject, OnInit } from '@angular/core';
import { LoadingController, MenuController, ToastController } from '@ionic/angular';

import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { InputUser } from 'src/app/models/user';
import { AuthService } from '../../services/auth.service';
import { MatchService } from '../../services/match.service';
import { UserService } from '../../services/user/user.service';


@Component({
  selector: 'umu-register-page',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPageComponent implements OnInit {
  
  steps = ['access', 'basic-data', 'password', 'sectors', 'description2', 'education', 'values']
  inputUser: InputUser = new InputUser()
  userAvatarB64: string = ''

  constructor(
    private menu: MenuController,
    private router: Router,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { }
  
  matchService = inject(MatchService)
  userService = inject(UserService)
  authService = inject(AuthService)
  translateService = inject(TranslateService)

  // Slides  
  currentSlide: number = 0
  nextStep: number = 1
  currentFormValue: object = {}
  continueBtnDisabled: boolean = true
  goToSlide(slide: number) {
    if(this.currentSlide > slide) {
      this.continueBtnDisabled = false
    }
    this.currentSlide = slide
  }

  updateInputUser(values: object) {
    this.inputUser = {
      ...this.inputUser,        
      ...values
    }
  }

  ngOnInit() {
    sessionStorage.clear();
    this.menu.enable(false);
  }

  onValidStep(values: object, nextStep: number = 0) {
    if(values != null){
      this.continueBtnDisabled = false
      this.currentFormValue = values
      this.nextStep = nextStep
    } else {
      this.continueBtnDisabled = true
    }
  }

  onStep1Complete(userType: 'candidate' | 'company') {
    this.inputUser.type = userType
    // Remove candidate last step for companies
    if (userType == 'company' && this.steps.length == 7) {
      this.steps = [...this.steps.slice(0,this.steps.length-1)];
    }
    this.goToSlide(1)
  }

  onStepComplete(values: object,nextStep: number) {
    this.updateInputUser(values)
    // Substract one, slides work with 0 based indexes
    this.goToSlide(nextStep-1)
    this.continueBtnDisabled = true
  }

  finish(){
    this.updateInputUser(this.currentFormValue)
    this.createUser()
  }

  createUser() {
    // Set date
    this.inputUser.createdDate = new Date().toISOString()

    // Create user in server
    this.userService.createUser(this.inputUser).subscribe({
      next: () => {

        this.loadingCtrl.dismiss();
        this.doFirstLogin(this.inputUser.email,this.inputUser.password)
      },
      error: (error) => {
        this.loadingCtrl.dismiss();
        this.translateService.stream(error.message).subscribe(async (value) => {
          const toast = await this.toastCtrl.create({
            color: 'danger',
            message: value,
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        });
      }
    })
  }

  doFirstLogin(email, password) {
    const credentials = { email, password }
    this.authService.login(
      credentials,
      () => {
        if(this.inputUser.type === 'candidate'){
          this.userService.me.subscribe((me)=> {
            this.matchService.calculateMyMatches().subscribe((match) => {
              this.router.navigateByUrl('/new-user', { replaceUrl: true });
              this.menu.enable(true);
            })
          })
          
        } else {
          this.router.navigateByUrl('/new-user', { replaceUrl: true });
          this.menu.enable(true);
        }
      }
    )
  }

}