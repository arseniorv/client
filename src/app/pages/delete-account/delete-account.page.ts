import { Component, OnInit } from "@angular/core";
import { map } from "rxjs";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user/user.service";

@Component({
    selector: 'app-delete-account',
    templateUrl: './delete-account.page.html',
    styleUrls: ['./delete-account.page.scss'],
  })
  export class DeleteAccountPageComponent implements OnInit { 
    userInfo: User;
    avatar: string; 
    
    constructor(private uService: UserService) {}



    ngOnInit(): void {
        this.getLoggedUser();
    }

    getLoggedUser() {
        this.uService.qGetMe().valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
          this.userInfo = item.me;
          this.avatar = item.me.avatarB64;
        });
      }

  }