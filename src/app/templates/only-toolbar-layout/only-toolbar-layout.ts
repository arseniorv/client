import { Component, CUSTOM_ELEMENTS_SCHEMA, HostListener, input, OnInit } from '@angular/core';
import { IonicModule, ScrollDetail } from '@ionic/angular';
import { ToolbarComponent } from 'src/app/components/toolbar/toolbar.component';
import { TitleHeader } from "../../partials/title-header/title-header";
import { MainMenuMobileComponent } from 'src/app/components/menus/main-menu-mobile/main-menu-mobile.component';

@Component({ 
  standalone: true,
  selector: 'umu-only-toolbar-layout',
  templateUrl: './only-toolbar-layout.html',
  styleUrls: ['./only-toolbar-layout.css'],
  imports: [ToolbarComponent, IonicModule, TitleHeader, MainMenuMobileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class OnlyToolbarLayoutComponent {


    headerScrolled = false 
    umuToolbarHeight: number = 56
    titleText = input<string>('')

// This function fix the toolbar problem in legal-info page when accessing directly through the URL
   
    // ngAfterViewInit() {
    //   window.dispatchEvent(new Event('resize'))
    // }
    
  
    @HostListener('window:scroll', ['$event'])
    onScroll(event: CustomEvent<ScrollDetail>) {
      let scrollPosition = window.scrollY; 
      const targetSection = document.getElementById('targetSection'); 
      let sectionPosition = targetSection.offsetTop;
  
      this.headerScrolled = targetSection.getBoundingClientRect().bottom < this.umuToolbarHeight;
    }
  
}
