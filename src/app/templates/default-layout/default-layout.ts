import { Component, inject, OnInit, HostListener, CUSTOM_ELEMENTS_SCHEMA, input } from '@angular/core'; 
import { HeaderComponent } from 'src/app/partials/header/header.component';
import { ToolbarComponent } from 'src/app/components/toolbar/toolbar.component';
import { IonicModule } from '@ionic/angular';
import { BackButtonComponent } from "../../components/buttons/back-button/back-button.component";
import { Router } from '@angular/router';
import { MainMenuMobileComponent } from 'src/app/components/menus/main-menu-mobile/main-menu-mobile.component';
import { User } from 'src/app/models/user';

@Component({ 
  standalone: true,
  selector: 'umu-default-layout',
  templateUrl: './default-layout.html',
  styleUrls: ['./default-layout.css'],
  imports: [HeaderComponent, ToolbarComponent, IonicModule, BackButtonComponent, MainMenuMobileComponent], 
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DefaultLayoutComponent implements OnInit{

  router = inject(Router)
  canEditHeader: boolean = false
  pagesCanEdit = ['user-profile', 'company-profile']
  pagesShowProfValues = ['user-profile', 'company-profile', 'offer-detail', 'project-detail']
  currentPage: string = ''
  umuToolbarHeight: number = 56
  headerScrolled = false; 
  user = input<User>({} as User);
  showProfValues: boolean = false;

  ngOnInit() {
    this.currentPage = this.router.url.slice(1,this.router.url.length)
    this.canEditHeader = !!this.pagesCanEdit.find((e) => e === this.currentPage);
    this.showProfValues = !!this.pagesShowProfValues.find((e) => this.currentPage.toString().includes(e));
  }

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    const targetSection = document.getElementById('targetSection'); 

    this.headerScrolled = targetSection.getBoundingClientRect().bottom < this.umuToolbarHeight;
  }
}
