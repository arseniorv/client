import { Routes } from '@angular/router';
import { AuthService } from './services/auth.service';

import { CandidaturesPageComponent } from './pages/candidatures/candidatures.page';
import { ChatPageComponent } from './pages/chat/chat.page';
import { CompanyOfferDetailPageComponent } from './pages/company-offer-detail/company-offer-detail.page';
import { CompanyOffersPageComponent } from './pages/company-offers/company-offers.page';
import { CompanyProfilePageComponent } from './pages/company-profile/company-profile.page';
import { ConversationsPageComponent } from './pages/conversations/conversations.page';
import { CreateOfferPageComponent } from './pages/create-offer/create-offer.page';
import { DeleteAccountPageComponent } from './pages/delete-account/delete-account.page';
import { EditOfferPageComponent } from './pages/edit-offer/edit-offer.page';
import { EditUserPageComponent } from './pages/edit-user/edit-user.page';
import { AllProjectsListComponent } from './pages/enterprising/all-projects-list/all-projects-list.page';
import { CreateProjectComponent } from './pages/enterprising/create-project/create-project.component';
import { EditProjectComponent } from './pages/enterprising/edit-project/edit-project.component';
import { InterestProjectsComponent } from './pages/enterprising/interest-projects/interest-projects.page';
import { InterestedEntityComponent } from './pages/enterprising/interested-entity/interested-entity.page';
import { ProjectDetailComponent } from './pages/enterprising/project-detail/project-detail.page';
import { ProjectListComponent } from './pages/enterprising/project-list/project-list.page';
import { EntityListPageComponent } from './pages/entity-list/entity-list.page';
import { LegalInfoPageComponent } from './pages/legal-info/legal-info.page';
import { LoginPageComponent } from './pages/login/login.page';
import { NewUserComponent } from './pages/new-user/new-user/new-user.component';
import { CandidateOfferDetailPageComponent } from './pages/candidate-offer-detail/candidate-offer-detail.page';
import { CandidateOffersPageComponent } from './pages/candidate-offers/candidate-offers.page';
import { PasswordResetPageComponent } from './pages/password-reset/password-reset.page';
import { RegisterPageComponent } from './pages/register/register.page';
import { CandidateProfilePageComponent } from './pages/candidate-profile/candidate-profile.page';
import { ValuesModalPageComponent } from './shared/modals/values-modal/values-modal.page';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path:'password-reset',
    component: PasswordResetPageComponent
  },
  {
    path:'password-reset/:token',
    component: PasswordResetPageComponent
  },
  {
    path: 'offer-list',
    component: CandidateOffersPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'offer-detail/:id',
    component: CandidateOfferDetailPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'user-profile',
    component: CandidateProfilePageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'entity-list',
    component: EntityListPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'values-modal',
    component: ValuesModalPageComponent,
  },
  {
    path: 'company-profile',
    component: CompanyProfilePageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'company-offer',
    component: CompanyOffersPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'company-offer-detail/:id',
    component: CompanyOfferDetailPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'candidatures',
    component: CandidaturesPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'edit-user',
    component: EditUserPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'register',
    component: RegisterPageComponent
  },
  {
    path: 'edit-offer',
    component: EditOfferPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'new-user',
    component: NewUserComponent
  },
  {
    path: 'create-offer',
    component: CreateOfferPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'delete-account',
    component: DeleteAccountPageComponent,
    canActivate: [AuthService]
  },
  {
    path: 'legal-info',
    component: LegalInfoPageComponent
  },
  {
    path: 'project-list',
    component: ProjectListComponent
  },
  {
    path: 'create-project',
    component: CreateProjectComponent
  },
  {
    path: 'interest-projects',
    component: InterestProjectsComponent
  },
  {
    path: 'project-detail/:id',
    component: ProjectDetailComponent
  },
  {
    path: 'edit-project',
    component: EditProjectComponent
  },
  {
    path: 'all-projects-list',
    component: AllProjectsListComponent
  },
  {
    path: 'interested-entity',
    component: InterestedEntityComponent

  },
  {
    path: 'chat/:chatId',
    component: ChatPageComponent
  },
  {
    path: 'conversations',
    component: ConversationsPageComponent
  }

];
