import { Component, input, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { BannerComponent } from 'src/app/components/banner/banner.component';

interface Ad {
  id: string,
  img: string
}

const ads = [
  {
    id: 'mp1',
    img: "reciclaje_vertical2.jpg",
  },
  {
    id: 'mp2',
    img: "bosque2.jpg",
  },
  {
    id: 'mp3',
    img: "accion_vertical2.jpg",
  },
  {
    id: 'mp4',
    img: "bosque_02.jpg",
  },
  {
    id: 'mp5',
    img: "mar_vertical2.jpg",
  },
  {
    id: 'mp6',
    img: "amapolas2.jpg",
  },
]

@Component({
  standalone: true,
  selector: 'umu-initial-banner',
  templateUrl: './initial-banner.component.html',
  styleUrls: ['./initial-banner.component.css'],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    BannerComponent
    ]
})
export class InitialBannerComponent implements OnInit {

  ads: Array<Ad> = ads
  wasDisplayed: boolean
  displayPeriodInHours = 48
  selectedAdIndex: number
  numOfAds: number = 6
  isOpen: boolean = false

  constructor() {
    const lastShowedDate = localStorage.getItem('initialBannerDisplayed') !== undefined ? new Date(Number(localStorage.getItem('initialBannerDisplayed'))) : undefined
    const now = new Date()
    const diffInHours = (now.getTime() - lastShowedDate.getTime()) / (1000*3600)

    if( lastShowedDate === undefined || diffInHours >= 48  ){
      localStorage.removeItem('initialBannerDisplayed')
      localStorage.setItem('initialBannerDisplayed', JSON.stringify(Date.now()));
      this.wasDisplayed = false
    } else {
      this.wasDisplayed = true
    }
  }

  ngOnInit(): void {
    if(!this.wasDisplayed){
      // Substract 1 to numOfAds due to the use od index 0
      this.selectedAdIndex = Math.floor(Math.random() * (this.numOfAds - 1));
      this.isOpen = true
    }
  }

  onCloseBanner(){
    this.isOpen = false;
  }
}
