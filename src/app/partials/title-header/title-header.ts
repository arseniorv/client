import { Component, input, OnInit } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";

@Component({
    standalone: true,
    selector: 'umu-title-header',
    templateUrl: './title-header.html',
    styleUrl: './title-header.css',
    imports: [
    IonicModule,
    TranslateModule
]
})
export class TitleHeader {
    titleText = input.required<string>()

}