import { Component } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";

@Component({
  standalone: true,
  selector: 'umu-legal-info',
  templateUrl: './legal-info.component.html',
  styleUrls: ['./legal-info.component.css'],
  imports: [
    TranslateModule,
    IonicModule
  ]
})
export class LegalInfoComponent {}