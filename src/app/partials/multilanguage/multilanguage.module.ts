import { HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { BannerComponent } from 'src/app/components/banner/banner.component';



@NgModule({ declarations: [],
    exports: [], 
    imports: [
      BannerComponent,
      TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })], 
    providers: [provideHttpClient(withInterceptorsFromDi())] })
export class MultilanguageModule { }

