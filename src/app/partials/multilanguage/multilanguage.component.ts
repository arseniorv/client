import { Component, inject, input, output } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from 'src/app/components/shared.module';
import { UserService } from 'src/app/services/user/user.service';
import { cityList, languageList } from 'src/app/utils/constants';

@Component({
  standalone: true,
  selector: 'umu-multilanguage',
  templateUrl: './multilanguage.component.html',
  styleUrls: ['./multilanguage.component.css'],
  imports: [
    SharedModule
  ]
})
export class MultilanguageComponent {
  private uService = inject(UserService);
  public translate = inject(TranslateService);
  languageListTranslated: any[] = [];
  finalLangList: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  sectorListTranslated: any[] = [];
  skillsListTranslated: any[] = [];

  color = input<string>('white');
  position = input<string>('absolute');
  onLanguageSwitch = output<void>();
  extended = input<boolean>(false);

  
  constructor() {
      this.translate.use(window.localStorage.getItem('language')? window.localStorage.getItem('language'): 'es');
    }

  //Multilanguage
  useLanguage(language: string): void {
    window.localStorage.setItem('language', language);
    this.translate.use(language);
    if (window.sessionStorage.getItem('userId')){
      this.uService.mUserPreferencesUpdate(window.sessionStorage.getItem('userId'), {language}).subscribe(() => {
        console.log('language stored!')
      })
    }   
  }

  //Translate and sort lists alphabetically
  translateCityList(){  

      if (this.cityListTranslated){
        this.cityListTranslated = []
      } 
 
      for (let city of cityList){
        this.translate.get(city).subscribe((cityTranslated: string) => {
            this.cityListTranslated.push({value:city, label:cityTranslated})
        }) 
      }
      return this.alphabeticalOrder(this.cityListTranslated)

  }   

  translateLangList(){
      if (this.languageListTranslated){
        this.languageListTranslated = []
      } 

      for (let language of languageList){
        this.translate.get(language).subscribe((languageTranslated: string) => {
            this.languageListTranslated.push({value:language, label:languageTranslated})
        }) 
      }

      return this.alphabeticalOrder(this.languageListTranslated)
  }

  translateSectorList(sectorsList){
    if (this.sectorListTranslated){
      this.sectorListTranslated = []
    } 

    for (let sector of sectorsList){
      let sectorTranslated = this.translate.instant(sector.name)
      this.sectorListTranslated.push({value:sector, label:sectorTranslated})
    }

    return this.alphabeticalOrder(this.sectorListTranslated)
  }

  translateSkillsLists(skillsList){  
    if (this.skillsListTranslated){
      this.skillsListTranslated = []
    } 

    for (let skill of skillsList){
      let skillsTranslated = this.translate.instant(skill.name)
      this.skillsListTranslated.push({label: skillsTranslated, name: skill.name, _id: skill._id})
    }
    return this.alphabeticalOrder(this.skillsListTranslated)
  }   


  alphabeticalOrder(translatedList){   
    return translatedList.sort((a, b) => new Intl.Collator(this.translate.currentLang).compare(a.label, b.label));
  }

  onLanguageSwitchEvent(language:string){
    this.useLanguage(language);
    this.onLanguageSwitch.emit();
  }
} 





