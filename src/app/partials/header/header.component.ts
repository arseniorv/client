import { Component, inject, input, OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user/user.service';
import { CommonModule } from '@angular/common';
import { IonicModule, IonRouterOutlet, ModalController } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ProfValueBadgeComponent } from 'src/app/components/badges/prof-value-badge/prof-value-badge.component';
import { FabButtonComponent } from "../../components/buttons/fab-button/fab-button.component";
import { SharedModule } from 'src/app/components/shared.module';
import { AvatarComponent } from "../../components/forms/avatar/avatar.component";
import { ProfValuesModalComponent } from 'src/app/components/modals/prof-values-modal/prof-values-modal.component';

@Component({
  standalone: true,
  selector: 'umu-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
  imports: [
    CommonModule,
    SharedModule,
    IonicModule,
    TranslateModule,
    ProfValueBadgeComponent,
    FabButtonComponent,
    AvatarComponent,
    ProfValuesModalComponent
]
})
export class HeaderComponent implements OnInit {
  
  private userService = inject(UserService);

  _loggedIn: boolean
  userInfo: User;
  canEdit = input<boolean>(false)
  showProfValues = input<boolean>(true);
  iconName=''
  iconPath='../../assets/icons/edit_icon.svg'
  onNewAvatar: string;
  isModalOpen: boolean = false;
  user = input<User>({} as User);
  
  constructor(
    private breakpointObserver: BreakpointObserver,
  ){}

  ngOnInit() {
    if(!this.user()._id){
      this.userService.me.subscribe(me => {
        this.userInfo = me
      })
    } else {
      this.userInfo = this.user();
    }
    
  }
  onUpdateAvatar(event){
    this.onNewAvatar = event;
  }

  openProfValuesModal() {
    this.isModalOpen = true;
  }
  onCloseEvent() {
    this.isModalOpen = false;
  }
}
